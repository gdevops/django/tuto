.. index::
   pair: Community; DjangoConEurope

.. _django_community:

==================================================
**Community**
==================================================

Forum
=======

- https://forum.djangoproject.com/


Discord
==========

- https://discord.gg/django

DjangoConEurope
================

- https://www.youtube.com/@DjangoConEurope/videos
