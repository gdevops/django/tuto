.. index::
   pair: Django ; Debug
   pair: python ; breakpoint

.. _debug:

=========================================================
**debug**
=========================================================





Template debug
================

.. seealso:: https://x.com/pybites/status/1314454822284865536?s=20

::

    # [your app]/templatetags/tags.py
    from django import template
    register = template.Librarry()


    @register.simple_tag(takes_context=True)
    def set_breakpoint(context):
        breakpoint()


::

    # in template
    {% load tags %}

    {% set_breakpoint %}
