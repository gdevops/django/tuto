.. index::
   pair: Group BY; SQL

.. _understand_group_by:

=================================================================================================
**Understand Group by in Django with SQL, Django QuerySets and SQL side by side** by Haki Benita
=================================================================================================

.. seealso::

   - https://hakibenita.com/django-group-by-sql




Introduction
=============


Aggregation is a source of confusion in any type of ORM and Django is no different.

The documentation provides a variety of examples and cheat-sheets that demonstrate
how to group and aggregate data using the ORM, but I decided to approach this
from a different angle.

In this article I put QuerySets and SQL side by side. If SQL is where you are
most comfortable, this is the Django GROUP BY cheat-sheet for you.
