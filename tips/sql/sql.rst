.. index::
   pair: Django ; SQL Tips

.. _django_sql_tips:

============================================
Django SQL tips
============================================

.. toctree::
   :maxdepth: 3

   tips_1/tips_1
   tips_2/tips_2
   tips_3/tips_3
