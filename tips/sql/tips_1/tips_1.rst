.. index::
   pair: Django ; SQL commands in your terminal

.. _django_sql_tips_1:

==============================================================
Displaying SQL commands in your terminal
==============================================================

::

	import logging as l
	lg = l.getLogger('django.db.backends')
	lg.setLevel(l.DEBUG)
	lg.addHandler(l.StreamHandler())


::

    from django.db import connection
    from django.db import reset_queries

    ...

    reset_queries()

    .. make some queries

    connection.queries
