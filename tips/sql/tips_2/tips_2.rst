.. index::
   pair: connection.queries ; SQL

.. _django_sql_tips_2:

==============================================================
**connection.queries** (only available if DEBUG is True)
==============================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/faq/models/#how-can-i-see-the-raw-sql-queries-django-is-running
   - :ref:`sqlparse`


Make sure your **Django DEBUG setting is set to True**. Then do this::

    >>> from django.db import connection
    >>> connection.queries
    [{'sql': 'SELECT polls_polls.id, polls_polls.question, polls_polls.pub_date FROM polls_polls',
    'time': '0.002'}]


**connection.queries is only available if DEBUG is True**.
