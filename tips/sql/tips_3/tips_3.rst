.. index::
   pair: query ; QuerySet

.. _django_sql_tips_3:

==============================================================
Utilisation de **QuerySet.query** (str(query))
==============================================================

- https://docs.djangoproject.com/en/dev/faq/models/#how-can-i-see-the-raw-sql-queries-django-is-running

Example
========

::

    In [43]: query = LogDocument.objects.filter(logdate__year=2020).query                                                                                                                                       In [44]: query                                                                                                                                                                                              Out[44]: <django.db.models.sql.query.Query at 0x7f2c37bc53a0>
    In [45]: str(query)                                                                                                                                                                                         Out[45]: 'SELECT `log_document`.`id`, `log_document`.`logdate`, `log_document`.`id_etude_notariale`, `log_document`.`agence_id`, `log_document`.`type_doc`,
    `log_document`.`status_doc`, `log_document`.`formality_type` FROM `log_document` WHERE `log_document`.`logdate`
    BETWEEN 2020-01-01 00:00:00 AND 2020-12-31 23:59:59.999999 ORDER BY `log_document`.`logdate` DESC'

