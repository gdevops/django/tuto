.. index::
   pair: Django ; Extensions

.. _vincent_2024_08_22:

=========================================================
2024-08-22 **Django UserProfile Model** by Will Vincent
=========================================================

- https://learndjango.com/tutorials/django-userprofile-model
- https://fosstodon.org/@wsvincent/113006343931336354
- https://fosstodon.org/@wsvincent/112995862693371206
- https://wsvincent.com/about/
- :ref:`william_vincent`


Description
===============

- https://docs.djangoproject.com/en/5.1/ref/contrib/auth/#user-model
- https://learndjango.com/tutorials/django-login-and-logout-tutorial
- https://learndjango.com/tutorials/django-custom-user-model

Every website needs a way to handle user authentication and logic about users. 

Django comes with a built-in `User model <https://docs.djangoproject.com/en/5.1/ref/contrib/auth/#user-model>`_ as well as views and URLs for login, 
log out, password change, and password reset. 

You can see how to implement it all `in this tutorial https://learndjango.com/tutorials/django-login-and-logout-tutorial <https://learndjango.com/tutorials/django-login-and-logout-tutorial>`_

But after over a decade of experience working with Django, I've come around 
to the idea that there is, in fact, one ideal way to start 99% of all new 
Django projects, and it is as follows:

- include a `custom user model <https://learndjango.com/tutorials/django-custom-user-model>`_ but don't add any fields to it
- add a `UserProfile model <https://learndjango.com/tutorials/django-userprofile-model#user-profile>`_

In this tutorial, I'll demonstrate how to implement these two key features, 
which will set you up on the right path for your next Django project.



Conclusion
=============

- https://learndjango.com/courses/

If you want more handholding on setting up a new Django project properly, 
I recommend one of the `Courses <https://learndjango.com/courses/>`_ on this site, which will go through all 
the steps in more detail.

