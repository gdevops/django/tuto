.. index::
   pair: zip ; lists

.. _django_tip_zip:

======================================================================================
**use of zip in order to iterate over several lists in Django templates**
======================================================================================

Use case 1
==============

Python code
---------------

.. code-block:: python 

    listes_des_projets_cartes_8_jours = zip(
        infos_fiches_weekly_8_jours.liste_projets_cartes,
        liste_data_formulaires_8_jours,
        tab_somme_projet_weekly_8_jours,
    )
    
    contexte = {

        "listes_des_projets_cartes": listes_des_projets_cartes_8_jours,
    }
    

Django template
--------------------

.. code-block:: python 

    {% for projet_carte, liste_data_formulaires, sum_projet in listes_des_projets_cartes %}


    {% endfor %}
