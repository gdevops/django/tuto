.. index::
   pair: docker-compose ; postgresql
   pair: docker-compose ; extra_hosts
   ! extra_hosts

.. _django_docker_compose_tips:

============================================
**docker-compose**
============================================

How to use the **local postgresql database** (host.docker.internal) with the django service in a docker-compose file ?
=============================================================================================================================

- https://docs.docker.com/compose/compose-file/compose-file-v3/#extra_hosts
- :ref:`docker:docker_20_10_0`

We have to add these extra_hosts parameter in the docker-compose file.

::

    extra_hosts:
        # Support host.docker.internal in dockerd on Linux
        - "host.docker.internal:host-gateway"
