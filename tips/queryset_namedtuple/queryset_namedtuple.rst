.. index::
   pair: QuerySet ; namedtuple

.. _queryset_namedtuple:

==============================================
QuerySet Results as namedtuple (Django 2.0+)
==============================================

.. seealso::

   - https://hakibenita.com/9-django-tips-for-working-with-databases
   - https://hakibenita.com/working-with-apis-the-pythonic-way
   - :ref:`django_2_0`



`I'm a big fan of namedtuples`_ and apparently starting Django 2.0 so is the ORM.

In Django 2.0 a new attribute was added to values_list called named. Setting
named to true will return the queryset as a list of namedtuples::

    >>> user.objects.values_list(
        'first_name',
        'last_name',
    )[0]
    ('Haki', 'Benita')

    >>> user_names = User.objects.values_list(
        'first_name',
        'last_name',
        named=True,
    )

    >>> user_names[0]
    Row(first_name='Haki', last_name='Benita')

    >>> user_names[0].first_name
    'Haki'

    >>> user_names[0].last_name
    'Benita'


.. _`I'm a big fan of namedtuples`: https://hakibenita.com/working-with-apis-the-pythonic-way
