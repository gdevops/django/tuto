.. index::
   pair: Django; URL

.. _django_url_resolving:

============================================
Django URL resolving
============================================


How to debug Django's url resolving ?
=========================================

Source
    - https://github.com/guettli/django-tips

If you are new to a big project, you might not easily see which view
function does handle an URL.

You can use resolve() to get the answer:

.. code-block::

    import django
    django.setup()
    from django.urls import resolve
    print(resolve('/foo/bar/'))

    --> ResolverMatch(func=foocms.views.bar_handler, args=('/foo/bar/',), kwargs={}, url_name=foocms, ...)
