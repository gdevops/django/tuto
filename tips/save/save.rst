.. index::
   pair: Tip ; save

.. _tip_save_update:

=========================================================
**save(update_fields=["field_name"])**
=========================================================

.. seealso::

   - https://stackoverflow.com/questions/13901244/update-only-specific-fields-in-a-models-model



To update a subset of fields, you can use **update_fields**:

    survey.save(update_fields=["active"])
