.. index::
   pair: Django ; Tips

.. _django_tips:

============================================
**Django tips**
============================================

- See also : https://hakibenita.com/9-django-tips-for-working-with-databases

.. toctree::
   :maxdepth: 3

   aggregation_filter/aggregation_filter
   dataclass/dataclass
   django-user-profile/django-user-profile
   debug/debug
   django_extensions/django_extensions
   docker-compose/docker-compose
   queryset_namedtuple/queryset_namedtuple
   save/save
   sql/sql
   understand_group_by/understand_group_by
   url_resolving/url_resolving
   zip-use-in-templates/zip-use-in-templates   
