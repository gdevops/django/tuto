
.. _django_runscript:

=========================================================
**runscript**
=========================================================

.. seealso::

   - https://monadical.com/posts/django-packages.html
   - https://django-extensions.readthedocs.io/en/latest/runscript.html



RunScript
============

Sometimes you want to run a script in your Django application.

Typically when you are writing your script, you will use the Django shell
to drive your development, then wrap the functionality in a Django command.

However, with this command, you can run the script directly by wrapping
it in a simple function.

For example, if you wanted to write a script to delete all Question objects
in your database, it would be as simple as:


.. code-block:: python

    # scripts/delete_all_questions.py

    from polls.models import Question

    def run():
        # Fetch all questions
        questions = Question.objects.all()
        # Delete questions
        questions.delete()


Then to run the script, you would::

    ./manage.py manage.py runscript  delete_all_questions
