
.. index::
   pair: People ; William Vincent

.. _william_vincent:

============================================
**William Vincent**
============================================

- https://learndjango.com/tutorials/
- https://fosstodon.org/@wsvincent
- https://fosstodon.org/@wsvincent.rss
- https://github.com/wsvincent/awesome-django



.. toctree::
   :maxdepth: 3

   books/books
   tutorials/tutorials
