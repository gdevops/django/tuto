
.. index::
   pair: Books ; William Vincent


.. _william_vincent_books:

============================================
William Vincent books
============================================




Django for professionnals, 2019
=================================

.. seealso::

   - :ref:`django_for_prof`
   - https://github.com/wsvincent/djangoforprofessionals
