
.. index::
   pair: People ; Vitor Freitas


.. _vitor_freitas:

===================================================
**Vitor Freitas** (Simple is Better Than Complex)
===================================================

.. seealso::

   - https://x.com/vitorfs
   - https://github.com/sibtc
   - https://simpleisbetterthancomplex.com/



.. toctree::
   :maxdepth: 3

   tutorials/tutorials
