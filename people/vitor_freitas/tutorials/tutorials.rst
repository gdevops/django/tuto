

.. _vitor_freitas_tutorials:

===================================================
Vitor Freitas **tutorials**
===================================================

.. seealso::


   - https://simpleisbetterthancomplex.com/archive/#tutorials





.. _vitor_better_models:

django-tip-22-designing-better-models
========================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tips/2018/02/10/django-tip-22-designing-better-models.html


a-complete-beginners-guide-to-django
=====================================

.. seealso::

   - https://simpleisbetterthancomplex.com/series/2017/09/04/a-complete-beginners-guide-to-django-part-1.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-2.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-3.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-4.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-5.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-6.html
   - https://simpleisbetterthancomplex.com/series/2017/10/16/a-complete-beginners-guide-to-django-part-7.html
