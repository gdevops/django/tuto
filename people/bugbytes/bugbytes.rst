.. index::
   ! Bug bytes

.. _bug_bytes:

==================================================
**Bug bytes** (Django + HTMX)
==================================================

- https://x.com/bugbytesio
- https://www.bugbytes.io/posts/
- https://github.com/bugbytes-io/
- https://www.youtube.com/channel/UCTwxaBjziKfy6y_uWu30orA/videos

Contact
========

We welcome your feedback, ideas and suggestions !

We are committed to improving our posts and videos, and implementing
the feedback and requests that we receive.

If you have any suggestions for improvements, or any ideas for future
posts and videos, please do get in touch.

We aim to respond to all suggestions, and welcome and appreciate any
interest in the work we are doing!

django-htmx
============

- https://github.com/bugbytes-io/django-htmx
