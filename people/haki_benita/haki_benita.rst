.. index::
   pair: People ; Haki Benita

.. _haki_benita:
.. _haki_benita_ref:

===============================================
**Haki Benita** (https://x.com/be_haki)
===============================================


- https://x.com/be_haki
- https://hakibenita.com/


Tests
=======


- :ref:`haki_django_test`


Tutorials
============


- :ref:`twilio_ivr`
