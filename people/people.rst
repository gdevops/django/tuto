.. index::
   pair: Django ; People

.. _django_people:

============================================
**Django people**
============================================

.. toctree::
   :maxdepth: 3

   adam_johnson/adam_johnson
   alice_ridgway/alice_ridgway
   bugbytes/bugbytes
   david_guillot/david_guillot
   haki_benita/haki_benita
   jack_linke/jack_linke
   jamie_matthews/jamie_matthews
   luke_plant/luke_plant
   mariusz_felisiak/mariusz_felisiak
   matt_layman/matt_layman
   michael_herman/michael_herman
   simon_willison/simon_willison
   tom_christie/tom_christie
   valentino_gagliardi/valentino_gagliardi
   vitor_freitas/vitor_freitas
   william_vincent/william_vincent
