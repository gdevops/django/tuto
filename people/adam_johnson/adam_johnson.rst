.. index::
   pair: People ; Adam Johnson

.. _adam_johnson:

======================================================
**Adam Johnson** (https://fosstodon.org/@adamchainz)
======================================================

- https://github.com/adamchainz
- https://fosstodon.org/@adamchainz

Tests
=======

- https://gumroad.com/l/suydt
- https://adamj.eu/tech/2020/09/07/how-to-unit-test-a-django-management-command/


Speed Up Your Django Tests
==============================

- https://gumroad.com/l/suydt
- https://x.com/AdamChainz/status/1271420333095030784?s=20

This book is a practical guide to making your Django project's tests faster.

It has many tips and tricks that apply to all projects, big and small.
And it covers the two most popular test runners: Django's test framework and pytest.

It's based on my experience speeding up various Django projects' test
suites, improving Django's own testing framework, and creating pytest plugins.


Actions
=========

.. toctree::
   :maxdepth: 3

   actions/actions
