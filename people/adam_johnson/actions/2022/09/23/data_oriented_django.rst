.. index::
   pair: Data Oriented ; Django

.. _data_oriented_django:

=============================================================================
**Data-Oriented Django** 2022-09-23, 15:00–15:30 (Europe/Lisbon), Auditorium
=============================================================================

- https://pretalx.evolutio.pt/djangocon-europe-2022/talk/B7GSRY/
- https://github.com/adamchainz/talk-data-oriented-django
- https://github.com/adamchainz/talk-data-oriented-django/blob/main/talk-data-oriented-django.pdf


Description
===========


**Data-Oriented Design** focuses on how software transforms specific inputs
to specific outputs, on specific hardware.

This talk will cover how this way of thinking can inform writing Django code.

My high school class defined computing as: software controlling hardware
to consume input data and emit output data. Simple enough that I still remember it.

Data-Oriented Design invites us to take this definition seriously.
Users only care about convenient input, correct output, as fast as possible.
Many abstractions we reach for can hold us back from reaching these goals.

This talk will first look at Data-Oriented Design through some general
examples, including speeding up JSON parsing in C++ more than 50x.

It will then show some Django data patterns that can make our code
small and fast, such as normalization, reducing memory usage, splitting
models, and techniques for getting the database to do as much work as possible.
