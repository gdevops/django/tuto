.. index::
   ! Jack Linke

.. _jack_linke_django:

==================================================
**Jack Linke** (Django + HTMX)
==================================================

- https://social.jacklinke.com/@jack
- https://jacklinke.com/
- https://github.com/jacklinke
- https://github.com/jacklinke/htmx-talk-2021
- https://hashnode.com/@jacklinke

Jack Linke 2024
===============


- :ref:`django_news:jack_linke_2024_12_18`


Conferences
============

- :ref:`tuto_htmx:jack_linke_2021_10_22`
