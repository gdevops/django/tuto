.. index::
   pair: People ; Tom Christie

.. _tom_christie:

===================================================
**Tom Christie**
===================================================

- https://github.com/tomchristie
- https://x.com/starletdreaming
- http://www.tomchristie.com/
- http://www.encode.io/



Hi, I'm Tom, a software engineer specializing in API design & development.

I have extensive experience in tech lead & project management roles,
with a career that spans speech recognition, networking infrastructure,
and web & mobile development.

I have developed one of the world's leading API frameworks, managing a
project that now has over 1,000 individual contributors.
My open-source software is used by internationally recognized companies
including Mozilla, Red Hat, Eventbrite, Heroku, and many more…

I live in Brighton, and am currently working on building a sustainable
business model for collaboratively funded open-source developement,
through my company, Encode.

Alongside my open-source work, I also have some availability for software
development & design on a consultancy basis. If you would like to discuss
working together, please get in touch.

Technical Background

Director, Encode. 2016-present.
Technical Lead, DabApps. 2012-2016.
Senior Web Engineer, Bright Interactive. 2010-2012.
Senior Software Engineer, Velocix/Alcatel-Lucent. 2006-2009.
R&D Engineer, Softsound/Autonomy. 2001-2004.
MPhil, Computer Speech & Language Processing, Cambridge University. 2000-2001.
First class BSc, Computer Science, Manchester University. 1997-2000.
Software Developer, BAe SEMA. 1996, 1997.
