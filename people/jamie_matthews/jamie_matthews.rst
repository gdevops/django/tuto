.. index::
   pair: People ; Jamie Matthews

.. _jamie_matthews:

===================================================
**Jamie Matthews**
===================================================


- https://x.com/j4mie
- https://x.com/dabapps
- https://github.com/dabapps
- https://www.dabapps.com/who-we-are/meet-the-team/
- https://www.mtth.org/

.. figure:: jamie_matthews.png
   :align: center


.. figure:: images/jamie_matthews_2.jpeg
   :align: center


Articles
==============

.. toctree::
   :maxdepth: 3

   articles/articles

Djangochat
============

- :ref:`django_readers`
- https://djangochat.com/episodes/optimizing-django-queries-jamie-matthews
- https://djangochat.com/episodes/optimizing-django-queries-jamie-matthews/transcript

Projects
========

- :ref:`django_forms_dynamic`
- :ref:`django_readers`
- :ref:`django_forms_dynamic`
