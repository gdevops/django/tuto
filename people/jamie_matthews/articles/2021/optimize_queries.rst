
.. _jamie_matthews_optimize_queries:

===================================================
**optimizing-django-queries-jamie-matthews**
===================================================

- https://djangochat.com/episodes/optimizing-django-queries-jamie-matthews/transcript

Introduction
==============

- https://djangochat.com/episodes/optimizing-django-queries-jamie-matthews/transcript

Okay, so I'll introduce myself first I'm technical director of dabapps,
I suppose in America that might be CTO. We're a kind of web or mobile agency.

So we build bespoke web apps, mobile apps, usually with API backends of
some sort. **And we use Django for all of our server side code**.

We're based in Brighton, which is on the south coast of England. And we've
got an office in Wales as well, about 30 people 1718 devs, mixture of
back end and front end full stack.
And we've probably built over the years, I was trying to add it up.
It's definitely over 50. And it might be kind of closer to 100, Django
projects, anything from like small code bases, you know, a few 1000 lines
of code up to our biggest projects are sort of 7080 100,000 lines of Python.
So we have some some really big projects. Oh, well, relatively speaking.

I realized I didn't, I didn't really answer your question, which was how
I how I came to Django. No, good. So well, I'll loop back around to that
one. So I think I've found I've tried to remember where I first used Python,
I definitely came out. I did a computer science degree in Manchester.
And I definitely came out of that with an awareness of Python. I can't
remember whether I used it as part of a course or whether I just sort
of found it in my you know, in my free time. I then moved down to Brighton
to do a master's and I use Python quite a lot.

They're doing kind of scientific computing type of stuff. When I finished
a master's, I went and got a job as a web developer, but doing PHP.
But I used to get so Brian has this really amazing sort of tech scene
with lots of meetups. And there was one which I used to go to a lot which
believe it or not, was called Flash Brighton, right? Remember flash?
Yeah. Well, so I didn't really do very much flash, but I kind of went
there because when they didn't have flash topics to talk about, they got
all sorts of other people and all sorts of other technologies.
And one of the speakers there was :ref:`Simon Willison <simon_willison>`,
who of course, is one of the original co creators of Django, who at the
time lived in Brighton.

So he was giving a talk about Django, this would have been, I think, I
think it was early 2009. So it would have been just after Onedotzero came
out and kind of just before 1.1 came out. So I went and saw this talk,
obviously I was doing a lot of web dev at the time.

I really liked Python and this was like, wow, now I can build websites
in Python. **This is the best thing I've ever seen**. So I went away
and kind of dabbled with it. Funnily enough, the very first bit of Django
that I use was inspect dB, which I imagine basically nobody uses.
Because what I did was, we had this kind of internal tool that was some
sort of inventory management system, which was like a PHP app that talked
to a MySQL database. And I kind of went, Oh, watch this, you know, and
went and pointed inspect dB at the database, generated all the models
wide up to the admin. And within about five minutes, I just had this like
working admin system for this for this app.

**And of course, everyone was very impressed**. And they all went, Wow,
well done. Let's get back to writing PHP. So, so yeah, I did that for
a while, played with Django in my spare time, you know, got a book about
it learned as much as I could. And then eventually, myself and a couple
of colleagues from from my first job decided that we were kind of going
to go off and do our own thing. Set up DevOps. From day one, it was it
was all about Django.

Our first project was, it was for a company who were a supplier of Tesco,
which I'm sure everyone's heard of. And so our very first project was
kind of quite high profile, very briefly featured on like the front page
of the BBC News. And it didn't fall over.

other supermarkets are available? Of course. Yeah. So it was briefly
quite high profile. And, you know, it worked really well and was really
successful project. So we kind of we got that we made a few more projects.
And then at some point, I'm trying to remember when it must have been
probably 2011.

Maybe I met :ref:`Tom <tom_christie>`, a Python meetup in a pub. And at the time,
I remember him saying, so I'm, I'm working on this this thing. It's kind
of it's like a framework for building rest API's in Django is like a
Django rest framework, but I'm really struggling to think of what to call it.
So I said, Well, you know, what about **Django rest framework ?**
And that's a good I bet, I bet that I do well, with like, SEO, you know, that'd be
nice and easily Google.

So, you know, step forward a few months, eventually, we ended up hiring
Tom, he was our first employee, after the three founders. And pick up.
Yeah, yeah. Yeah, now that it was really good, he was really kind of
crucial in shaping a lot of the early tech decisions at DevOps. Sure.

And he kind of, he was working on client projects for us. But at the same
time, he was also working on rest framework. And he, if you remember,
when, quite a while ago, he did a Kickstarter to fund some rest framework
development. And he did that work while he was working for us. And so we
used to kind of sit opposite each other and bounce ideas around.
And I think I'm kind of I don't, I haven't contributed a huge amount to
rest framework, but I think I'm responsible for, like serializer Method
field, the original version of that was me and like, you know, things
like that. So so so that was fun. So that was kind of back in the day.
And then, yeah, we sort of transitioned over to because originally, we
did, I guess you might call it a classic Django app where you do the
templates on the back end, you know, renders HTML on the back end.
That was, you know, for the first few years, that's what we did.
And then we moved over to this sort of API driven, react app type approach,
obviously, using using REST framework. And that's, that's what we've
been doing ever since really, although I've kind of recently got quite
interested in the whole on Poly HTML,
