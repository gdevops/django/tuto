.. index::
   ! Matt Layman

.. _matt_layman:

=======================================================================
**Matt Layman**
=======================================================================

- https://www.mattlayman.com
- https://x.com/mblayman
- https://www.mattlayman.com/understand-django

Introduction
================

I'm the software architect at @drondemand

- I like Python and organize @pythonfrederick
- I podcast on @djangoriffs
- I stream SaaS dev at http://twitch.tv/mblayman.
