
.. _psql_valentino:

===============================================
A PostgreSQL console cheat sheet (2020-03-16)
===============================================

.. seealso::

   - https://www.valentinog.com/blog/psql/




Introduction
==============

PostgreSQL is a fantastic database isn't it ?

However for all those starting out, PostgreSQL console could be confusing to
say the least.

Let's see in this cheat sheet what are the basic commands for moving your first
steps it the PostgreSQL console.

A PostgreSQL console cheat sheet: first steps
===============================================

::

    sudo su - postgres

::

    [sudo] Mot de passe de pvergain :

::

    Pas de répertoire, connexion avec HOME=/

::

    $ psql

::

    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

    postgres=#


A PostgreSQL console cheat sheet: interacting with databases
===============================================================

Once inside the PostgreSQL console you can interact with databases.

To create a new database::

    postgres=# create database family;

::

    CREATE DATABASE

To list all the databases in the server
----------------------------------------

::

    \l

::

    postgres=# \l
                                      Liste des bases de données
        Nom    | Propriétaire | Encodage | Collationnement | Type caract. |    Droits d'accès
    -----------+--------------+----------+-----------------+--------------+-----------------------
     db_family | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
     postgres  | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
     template0 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  | =c/postgres          +
               |              |          |                 |              | postgres=CTc/postgres
     template1 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  | =c/postgres          +
               |              |          |                 |              | postgres=CTc/postgres
    (4 lignes)


To connect to a database run
----------------------------

To connect to a database run::

    \c db_family


or::

    \connect db_family

::

    Vous êtes maintenant connecté à la base de données « db_family » en tant qu'utilisateur « postgres ».


You can also create a database with **createdb**
--------------------------------------------------

Note that outside the PostgreSQL console you can also create a database with
createdb::

    createdb db_test

::

    pvergain@pvergain-MS-7721 > createdb db_test

::

    createdb: erreur : n'a pas pu se connecter à la base de données template1 : FATAL:  le rôle « pvergain » n'existe pas

::

    sudo su - postgres

::

    [sudo] Mot de passe de pvergain :

::

    Pas de répertoire, connexion avec HOME=/

::

    $ createdb db_test
    $

::

    $ psql

::

    psql (12.2 (Ubuntu 12.2-2.pgdg18.04+1))
    Saisissez « help » pour l'aide.

::

    postgres=# \list

::

                                      Liste des bases de données
        Nom    | Propriétaire | Encodage | Collationnement | Type caract. |    Droits d'accès
    -----------+--------------+----------+-----------------+--------------+-----------------------
     db_family | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
     db_test   | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
     postgres  | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  |
     template0 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  | =c/postgres          +
               |              |          |                 |              | postgres=CTc/postgres
     template1 | postgres     | UTF8     | fr_FR.UTF-8     | fr_FR.UTF-8  | =c/postgres          +
               |              |          |                 |              | postgres=CTc/postgres
    (5 lignes)


A PostgreSQL console cheat sheet: interacting with tables
==============================================================

Once connected to a database you can list all its tables with::

    \dt
