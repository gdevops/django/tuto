

.. _alice_ridgeway_4_types_methods:

=================================================================
4 Types of Model Methods Django Beginners Should Know About
=================================================================

- https://ctrlzblog.com/4-types-of-model-methods-django-beginners-should-know-about/


Introduction
==============

- https://ctrlzblog.com/how-django-models-work-a-beginners-guide/
- https://github.com/ctrlz-blog/basic-django-blog/blob/code-freeze-4-model-methods-beginners-should-know-about/blog/models.py

Django models are not just for defining what columns will you into your
database table. As well as setting fields as attributes of the class,
you can also define methods.

This post will go through 4 kinds of methods you can use on your Django
models and when to use them. These are:

- Properties
- Overriding parent methods
- Custom methods
- Dunder methods

If you’re new to Django and want to build a better understanding of how
models work, I recommend reading my beginner’s `guide to Django models first <https://ctrlzblog.com/how-django-models-work-a-beginners-guide/>`_.

Adding methods to your models allow you to add custom functionality to
your model and keep your code DRY (Don’t Repeat Yourself).

All the examples in this post are for a model called **Post**, which is part
of a blogging application.

You can find the full code for the post in this `GitHub repository <https://github.com/ctrlz-blog/basic-django-blog/blob/code-freeze-4-model-methods-beginners-should-know-about/blog/models.py>`_
