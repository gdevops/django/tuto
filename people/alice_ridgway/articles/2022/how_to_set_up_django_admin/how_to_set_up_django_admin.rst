

.. _alice_ridgeway_2022_django_admin:

===================================================
How to Get Started with Django Admin
===================================================

- https://ctrlzblog.com/how-to-set-up-django-admin/

Introduction
============

From creating a superuser to basic customisation of the admin panel,
learn how to get started with Django Admin.


The admin panel is one Django’s most powerful features. It’s the place
where developers and administrators can interact with the database in a
visual way.

When you’re working on an enterprise scale codebase, I can’t stress
how useful having the panel is.
In other frameworks, you may need to build the UI for administrators
yourself, which can be very time consuming.

I’m going to cover the steps to get started with the admin panel, located
at localhost:8000/admin. This is going to involve creating a superuser
so you can log in; registering models, and adding some very basic customisation.
