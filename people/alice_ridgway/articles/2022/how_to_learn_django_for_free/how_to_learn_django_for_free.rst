

.. _alice_ridgeway_2022_how_to_learn:

===================================================
How to learn Django for free in 2022
===================================================


- https://ctrlzblog.com/how-to-learn-django-for-free-in-2022/


Introduction
==============

Today, I’m going to recommend some resources those of you who want to
learn Django for free.

These are the courses and videos I would pick if I were starting out
as a beginner. No affiliates or sponsorships here, just my personal favourites.

The trend I noticed when researching for this post is courses are abundant
for beginners but not for intermediate and advanced.

That is not to say that free resources are not out there.

Once you hit that intermediate level, you’ll be learning through short
topic-focused videos rather than courses.

I will only be recommending resources if either:

- I have used it myself and found it valuable
- I have reviewed the source-code.

First, learn some Python
=========================

Learn a bit of Python. You don’t have to become an expert before taking
up Django but learning the basics will go a long way.

I didn’t know much Python before starting out with Django so tried to
learn the two in parallel. I can say from experience that it is possible
but I found that my lack of Python experience slowed me down, particularly
when it came to debugging.

Learning some Python first will help you understand how Django works
and put you in a stronger position to debug your errors.

An underrated skill for beginner Django developers is understanding when
your error is a Python problem and when it is a Django problem.

Knowing the difference will make it easier for you to find the resources
you need.

Socratica
---------------

- https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-

`Socratica <https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er->`_ is hands-down the best YouTube channel for Python beginners
(in my opinion). The videos are short and well explained.

**It’s the channel I wish I had discovered when I was a beginner**.
I’ve since used it to fill in gaps in my knowledge.

Code Wars
==========

- https://www.codewars.com/

`Code Wars <https://www.codewars.com/>`_ is a great way for beginners to get familiar with the Python
syntax.
After each challenge, always look at the most popular solutions and
refactor– they will teach you a lot.

Your goal isn’t to work through all the levels. Instead, you’re aiming
to get familiar with basic Python data structures like lists and dictionaries.


5. Advanced Python
======================

- https://www.youtube.com/c/ArjanCodes/videos

As you gain experience as a developer and your projects get more complex,
success depends less on what you’re building but how.

Right now, I’m a big fan of Arjan Codes. A few months ago, I was assigned
to build a new tool at work with just one other developer.
I was given a lot of freedom to write the code how I wanted, so it was
important that I wrote clean code.

Complex projects involving data from multiple sources involve a lot of
Python. Arjan’s videos have helped me make more use of Object Orientated
Programming (OOP) and identify code smells.

This video on common Python code smells is a great way to learn a few
Python best practices:
