

.. _alice_ridgeway_2022_django_models:

===================================================
How Django Models Work – A Beginner’s Guide
===================================================

- https://ctrlzblog.com/how-django-models-work-a-beginners-guide/


Introduction
==============

Django models are used to manage your applications database without
writing SQL.

**The database is the core of any Django application**.

However, the language of databases (well… most of them) is SQL, not Python.

Something that makes Django very powerful is it allows you to build very
complex data-driven web applications without having to write a line of SQL.

To do that, Django needs a way of converting the developer’s Python
code into SQL commands that the database will understand.

We need models. This post will go through how Django models work, so you
can start coding data-driven apps.
