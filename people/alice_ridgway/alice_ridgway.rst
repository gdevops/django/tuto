
.. index::
   pair: People ; Alice Ridgway
   ! Alice Ridgway


.. _alice_ridgeway:

===================================================
**Alice Ridgway**
===================================================


- https://x.com/aliceridgway404
- https://ctrlzblog.com/

.. toctree::
   :maxdepth: 3

   articles/articles
