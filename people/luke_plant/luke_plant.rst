.. index::
   pair: People ; Luke Plant

.. _luke_plant:

===================================================
**Luke Plant**
===================================================


- https://lukeplant.me.uk/personal.html
- https://x.com/spookylukey


Articles 2020
===============

.. seealso::

   - https://lukeplant.me.uk/blog/posts/test-smarter-not-harder/
