
.. index::
   pair: Tips ; settings


.. _settings_tips:

============================================
Django settings tips
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/settings



Tips1
=======


.. code-block:: python
   :linenos:


    from django.conf import settings

    if settings.DEBUG:
        # pour les tests, à placer après le 2e ADRESSES_COURIEL
        ADRESSES_COURIEL = {
            "achat": ["patrick.vergain@xxx.yy"],
            "nomenclature": ["patrick.vergain@xxx.yy"],
            "service": ["patrick.vergain@xxx.yy"],
            "sortie": ["patrick.vergain@xxx.yy"],
        }


Tips2
========


.. code-block:: python
   :linenos:


    """conftest.py


    For information, we could also use this code::

        os.environ[
            "DATABASE_URL"
        ] = "postgresql://<username>:<password>@X.Y.Z.T:5432/db_xxxxxx"
        DATABASES = {"default": env.db("DATABASE_URL")}


        # on corrige par anticipation car django-environ fait pointer sur
        # 'django.db.backends.postgresql_psycopg2' au lieu de 'django.db.backends.postgresql'
        # A ce jour (2017-12-01 les 2 modules existent.
        # https://docs.docker.com/compose/django/#connect-the-database

        DATABASES["default"]["ENGINE"] = "django.db.backends.postgresql"


    Calling pytest::

        pytest --cov='.'

    """

    import pytest
    from django.conf import settings

    # http://github.com/joke2k/django-environ
    # import environ

    @pytest.fixture(scope="session")
    def django_db_setup():

        settings.DATABASES["default"] = {
            "ENGINE": "django.db.backends.postgresql",
            # Test database
            "HOST": "X.Y.Z.T",
            "NAME": "db_xxx",
        }
