
.. index::
   pair: Django ; settings


.. _django_settings:

============================================
Django settings
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/settings


.. toctree::
   :maxdepth: 3


   tips/tips
