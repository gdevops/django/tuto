
.. index::
   pair: Django ; contrib


.. _django_contrib:

============================================
Django contrib
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib


.. toctree::
   :maxdepth: 3


   staticfiles/staticfiles
