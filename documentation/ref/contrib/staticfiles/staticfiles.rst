.. index::
   pair: Django ; static files
   pair: rahmonov ; static files
   ! static files
   ! STATIC_ROOT

.. _django_static_files:

============================================
Django staticfiles
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/

.. toctree::
   :maxdepth: 3

   learndjango/learndjango
   mattlayman/mattlayman
   rahmonov/rahmonov
