.. index::
   pair: Static files; Matt Layman


.. _serving_static_files:

=======================================================================
Serving Static Files by **Matt Layman**
=======================================================================

.. seealso::

   - https://www.mattlayman.com/understand-django
   - https://www.mattlayman.com/understand-django/serving-static-files/



Introduction
==============

In the previous Understand Django article, I described how Django gives
us tools to run code for any request using the middleware system.

Our next focus will be on static files. Static files are vital to your
application, but they have little to do with Python code.

We’ll see what they are and what they do.
