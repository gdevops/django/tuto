.. index::
   pair: Django ; ref

.. _django_ref_models:

============================================
**Django ref models**
============================================

- https://docs.djangoproject.com/en/dev/ref/models
- https://github.com/django/django/blob/master/docs/ref/models


.. toctree::
   :maxdepth: 3


   fields/fields
   indexes/indexes
   constraints/constraints
   options/options
   querysets/querysets
