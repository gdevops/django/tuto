
.. index::
   pair: Tests ; indexes

.. _tests_models_indexes:

============================================
Tests **indexes**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models/indexes
   - https://github.com/django/django/blob/master/docs/ref/models/indexes.txt
   - https://github.com/django/django/blob/master/tests






models.py
=========

.. literalinclude:: models.py
   :linenos:


tests.py
=========

.. literalinclude:: tests.py
   :linenos:
