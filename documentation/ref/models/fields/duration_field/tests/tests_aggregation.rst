
.. _tests_aggregation_duration_field:

============================================
**Tests aggregation DurationField**
============================================




Classe Publisher
=================

.. code-block:: python
   :linenos:

    class Publisher(models.Model):
        name = models.CharField(max_length=255)
        num_awards = models.IntegerField()
        duration = models.DurationField(blank=True, null=True)

        def __str__(self):
            return self.name


class AggregateTestCase(TestCase)
=======================================


.. code-block:: python
   :linenos:


    import datetime
    import re
    from decimal import Decimal

    from django.core.exceptions import FieldError
    from django.db import connection
    from django.db.models import (
        Avg, Case, Count, DecimalField, DurationField, Exists, F, FloatField, Func,
        IntegerField, Max, Min, OuterRef, Subquery, Sum, Value, When,
    )
    from django.db.models.functions import Coalesce
    from django.test import TestCase
    from django.test.testcases import skipUnlessDBFeature
    from django.test.utils import Approximate, CaptureQueriesContext
    from django.utils import timezone

    from .models import Author, Book, Publisher, Store


    class AggregateTestCase(TestCase):

        @classmethod
        def setUpTestData(cls):
            cls.a1 = Author.objects.create(name='Adrian Holovaty', age=34)
            cls.a2 = Author.objects.create(name='Jacob Kaplan-Moss', age=35)
            cls.a3 = Author.objects.create(name='Brad Dayley', age=45)
            cls.a4 = Author.objects.create(name='James Bennett', age=29)
            cls.a5 = Author.objects.create(name='Jeffrey Forcier', age=37)
            cls.a6 = Author.objects.create(name='Paul Bissex', age=29)
            cls.a7 = Author.objects.create(name='Wesley J. Chun', age=25)
            cls.a8 = Author.objects.create(name='Peter Norvig', age=57)
            cls.a9 = Author.objects.create(name='Stuart Russell', age=46)
            cls.a1.friends.add(cls.a2, cls.a4)
            cls.a2.friends.add(cls.a1, cls.a7)
            cls.a4.friends.add(cls.a1)
            cls.a5.friends.add(cls.a6, cls.a7)
            cls.a6.friends.add(cls.a5, cls.a7)
            cls.a7.friends.add(cls.a2, cls.a5, cls.a6)
            cls.a8.friends.add(cls.a9)
            cls.a9.friends.add(cls.a8)

            cls.p1 = Publisher.objects.create(name='Apress', num_awards=3, duration=datetime.timedelta(days=1))
            cls.p2 = Publisher.objects.create(name='Sams', num_awards=1, duration=datetime.timedelta(days=2))
            cls.p3 = Publisher.objects.create(name='Prentice Hall', num_awards=7)
            cls.p4 = Publisher.objects.create(name='Morgan Kaufmann', num_awards=9)
            cls.p5 = Publisher.objects.create(name="Jonno's House of Books", num_awards=0)



test_avg_duration_field
==========================

.. code-block:: python
   :linenos:

    def test_avg_duration_field(self):
        # Explicit `output_field`.
        self.assertEqual(
            Publisher.objects.aggregate(Avg('duration', output_field=DurationField())),
            {'duration__avg': datetime.timedelta(days=1, hours=12)}
        )
        # Implicit `output_field`.
        self.assertEqual(
            Publisher.objects.aggregate(Avg('duration')),
            {'duration__avg': datetime.timedelta(days=1, hours=12)}
        )


test_sum_duration_field
==========================

.. code-block:: python
   :linenos:

    def test_sum_duration_field(self):
        self.assertEqual(
            Publisher.objects.aggregate(Sum('duration', output_field=DurationField())),
            {'duration__sum': datetime.timedelta(days=3)}
        )
