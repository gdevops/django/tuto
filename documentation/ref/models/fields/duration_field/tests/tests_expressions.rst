.. index::
   pair: Tests ; DurationField

.. _tests_expressions_duration_field:

============================================
**Tests expressions DurationField**
============================================




Classe Experiment
=================

.. code-block:: python
   :linenos:

    class Experiment(models.Model):
        name = models.CharField(max_length=24)
        assigned = models.DateField()
        completed = models.DateField()
        estimated_time = models.DurationField()
        start = models.DateTimeField()
        end = models.DateTimeField()

        class Meta:
            db_table = 'expressions_ExPeRiMeNt'
            ordering = ('name',)

        def duration(self):
            return self.end - self.start

Classe Time
=================

.. code-block:: python
   :linenos:


    class Time(models.Model):
        time = models.TimeField(null=True)

        def __str__(self):
            return "%s" % self.time


class FTimeDeltaTests(TestCase)
===================================

.. code-block:: python
   :linenos:


    import datetime
    import pickle
    import unittest
    import uuid
    from copy import deepcopy
    from unittest import mock

    from django.core.exceptions import FieldError
    from django.db import DatabaseError, connection
    from django.db.models import (
        Avg, BooleanField, Case, CharField, Count, DateField, DateTimeField,
        DurationField, Exists, Expression, ExpressionList, ExpressionWrapper, F,
        Func, IntegerField, Max, Min, Model, OrderBy, OuterRef, Q, StdDev,
        Subquery, Sum, TimeField, UUIDField, Value, Variance, When,
    )
    from django.db.models.expressions import Col, Combinable, Random, RawSQL, Ref
    from django.db.models.functions import (
        Coalesce, Concat, Length, Lower, Substr, Upper,
    )
    from django.db.models.sql import constants
    from django.db.models.sql.datastructures import Join
    from django.test import SimpleTestCase, TestCase, skipUnlessDBFeature
    from django.test.utils import Approximate, isolate_apps

    from .models import (
        UUID, UUIDPK, Company, Employee, Experiment, Number, RemoteEmployee,
        Result, SimulationRun, Time,
    )


    class FTimeDeltaTests(TestCase):

        @classmethod
        def setUpTestData(cls):
            cls.sday = sday = datetime.date(2010, 6, 25)
            cls.stime = stime = datetime.datetime(2010, 6, 25, 12, 15, 30, 747000)
            midnight = datetime.time(0)

            delta0 = datetime.timedelta(0)
            delta1 = datetime.timedelta(microseconds=253000)
            delta2 = datetime.timedelta(seconds=44)
            delta3 = datetime.timedelta(hours=21, minutes=8)
            delta4 = datetime.timedelta(days=10)
            delta5 = datetime.timedelta(days=90)

            # Test data is set so that deltas and delays will be
            # strictly increasing.
            cls.deltas = []
            cls.delays = []
            cls.days_long = []

            # e0: started same day as assigned, zero duration
            end = stime + delta0
            e0 = Experiment.objects.create(
                name='e0', assigned=sday, start=stime, end=end,
                completed=end.date(), estimated_time=delta0,
            )
            cls.deltas.append(delta0)
            cls.delays.append(e0.start - datetime.datetime.combine(e0.assigned, midnight))
            cls.days_long.append(e0.completed - e0.assigned)

            # e1: started one day after assigned, tiny duration, data
            # set so that end time has no fractional seconds, which
            # tests an edge case on sqlite.
            delay = datetime.timedelta(1)
            end = stime + delay + delta1
            e1 = Experiment.objects.create(
                name='e1', assigned=sday, start=stime + delay, end=end,
                completed=end.date(), estimated_time=delta1,
            )
            cls.deltas.append(delta1)
            cls.delays.append(e1.start - datetime.datetime.combine(e1.assigned, midnight))
            cls.days_long.append(e1.completed - e1.assigned)

            # e2: started three days after assigned, small duration
            end = stime + delta2
            e2 = Experiment.objects.create(
                name='e2', assigned=sday - datetime.timedelta(3), start=stime,
                end=end, completed=end.date(), estimated_time=datetime.timedelta(hours=1),
            )
            cls.deltas.append(delta2)
            cls.delays.append(e2.start - datetime.datetime.combine(e2.assigned, midnight))
            cls.days_long.append(e2.completed - e2.assigned)

            # e3: started four days after assigned, medium duration
            delay = datetime.timedelta(4)
            end = stime + delay + delta3
            e3 = Experiment.objects.create(
                name='e3', assigned=sday, start=stime + delay, end=end,
                completed=end.date(), estimated_time=delta3,
            )
            cls.deltas.append(delta3)
            cls.delays.append(e3.start - datetime.datetime.combine(e3.assigned, midnight))
            cls.days_long.append(e3.completed - e3.assigned)

            # e4: started 10 days after assignment, long duration
            end = stime + delta4
            e4 = Experiment.objects.create(
                name='e4', assigned=sday - datetime.timedelta(10), start=stime,
                end=end, completed=end.date(), estimated_time=delta4 - datetime.timedelta(1),
            )
            cls.deltas.append(delta4)
            cls.delays.append(e4.start - datetime.datetime.combine(e4.assigned, midnight))
            cls.days_long.append(e4.completed - e4.assigned)

            # e5: started a month after assignment, very long duration
            delay = datetime.timedelta(30)
            end = stime + delay + delta5
            e5 = Experiment.objects.create(
                name='e5', assigned=sday, start=stime + delay, end=end,
                completed=end.date(), estimated_time=delta5,
            )
            cls.deltas.append(delta5)
            cls.delays.append(e5.start - datetime.datetime.combine(e5.assigned, midnight))
            cls.days_long.append(e5.completed - e5.assigned)

            cls.expnames = [e.name for e in Experiment.objects.all()]


test_durationfield_add
=======================

.. code-block:: python
   :linenos:

    def test_durationfield_add(self):
        zeros = [e.name for e in Experiment.objects.filter(start=F('start') + F('estimated_time'))]
        self.assertEqual(zeros, ['e0'])

        end_less = [e.name for e in Experiment.objects.filter(end__lt=F('start') + F('estimated_time'))]
        self.assertEqual(end_less, ['e2'])

        delta_math = [
            e.name for e in
            Experiment.objects.filter(end__gte=F('start') + F('estimated_time') + datetime.timedelta(hours=1))
        ]
        self.assertEqual(delta_math, ['e4'])

        queryset = Experiment.objects.annotate(shifted=ExpressionWrapper(
            F('start') + Value(None, output_field=DurationField()),
            output_field=DateTimeField(),
        ))
        self.assertIsNone(queryset.first().shifted)




test_date_subtraction
=======================

.. code-block:: python
   :linenos:

    @skipUnlessDBFeature('supports_temporal_subtraction')
    def test_date_subtraction(self):
        queryset = Experiment.objects.annotate(
            completion_duration=ExpressionWrapper(
                F('completed') - F('assigned'), output_field=DurationField()
            )
        )

        at_least_5_days = {e.name for e in queryset.filter(completion_duration__gte=datetime.timedelta(days=5))}
        self.assertEqual(at_least_5_days, {'e3', 'e4', 'e5'})

        at_least_120_days = {e.name for e in queryset.filter(completion_duration__gte=datetime.timedelta(days=120))}
        self.assertEqual(at_least_120_days, {'e5'})

        less_than_5_days = {e.name for e in queryset.filter(completion_duration__lt=datetime.timedelta(days=5))}
        self.assertEqual(less_than_5_days, {'e0', 'e1', 'e2'})

        queryset = Experiment.objects.annotate(difference=ExpressionWrapper(
            F('completed') - Value(None, output_field=DateField()),
            output_field=DurationField(),
        ))
        self.assertIsNone(queryset.first().difference)

        queryset = Experiment.objects.annotate(shifted=ExpressionWrapper(
            F('completed') - Value(None, output_field=DurationField()),
            output_field=DateField(),
        ))
        self.assertIsNone(queryset.first().shifted)


test_date_subquery_subtraction
==================================

.. code-block:: python
   :linenos:

    @skipUnlessDBFeature('supports_temporal_subtraction')
    def test_date_subquery_subtraction(self):
        subquery = Experiment.objects.filter(pk=OuterRef('pk')).values('completed')
        queryset = Experiment.objects.annotate(
            difference=ExpressionWrapper(
                subquery - F('completed'), output_field=DurationField(),
            ),
        ).filter(difference=datetime.timedelta())
        self.assertTrue(queryset.exists())


test_time_subquery_subtraction
==================================

.. code-block:: python
   :linenos:

    @skipUnlessDBFeature('supports_temporal_subtraction')
    def test_time_subquery_subtraction(self):
        Time.objects.create(time=datetime.time(12, 30, 15, 2345))
        subquery = Time.objects.filter(pk=OuterRef('pk')).values('time')
        queryset = Time.objects.annotate(
            difference=ExpressionWrapper(
                subquery - F('time'), output_field=DurationField(),
            ),
        ).filter(difference=datetime.timedelta())
        self.assertTrue(queryset.exists())


test_datetime_subtraction_microseconds
=========================================

.. code-block:: python
   :linenos:

    @skipUnlessDBFeature('supports_temporal_subtraction')
    def test_datetime_subtraction_microseconds(self):
        delta = datetime.timedelta(microseconds=8999999999999999)
        Experiment.objects.update(end=F('start') + delta)
        qs = Experiment.objects.annotate(
            delta=ExpressionWrapper(F('end') - F('start'), output_field=DurationField())
        )
        for e in qs:
            self.assertEqual(e.delta, delta)


test_duration_with_datetime
==================================

.. code-block:: python
   :linenos:

    def test_duration_with_datetime(self):
        # Exclude e1 which has very high precision so we can test this on all
        # backends regardless of whether or not it supports
        # microsecond_precision.
        over_estimate = Experiment.objects.exclude(name='e1').filter(
            completed__gt=self.stime + F('estimated_time'),
        ).order_by('name')
        self.assertQuerysetEqual(over_estimate, ['e3', 'e4', 'e5'], lambda e: e.name)

test_duration_with_datetime_microseconds
============================================

.. code-block:: python
   :linenos:

    def test_duration_with_datetime_microseconds(self):
        delta = datetime.timedelta(microseconds=8999999999999999)
        qs = Experiment.objects.annotate(dt=ExpressionWrapper(
            F('start') + delta,
            output_field=DateTimeField(),
        ))
        for e in qs:
            self.assertEqual(e.dt, e.start + delta)


test_date_minus_duration
==================================

.. code-block:: python
   :linenos:

    def test_date_minus_duration(self):
        more_than_4_days = Experiment.objects.filter(
            assigned__lt=F('completed') - Value(datetime.timedelta(days=4), output_field=DurationField())
        )
        self.assertQuerysetEqual(more_than_4_days, ['e3', 'e4', 'e5'], lambda e: e.name)


test_negative_timedelta_update
==================================

.. code-block:: python
   :linenos:

    def test_negative_timedelta_update(self):
        # subtract 30 seconds, 30 minutes, 2 hours and 2 days
        experiments = Experiment.objects.filter(name='e0').annotate(
            start_sub_seconds=F('start') + datetime.timedelta(seconds=-30),
        ).annotate(
            start_sub_minutes=F('start_sub_seconds') + datetime.timedelta(minutes=-30),
        ).annotate(
            start_sub_hours=F('start_sub_minutes') + datetime.timedelta(hours=-2),
        ).annotate(
            new_start=F('start_sub_hours') + datetime.timedelta(days=-2),
        )
        expected_start = datetime.datetime(2010, 6, 23, 9, 45, 0)
        # subtract 30 microseconds
        experiments = experiments.annotate(new_start=F('new_start') + datetime.timedelta(microseconds=-30))
        expected_start += datetime.timedelta(microseconds=+746970)
        experiments.update(start=F('new_start'))
        e0 = Experiment.objects.get(name='e0')
        self.assertEqual(e0.start, expected_start)
