
.. _tests_model_duration_field:

============================================
**Tests model DurationField**
============================================




Classe DurationModel
======================

.. code-block:: python

    class DurationModel(models.Model):
        field = models.DurationField()


Classe NullDurationModel
============================

.. code-block:: python


    class NullDurationModel(models.Model):
        field = models.DurationField(null=True)


Classe VerboseNameField
============================

.. code-block:: python
   :linenos:


    class VerboseNameField(models.Model):
        id = models.AutoField("verbose pk", primary_key=True)
        field1 = models.BigIntegerField("verbose field1")
        field2 = models.BooleanField("verbose field2", default=False)
        field3 = models.CharField("verbose field3", max_length=10)
        field4 = models.DateField("verbose field4")
        field5 = models.DateTimeField("verbose field5")
        field6 = models.DecimalField("verbose field6", max_digits=6, decimal_places=1)
        field7 = models.EmailField("verbose field7")
        field8 = models.FileField("verbose field8", upload_to="unused")
        field9 = models.FilePathField("verbose field9")
        field10 = models.FloatField("verbose field10")
        # Don't want to depend on Pillow in this test
        # field_image = models.ImageField("verbose field")
        field11 = models.IntegerField("verbose field11")
        field12 = models.GenericIPAddressField("verbose field12", protocol="ipv4")
        field13 = models.NullBooleanField("verbose field13")
        field14 = models.PositiveIntegerField("verbose field14")
        field15 = models.PositiveSmallIntegerField("verbose field15")
        field16 = models.SlugField("verbose field16")
        field17 = models.SmallIntegerField("verbose field17")
        field18 = models.TextField("verbose field18")
        field19 = models.TimeField("verbose field19")
        field20 = models.URLField("verbose field20")
        field21 = models.UUIDField("verbose field21")
        field22 = models.DurationField("verbose field22")


class BasicFieldTests(SimpleTestCase)
=========================================


.. code-block:: python
   :linenos:


    import pickle

    from django import forms
    from django.core.exceptions import ValidationError
    from django.db import models
    from django.test import SimpleTestCase, TestCase
    from django.utils.functional import lazy

    from .models import (
        Bar, Choiceful, Foo, RenamedField, VerboseNameField, Whiz, WhizDelayed,
        WhizIter, WhizIterEmpty,
    )


test_model_duration_field.py
===============================

.. literalinclude:: test_form_durationfield.py
   :linenos:
