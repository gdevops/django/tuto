
.. _tests_annotation_duration_field:

============================================
**Tests annotation DurationField**
============================================




Classe Ticket
=================

.. code-block:: python
   :linenos:

    class Ticket(models.Model):
        active_at = models.DateTimeField()
        duration = models.DurationField()

        def __str__(self):
            return '{} - {}'.format(self.active_at, self.duration)


class NonAggregateAnnotationTestCase(TestCase)
==================================================


.. code-block:: python
   :linenos:


    import datetime
    from decimal import Decimal

    from django.core.exceptions import FieldDoesNotExist, FieldError
    from django.db.models import (
        BooleanField, CharField, Count, DateTimeField, ExpressionWrapper, F, Func,
        IntegerField, NullBooleanField, OuterRef, Q, Subquery, Sum, Value,
    )
    from django.db.models.expressions import RawSQL
    from django.db.models.functions import Length, Lower
    from django.test import TestCase, skipUnlessDBFeature

    from .models import (
        Author, Book, Company, DepartmentStore, Employee, Publisher, Store, Ticket,
    )


    def cxOracle_py3_bug(func):
        """
        There's a bug in Django/cx_Oracle with respect to string handling under
        Python 3 (essentially, they treat Python 3 strings as Python 2 strings
        rather than unicode). This makes some tests here fail under Python 3, so
        we mark them as expected failures until someone fixes them in #23843.
        """
        from unittest import expectedFailure
        from django.db import connection
        return expectedFailure(func) if connection.vendor == 'oracle' else func


    class NonAggregateAnnotationTestCase(TestCase):

        @classmethod
        def setUpTestData(cls):
            cls.a1 = Author.objects.create(name='Adrian Holovaty', age=34)
            cls.a2 = Author.objects.create(name='Jacob Kaplan-Moss', age=35)
            cls.a3 = Author.objects.create(name='Brad Dayley', age=45)
            cls.a4 = Author.objects.create(name='James Bennett', age=29)
            cls.a5 = Author.objects.create(name='Jeffrey Forcier', age=37)
            cls.a6 = Author.objects.create(name='Paul Bissex', age=29)
            cls.a7 = Author.objects.create(name='Wesley J. Chun', age=25)
            cls.a8 = Author.objects.create(name='Peter Norvig', age=57)
            cls.a9 = Author.objects.create(name='Stuart Russell', age=46)


test_mixed_type_annotation_date_interval
==================================================

.. code-block:: python
   :linenos:

    def test_mixed_type_annotation_date_interval(self):
        active = datetime.datetime(2015, 3, 20, 14, 0, 0)
        duration = datetime.timedelta(hours=1)
        expires = datetime.datetime(2015, 3, 20, 14, 0, 0) + duration
        Ticket.objects.create(active_at=active, duration=duration)
        t = Ticket.objects.annotate(
            expires=ExpressionWrapper(F('active_at') + F('duration'), output_field=DateTimeField())
        ).first()
        self.assertEqual(t.expires, expires)
