.. index::
   pair: Django type ; DurationField
   pair: Duration ; Field

.. _django_duration_field:
.. _duration_field:

============================================
**DurationField**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models/fields/#durationfield
   - https://github.com/django/django/blob/master/docs/ref/models/fields.txt
   - https://docs.djangoproject.com/fr/3.0/ref/models/fields/#durationfield
   - https://docs.python.org/3/library/datetime.html#datetime.timedelta
   - https://stackoverflow.com/questions/30222660/how-should-i-use-durationfield-in-my-model





``DurationField``
===================

.. class:: DurationField(**options)

A field for storing periods of time - modeled in Python by
:class:`~python:datetime.timedelta`.

When used on PostgreSQL, the data type used is an ``interval`` and on Oracle the
data type is ``INTERVAL DAY(9) TO SECOND(6)``.
Otherwise a ``bigint`` of microseconds is used.

.. note::

    Arithmetic with ``DurationField`` works in most cases.
    However on all databases **other than PostgreSQL**, comparing the value
    of a ``DurationField`` to arithmetic on ``DateTimeField`` instances will not
    work as expected.



Description
=============

Un champ pour stocker des périodes de temps, représentées en Python par des
objets timedelta_.

Avec PostgreSQL, le type de données utilisé est un interval et avec Oracle, le
type de données est INTERVAL DAY(9) TO SECOND(6).

Sinon, c’est un grand nombre entier bigint de microsecondes qui est utilisé.


.. warning::  L’arithmétique des champs DurationField fonctionne la plupart du temps.
   Cependant, pour toutes les bases de données autres que PostgreSQL, la
   comparaison de valeurs d’un champ DurationField avec l’arithmétique des
   instances DateTimeField ne fonctionne pas comme on pourrait l’espérer.


.. _timedelta: https://docs.python.org/3/library/datetime.html#datetime.timedelta


Timedelta examples
--------------------

::

    In [1]: from datetime import datetime, timedelta
    In [2]: one_day = timedelta(days=1)
    In [3]: one_day

::

    Out[3]: datetime.timedelta(days=1)

PostgreSQL interval data type
================================

.. seealso::

   - :ref:`postgresql_interval`


Tests DurationField
====================

.. toctree::
   :maxdepth: 3

   tests/tests
