
.. index::
   pair: ref ; models fields


.. _django_ref_models_fields:

============================================
Django ref models fields
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/models


.. toctree::
   :maxdepth: 3


   duration_field/duration_field
