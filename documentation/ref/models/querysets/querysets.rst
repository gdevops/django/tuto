
.. index::
   pair: exists ; querysets
   ! querysets


.. _django_ref_models_querysets:

============================================
Django ref models **querysets**
============================================


- https://docs.djangoproject.com/fr/4.1/ref/models/querysets/#django.db.models.query.QuerySet


exists()
==========

- https://docs.djangoproject.com/fr/4.1/ref/models/querysets/#django.db.models.query.QuerySet.exists

Renvoie True si le QuerySet contient au moins un résultat, sinon False.

Cette méthode s’efforce de lancer la requête la plus simple et rapide
possible, mais la requête exécutée est quasi identique à celle du QuerySet normal.

exists() est utile pour des recherches liées à l’existence d’au moins un
objet dans un QuerySet, particulièrement dans le contexte de QuerySet volumineux.

Pour savoir si un jeu de requête contient au moins un élément::

    if some_queryset.exists():
        print("There is at least one object in some_queryset")

Ce qui sera plus rapide que::

    if some_queryset:
        print("There is at least one object in some_queryset")

…mais pas de beaucoup (plus le jeu de requête est potentiellement volumineux,
plus le gain sera élevé).

De plus, si some_queryset n’a pas encore été évalué mais que vous savez
qu’il le sera à un moment donné, l’appel à some_queryset.exists()
effectue du travail en plus (une requête pour le contrôle d’existence,
plus une autre plus tard au moment de charger les résultats) par rapport
à bool(some_queryset), qui récupère les résultats et vérifie ensuite
s’il y en a au moins un.
