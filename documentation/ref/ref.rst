
.. index::
   pair: Django ; ref


.. _django_ref:

============================================
Django ref
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref
   - https://github.com/django/django/blob/master/docs/ref



.. toctree::
   :maxdepth: 3


   contrib/contrib
   models/models
   settings/settings
