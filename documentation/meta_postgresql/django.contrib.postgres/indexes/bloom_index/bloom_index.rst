.. index::
   pair: django.contrib.postgres.indexes; BloomIndex
   ! BloomIndex
   ! Bloom filters

.. _django_bloom_index:

============================================
**BloomIndex**
============================================

.. seealso::

   - https://en.wikipedia.org/wiki/Bloom_filter
   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/indexes/#django.contrib.postgres.indexes.BloomIndex
   - :ref:`postgresql_bloom_index`



Description
============

Creates a bloom index.

To use this index access you need to activate the bloom extension on PostgreSQL.
You can install it using the BloomExtension migration operation.

Provide an integer number of bits from 1 to 4096 to the length parameter to
specify the length of each index entry. PostgreSQL’s default is 80.

The columns argument takes a tuple or list of up to 32 values that are integer
number of bits from 1 to 4095.
