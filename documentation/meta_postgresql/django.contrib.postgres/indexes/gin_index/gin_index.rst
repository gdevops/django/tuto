.. index::
   pair: django.contrib.postgres.indexes; GINIndex
   ! GIN index

.. _django_gin_index:

============================================
**GinIndex**
============================================

.. seealso::

   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/indexes/#django.contrib.postgres.indexes.GinIndex
   - :ref:`postgresql_bloom_index`
