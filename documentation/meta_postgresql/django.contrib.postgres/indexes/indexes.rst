.. index::
   pair: django.contrib.postgres ; indexes

.. _django_postgres_indexes:

============================================
**django.contrib.postgres.indexes**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/indexes/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/indexes.txt

.. toctree::
   :maxdepth: 3

   bloom_index/bloom_index
   gin_index/gin_index
   gist_index/gist_index
