.. index::
   pair: Contrib ; django.contrib.postgres

.. _django_postgresql:

============================================
**django.contrib.postgres**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/search/?q=postgresql
   - https://github.com/django/django/tree/master/django/contrib/postgres
   - https://github.com/django/django/tree/master/docs/ref/contrib/postgres
   - https://github.com/django/django/tree/master/tests/postgres_tests


.. toctree::
   :maxdepth: 3

   fields/fields
   indexes/indexes
   search/search
   versions/versions
