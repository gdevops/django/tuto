.. index::
   pair: django.contrib.postgres.fields ; ArrayField
   ! ArrayField

.. _django_array_field:

===================================================
**ArrayField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#arrayfield
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - :ref:`postgresql_array`




Description
=============

**A field for storing lists of data**.

Most field types can be used, and you pass another field instance as the base_field.

You may also specify a size. **ArrayField can be nested** to store multi-dimensional arrays.

If you give the field a default, ensure it’s a callable such as list (for an
empty default) or a callable that returns a list (such as a function).

Incorrectly using default=[] creates a mutable default that is shared between
all instances of ArrayField.


.. _array_field_tags_example:

Querying ``ArrayField`` (tags example)
========================================

There are a number of custom lookups and transforms for :class:`ArrayField`.
We will use the following example model::

    from django.contrib.postgres.fields import ArrayField
    from django.db import models

    class Post(models.Model):
        name = models.CharField(max_length=200)
        tags = ArrayField(models.CharField(max_length=200), blank=True)

        def __str__(self):
            return self.name



.. _django_array_field_contains:

``contains``
----------------

The *contains* lookup is overridden on :class:`ArrayField`. The
returned objects will be those where the values passed are a subset of the
data. It uses the SQL operator ``@>``. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])
    >>> Post.objects.create(name='Third post', tags=['tutorial', 'django'])

    >>> Post.objects.filter(tags__contains=['thoughts'])
    <QuerySet [<Post: First post>, <Post: Second post>]>

    >>> Post.objects.filter(tags__contains=['django'])
    <QuerySet [<Post: First post>, <Post: Third post>]>

    >>> Post.objects.filter(tags__contains=['django', 'thoughts'])
    <QuerySet [<Post: First post>]>


``contained_by``
----------------

This is the inverse of the `contains <arrayfield.contains>` lookup -
the objects returned will be those where the data is a subset of the values
passed. It uses the SQL operator ``<@``. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])
    >>> Post.objects.create(name='Third post', tags=['tutorial', 'django'])

    >>> Post.objects.filter(tags__contained_by=['thoughts', 'django'])
    <QuerySet [<Post: First post>, <Post: Second post>]>

    >>> Post.objects.filter(tags__contained_by=['thoughts', 'django', 'tutorial'])
    <QuerySet [<Post: First post>, <Post: Second post>, <Post: Third post>]>


``overlap``
------------

Returns objects where the data shares any results with the values passed. Uses
the SQL operator ``&&``. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])
    >>> Post.objects.create(name='Third post', tags=['tutorial', 'django'])

    >>> Post.objects.filter(tags__overlap=['thoughts'])
    <QuerySet [<Post: First post>, <Post: Second post>]>

    >>> Post.objects.filter(tags__overlap=['thoughts', 'tutorial'])
    <QuerySet [<Post: First post>, <Post: Second post>, <Post: Third post>]>


``len``
----------

Returns the length of the array. The lookups available afterwards are those
available for :class:`~django.db.models.IntegerField`. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])

    >>> Post.objects.filter(tags__len=1)
    <QuerySet [<Post: Second post>]>


Index transforms
-------------------

Index transforms index into the array. Any non-negative integer can be used.
There are no errors if it exceeds the :attr:`size <ArrayField.size>` of the
array. The lookups available after the transform are those from the
:attr:`base_field <ArrayField.base_field>`. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])

    >>> Post.objects.filter(tags__0='thoughts')
    <QuerySet [<Post: First post>, <Post: Second post>]>

    >>> Post.objects.filter(tags__1__iexact='Django')
    <QuerySet [<Post: First post>]>

    >>> Post.objects.filter(tags__276='javascript')
    <QuerySet []>

.. note::

    PostgreSQL uses 1-based indexing for array fields when writing raw SQL.
    However these indexes and those used in `slices <arrayfield.slice>`
    use 0-based indexing to be consistent with Python.


Slice transforms
-------------------

Slice transforms take a slice of the array. Any two non-negative integers can
be used, separated by a single underscore. The lookups available after the
transform do not change. For example::

    >>> Post.objects.create(name='First post', tags=['thoughts', 'django'])
    >>> Post.objects.create(name='Second post', tags=['thoughts'])
    >>> Post.objects.create(name='Third post', tags=['django', 'python', 'thoughts'])

    >>> Post.objects.filter(tags__0_1=['thoughts'])
    <QuerySet [<Post: First post>, <Post: Second post>]>

    >>> Post.objects.filter(tags__0_2__contains=['thoughts'])
    <QuerySet [<Post: First post>, <Post: Second post>]>

.. note::

    PostgreSQL uses 1-based indexing for array fields when writing raw SQL.
    However these slices and those used in indexes <arrayfield.index>`
    use 0-based indexing to be consistent with Python.

.. admonition:: Multidimensional arrays with indexes and slices

    PostgreSQL has some rather esoteric behavior when using indexes and slices
    on multidimensional arrays. It will always work to use indexes to reach
    down to the final underlying data, but most other slices behave strangely
    at the database level and cannot be supported in a logical, consistent
    fashion by Django.

Tests
======

.. toctree::
   :maxdepth: 3

   tests/tests
