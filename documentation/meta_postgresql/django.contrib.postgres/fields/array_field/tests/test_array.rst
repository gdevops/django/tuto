
.. _tests_test_array_field:

============================================
**Tests ArrayField**
============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/test_array.py




__init__.py
==============


.. literalinclude:: ../../tests/__init__.py
   :linenos:


fields.py
==============

.. literalinclude:: ../../tests/fields.py
   :linenos:



models.py
==============

.. literalinclude:: ../../tests/models.py
   :linenos:


test_array.py
==============

.. literalinclude:: test_array.py
   :linenos:
