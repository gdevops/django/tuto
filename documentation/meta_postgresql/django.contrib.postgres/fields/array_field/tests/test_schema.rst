
.. _tests_schema_array_field:

============================================
**Tests schema ArrayField**
============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/test_array.py




class SchemaTests(TransactionTestCase)
========================================

.. code-block:: python
   :linenos:


    import datetime
    import itertools
    import unittest
    from copy import copy
    from unittest import mock

    from django.core.management.color import no_style
    from django.db import (
        DatabaseError, DataError, IntegrityError, OperationalError, connection,
    )
    from django.db.models import (
        CASCADE, PROTECT, AutoField, BigAutoField, BigIntegerField, BinaryField,
        BooleanField, CharField, CheckConstraint, DateField, DateTimeField,
        ForeignKey, ForeignObject, Index, IntegerField, ManyToManyField, Model,
        OneToOneField, PositiveIntegerField, Q, SlugField, SmallAutoField,
        SmallIntegerField, TextField, TimeField, UniqueConstraint, UUIDField,
    )
    from django.db.transaction import TransactionManagementError, atomic
    from django.test import (
        TransactionTestCase, skipIfDBFeature, skipUnlessDBFeature,
    )
    from django.test.utils import CaptureQueriesContext, isolate_apps
    from django.utils import timezone

    from .fields import (
        CustomManyToManyField, InheritedManyToManyField, MediumBlobField,
    )
    from .models import (
        Author, AuthorCharFieldWithIndex, AuthorTextFieldWithIndex,
        AuthorWithDefaultHeight, AuthorWithEvenLongerName, AuthorWithIndexedName,
        AuthorWithIndexedNameAndBirthday, AuthorWithUniqueName,
        AuthorWithUniqueNameAndBirthday, Book, BookForeignObj, BookWeak,
        BookWithLongName, BookWithO2O, BookWithoutAuthor, BookWithSlug, IntegerPK,
        Node, Note, NoteRename, Tag, TagIndexed, TagM2MTest, TagUniqueRename,
        Thing, UniqueTest, new_apps,
    )


    class SchemaTests(TransactionTestCase):
        """
        Tests for the schema-alteration code.

        Be aware that these tests are more liable than most to false results,
        as sometimes the code to check if a test has worked is almost as complex
        as the code it is testing.
        """

        available_apps = []

        models = [
            Author, AuthorCharFieldWithIndex, AuthorTextFieldWithIndex,
            AuthorWithDefaultHeight, AuthorWithEvenLongerName, Book, BookWeak,
            BookWithLongName, BookWithO2O, BookWithSlug, IntegerPK, Node, Note,
            Tag, TagIndexed, TagM2MTest, TagUniqueRename, Thing, UniqueTest,
        ]


test_alter_field_with_custom_db_type
=====================================

.. code-block:: python
   :linenos:

    @unittest.skipUnless(connection.vendor == 'postgresql', 'PostgreSQL specific')
    def test_alter_field_with_custom_db_type(self):
        from django.contrib.postgres.fields import ArrayField

        class Foo(Model):
            field = ArrayField(CharField(max_length=255))

            class Meta:
                app_label = 'schema'

        with connection.schema_editor() as editor:
            editor.create_model(Foo)
        self.isolated_local_models = [Foo]
        old_field = Foo._meta.get_field('field')
        new_field = ArrayField(CharField(max_length=16))
        new_field.set_attributes_from_name('field')
        new_field.model = Foo
        with connection.schema_editor() as editor:
            editor.alter_field(Foo, old_field, new_field, strict=True)


test_alter_array_field_decrease_base_field_length
===================================================

.. code-block:: python
   :linenos:

    @isolate_apps('schema')
    @unittest.skipUnless(connection.vendor == 'postgresql', 'PostgreSQL specific')
    def test_alter_array_field_decrease_base_field_length(self):
        from django.contrib.postgres.fields import ArrayField

        class ArrayModel(Model):
            field = ArrayField(CharField(max_length=16))

            class Meta:
                app_label = 'schema'

        with connection.schema_editor() as editor:
            editor.create_model(ArrayModel)
        self.isolated_local_models = [ArrayModel]
        ArrayModel.objects.create(field=['x' * 16])
        old_field = ArrayModel._meta.get_field('field')
        new_field = ArrayField(CharField(max_length=15))
        new_field.set_attributes_from_name('field')
        new_field.model = ArrayModel
        with connection.schema_editor() as editor:
            msg = 'value too long for type character varying(15)'
            with self.assertRaisesMessage(DataError, msg):
                editor.alter_field(ArrayModel, old_field, new_field, strict=True)


test_alter_array_field_decrease_nested_base_field_length
===========================================================

.. code-block:: python
   :linenos:


    @isolate_apps('schema')
    @unittest.skipUnless(connection.vendor == 'postgresql', 'PostgreSQL specific')
    def test_alter_array_field_decrease_nested_base_field_length(self):
        from django.contrib.postgres.fields import ArrayField

        class ArrayModel(Model):
            field = ArrayField(ArrayField(CharField(max_length=16)))

            class Meta:
                app_label = 'schema'

        with connection.schema_editor() as editor:
            editor.create_model(ArrayModel)
        self.isolated_local_models = [ArrayModel]
        ArrayModel.objects.create(field=[['x' * 16]])
        old_field = ArrayModel._meta.get_field('field')
        new_field = ArrayField(ArrayField(CharField(max_length=15)))
        new_field.set_attributes_from_name('field')
        new_field.model = ArrayModel
        with connection.schema_editor() as editor:
            msg = 'value too long for type character varying(15)'
            with self.assertRaisesMessage(DataError, msg):
                editor.alter_field(ArrayModel, old_field, new_field, strict=True)
