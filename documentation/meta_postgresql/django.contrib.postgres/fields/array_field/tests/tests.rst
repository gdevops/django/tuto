.. index::
   pair: Tests ; ArrayField

.. _tests_array_field:

============================================
**Tests ArrayField**
============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/test_array.py

.. toctree::
   :maxdepth: 3

   test_schema
   test_array
