.. index::
   pair: django.contrib.postgres.fields ; DateRangeField
   ! DateRangeField

.. _django_date_rangefield:

===================================================
**DateRangeField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#datetimerangefield
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt




``DateRangeField``
===================

.. class:: DateRangeField(**options)

    Stores a range of dates. Based on a
    :class:`~django.db.models.DateField`. Represented by a ``daterange`` in the
    database and a :class:`~psycopg2:psycopg2.extras.DateRange` in Python.

    Regardless of the bounds specified when saving the data, PostgreSQL always
    returns a range in a canonical form that includes the lower bound and
    excludes the upper bound; that is ``[)``.
