.. index::
   pair: django.contrib.postgres.fields ; IntegerRangeField
   ! IntegerRangeField

.. _django_integer_rangefield:

===================================================
**IntegerRangeField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#decimalrangefield
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt




`IntegerRangeField``
---------------------

.. class:: IntegerRangeField(**options)

    Stores a range of integers. Based on an
    :class:`~django.db.models.IntegerField`. Represented by an ``int4range`` in
    the database and a :class:`~psycopg2:psycopg2.extras.NumericRange` in
    Python.

    Regardless of the bounds specified when saving the data, PostgreSQL always
    returns a range in a canonical form that includes the lower bound and
    excludes the upper bound; that is ``[)``.
