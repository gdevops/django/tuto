.. index::
   pair: Tests ; RangeField

.. _tests_test_ranges:

============================================
**Tests RangeField**
============================================




__init__.py
==============


.. literalinclude:: ../../tests/__init__.py
   :linenos:


fields.py
==============

.. literalinclude:: ../../tests/fields.py
   :linenos:



models.py
==============

.. literalinclude:: ../../tests/models.py
   :linenos:


test_ranges.py
=================

.. literalinclude:: test_ranges.py
   :linenos:


test_constraints.py
=====================

.. literalinclude:: test_constraints.py
   :linenos:
