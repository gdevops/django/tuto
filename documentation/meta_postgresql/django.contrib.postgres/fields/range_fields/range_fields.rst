.. index::
   pair: django.contrib.postgres.fields ; RangeFields
   ! RangeFields

.. _django_range_fields:

===================================================
**Range Fields**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#range-fields
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt
   - :ref:`postgresql_range_types`





Range fields
==============

.. toctree::
   :maxdepth: 3

   description/description
   range_boundary/range_boundary
   range_operators/range_operators
   integerrangefield/integerrangefield
   bigintegerrangefield/bigintegerrangefield
   decimalrangefield/decimalrangefield
   daterangefield/daterangefield
   datetimerangefield/datetimerangefield

Tests
==============

.. toctree::
   :maxdepth: 3

   tests/tests
