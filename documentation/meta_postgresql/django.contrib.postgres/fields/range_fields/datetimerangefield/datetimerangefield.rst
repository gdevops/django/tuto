.. index::
   pair: django.contrib.postgres.fields ; DateTimeRangeField
   ! DateTimeRangeField

.. _django_datetime_rangefield:

===================================================
**DateTimeRangeField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#datetimerangefield
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt




``DateTimeRangeField``
=========================

.. class:: DateTimeRangeField(**options)

    Stores a range of timestamps. Based on a
    :class:`~django.db.models.DateTimeField`. Represented by a ``tstzrange`` in
    the database and a :class:`~psycopg2:psycopg2.extras.DateTimeTZRange` in
    Python.
