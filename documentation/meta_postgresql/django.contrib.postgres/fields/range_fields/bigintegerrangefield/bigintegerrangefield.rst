.. index::
   pair: django.contrib.postgres.fields ; BigIntegerRangeField
   ! BigIntegerRangeField

.. _django_biginteger_rangefield:

===================================================
**BigIntegerRangeField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#decimalrangefield
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt




``BigIntegerRangeField``
------------------------

.. class:: BigIntegerRangeField(**options)

    Stores a range of large integers. Based on a
    :class:`~django.db.models.BigIntegerField`. Represented by an ``int8range``
    in the database and a :class:`~psycopg2:psycopg2.extras.NumericRange` in
    Python.

    Regardless of the bounds specified when saving the data, PostgreSQL always
    returns a range in a canonical form that includes the lower bound and
    excludes the upper bound; that is ``[)``.
