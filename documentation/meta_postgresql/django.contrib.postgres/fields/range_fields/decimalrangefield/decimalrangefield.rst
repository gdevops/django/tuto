.. index::
   pair: django.contrib.postgres.fields ; DecimalRangeField
   ! DecimalRangeField

.. _django_decimal_rangefield:

===================================================
**DecimalRangeField**
===================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/#decimalrangefield
   - https://docs.djangoproject.com/fr/3.0/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt




``DecimalRangeField``
---------------------

.. class:: DecimalRangeField(**options)

    Stores a range of floating point values. Based on a
    :class:`~django.db.models.DecimalField`. Represented by a ``numrange`` in
    the database and a :class:`~psycopg2:psycopg2.extras.NumericRange` in
    Python.
