.. index::
   pair: django.contrib.postgres ; fields

.. _django_postgres_fields:

============================================
**django.contrib.postgres.fields**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/postgres/fields/
   - https://github.com/django/django/blob/master/docs/ref/contrib/postgres/fields.txt
   - https://github.com/django/django/tree/master/tests/postgres_tests
   - https://github.com/django/django/blob/master/tests/postgres_tests/fields.py

.. toctree::
   :maxdepth: 3

   array_field/array_field
   hstore_field/hstore_field
   json_field/json_field
   range_fields/range_fields
