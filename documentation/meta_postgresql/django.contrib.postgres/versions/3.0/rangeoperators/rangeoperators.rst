.. index::
   pair: RangeOperators ; PostgreSQL


.. _postgresql_rangeoperators:

===============================================================================
PostgreSQL RangeOperators
===============================================================================


- https://docs.djangoproject.com/en/3.0/ref/contrib/postgres/fields/#django.contrib.postgres.fields.RangeOperators
- https://www.postgresql.org/docs/current/functions-range.html#RANGE-OPERATORS-TABLE
- https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-INCLUSIVITY


Description
===============


PostgreSQL provides a set of SQL operators that can be used together with the
range data types (see the PostgreSQL documentation for the full details of
range operators).

This class is meant as a convenient method to avoid typos.

The operator names overlap with the names of corresponding lookups

::

    class RangeOperators:
        EQUAL = '='
        NOT_EQUAL = '<>'
        CONTAINS = '@>'
        CONTAINED_BY = '<@'
        OVERLAPS = '&&'
        FULLY_LT = '<<'
        FULLY_GT = '>>'
        NOT_LT = '&>'
        NOT_GT = '&<'
        ADJACENT_TO = '-|-'


https://x.com/l_avrot and https://x.com/be_haki
--------------------------------------------------------------------


- https://x.com/be_haki/status/1197461528208957440?s=20
- https://x.com/l_avrot/status/1197469528470495232?s=20


A reader from Reddit just pointed out that in #PostgreSQL to filter a date
range you can use a range type::

    SELECT * FROM sales WHERE created <@ daterange(date '2019-01-01', date '2020-01-01', '[)');


Nice!


.. figure:: l_avrot_operators.png
   :align: center

I call **@>** and **<@** the *bird operators* because they look like birds and
they're so cute!



django/contrib/postgres/fields/ranges.py
=============================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/postgres/fields/ranges.py
   - https://www.postgresql.org/docs/current/functions-range.html#RANGE-OPERATORS-TABLE


.. literalinclude:: django_contrib_postgres_fields_ranges.py
   :linenos:


tests/postgres_tests/models.py
===================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/models.py


.. literalinclude:: tests_postgres_tests_models.py
   :linenos:


tests/postgres_tests/test_constraints.py
==========================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/test_constraints.py

.. literalinclude:: tests_postgres_tests_test_constraints.py
   :linenos:
