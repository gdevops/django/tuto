.. index::
   pair: Index concurrently ; PostgreSQL


.. _postgresql_index_concurrently:

===============================================================================
PostgreSQL index concurrently
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/en/3.0/ref/contrib/postgres/operations/#django.contrib.postgres.operations.AddIndexConcurrently




Description
===============


django/contrib/postgres/operations.py
=============================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/postgres/operations.py


.. literalinclude:: django_contrib_postgres_operations.py
   :linenos:


tests/postgres_tests/test_operations.py
============================================

.. seealso::

   - https://github.com/django/django/blob/master/tests/postgres_tests/test_operations.py


.. literalinclude:: tests_postgres_tests_test_operations.py
   :linenos:
