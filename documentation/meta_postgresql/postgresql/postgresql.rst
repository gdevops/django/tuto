.. index::
   pair: Database ; PostgreSQL

.. _postgresql_django:

============================================
**PostgreSQL**
============================================

.. seealso::

   - :ref:`postgresql_ref`





PostgreSQL versions
=====================

.. seealso::

   - :ref:`postgresl_versions`
