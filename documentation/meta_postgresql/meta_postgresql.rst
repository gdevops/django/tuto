.. index::
   pair: Meta ; PostgreSQL

.. _meta_postgresql:

=============================================================
**PostgreSQL** + **django.contrib.postgres**
=============================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/search/?q=postgresql
   - https://github.com/django/django/tree/master/django/contrib/postgres


.. toctree::
   :maxdepth: 3

   postgresql/postgresql
   django.contrib.postgres/django.contrib.postgres
