
.. index::
   pair: Django ; Continuous Integration


.. _django_ci:

============================================
Django continuous integration
============================================


.. seealso::

   - https://dev.to/kpavlovsky_pro/setting-up-tests-in-gitlab-ci-for-django-project-with-docker-engine
