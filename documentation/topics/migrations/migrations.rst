
.. index::
   pair: Django ; Migrations


.. _django_migrations:

============================================
Django migrations
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/migrations/
   - https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html
   - https://realpython.com/django-migrations-a-primer/
   - https://simpleisbetterthancomplex.com/tutorial/2017/09/26/how-to-create-django-data-migrations.html
   - https://docs.djangoproject.com/en/dev/topics/migrations/
   - https://docs.djangoproject.com/fr/2.2/topics/migrations/


.. toctree::
   :maxdepth: 3

   migrate/migrate
