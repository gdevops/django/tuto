
.. index::
   pair: Django ; migrate


.. _django_migrate_command:

============================================
Django migrate command
============================================




python manage_dev.py help migrate
==================================

::

    python manage_dev.py help migrate

::

    - usage: manage_dev.py migrate [-h] [--noinput] [--database DATABASE] [--fake]
                             [--fake-initial] [--plan] [--run-syncdb]
                             [--version] [-v {0,1,2,3}] [--settings SETTINGS]
                             [--pythonpath PYTHONPATH] [--traceback]
                             [--no-color] [--force-color]
                             [app_label] [migration_name]

    Updates database schema. Manages both apps with migrations and those without.

    positional arguments:
      app_label             App label of an application to synchronize the state.
      migration_name        Database state will be brought to the state after that
                            migration. Use the name "zero" to unapply all
                            migrations.

    optional arguments:
      -h, --help            show this help message and exit
      --noinput, --no-input
                            Tells Django to NOT prompt the user for input of any
                            kind.
      --database DATABASE   Nominates a database to synchronize. Defaults to the
                            "default" database.
      --fake                Mark migrations as run without actually running them.
      --fake-initial        Detect if tables already exist and fake-apply initial
                            migrations if so. Make sure that the current database
                            schema matches your initial migration before using
                            this flag. Django will only check for an existing
                            table name.
      --plan                Shows a list of the migration actions that will be
                            performed.
      --run-syncdb          Creates tables for apps without migrations.
      --version             show program's version number and exit
      -v {0,1,2,3}, --verbosity {0,1,2,3}
                            Verbosity level; 0=minimal output, 1=normal output,
                            2=verbose output, 3=very verbose output
      --settings SETTINGS   The Python path to a settings module, e.g.
                            "myproject.settings.main". If this isn't provided, the
                            DJANGO_SETTINGS_MODULE environment variable will be
                            used.
      --pythonpath PYTHONPATH
                            A directory to add to the Python path, e.g.
                            "/home/djangoprojects/myproject".
      --traceback           Raise on CommandError exceptions
      --no-color            Don't colorize the command output.
      --force-color         Force colorization of the command output.
