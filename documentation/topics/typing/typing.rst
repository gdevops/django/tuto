.. index::
   pair: Django ; Typing

.. _django_typing:

============================================
**Django typing**
============================================


.. seealso::

   - https://mypy-lang.blogspot.com/2019/03/extending-mypy-with-plugins.html





Python typing
=============

.. seealso::

   - :ref:`python_typing`
