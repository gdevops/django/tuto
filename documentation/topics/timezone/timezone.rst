
.. index::
   pair: Django ; timezone
   pair: Django ; datetime


.. _django_tz:

============================================
Django **timezone**
============================================

.. seealso::

   - :ref:`django_datetime`





Django **datetime**
======================

.. seealso::

   - :ref:`django_datetime`


Django **timezone**
=====================

.. seealso::

   - :ref:`django_timezone`
