.. index::
   pair: Django ; Deployment


.. _django_deployment:

============================================
**Django deployment**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/howto/deployment/asgi/

.. toctree::
   :maxdepth: 3


   asgi/asgi
