.. index::
   pair: ASGI ; Deployment
   ! ASGI


.. _django_asgi_hypercorn:

============================================
How to use Django with **Hypercorn**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/howto/deployment/asgi/hypercorn/
   - https://pgjones.gitlab.io/hypercorn/




How to use Django with Hypercorn
====================================

`Hypercorn <https://pgjones.gitlab.io/hypercorn/>`_ is an ASGI server
that supports HTTP/1, HTTP/2, and HTTP/3 with an emphasis on protocol support.

Installing Hypercorn
=========================


You can install Hypercorn with pip::

    python -m pip install hypercorn


docker-compose_dev.yml
==========================

.. code-block:: yaml

    # docker-compose_dev.yml
    #
    version: "3"
    services:
      django:
        build:
          context: docker-django
          dockerfile: Dockerfile_dev

        # command: bash -c "/opt/intranet/docker/.venv/bin/python manage.py migrate; /opt/intranet/docker/.venv/bin/python manage.py runserver 0.0.0.0:8004"
        command: bash -c "/opt/intranet/docker/.venv/bin/python manage.py migrate; /opt/intranet/docker/.venv/bin/hypercorn --bind '0.0.0.0:8004' config.asgi:application --reload"
        container_name: container_django_intranet_dev
        env_file:
          - ".env_django_dev"
        ports:
          # container will need access to port 8004
          - "8004:8004"
        volumes:
          # permet d'accéder au code du host
          - ./docker-django/intranet/:/opt/intranet/intranet
          - /home/pvergain/fiches_temps:/users
