"""Gestion des rapports (reports) pour les transactions mensuelles.


Thanks to:

- https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html


def export_users_csv(request):
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="users.csv"'

    writer = csv.writer(response)
    writer.writerow(["Username", "First name", "Last name", "Email address"])

    users = User.objects.all().values_list(
        "username", "first_name", "last_name", "email"
    )
    for user in users:
        writer.writerow(user)

    return response

"""
# http://ianalexandr.com/blog/getting-started-with-django-logging-in-5-minutes.html
import csv
import logging

import pendulum
from agence.models import Agence
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.core.paginator import Paginator
from django.db.models import Q
from django.db.models import Sum
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
from django.http.request import QueryDict
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from stats.models.models_day import StatsDay
from stats.models.models_day_agence import StatsDayAgence
from stats.models.models_day_agence import StatsMonthAgence
from stats.models.models_global_counters import StatsGlobalCounters
from stats.models.models_mixins import EnumTypeSums
from stats.models.models_mixins import InfosTransactionMixin
from users.models import User

from .forms import FormFiltreReports
from .models import LogTransactionNotariale
from .serializers import LogTransactionNotarialeSerializer
from .views import get_america_bogota_date_from_get_request
from .views import get_time_from_america_bogota

# https://docs.djangoproject.com/en/dev/topics/settings/#using-settings-in-python-code

# Get an instance of a logger
logger = logging.getLogger(__name__)


__all__ = ("VueReportGeneralInfoMonth",)


def ReadGetVueReportGeneralInfoMonth(
    request: HttpRequest,
) -> (pendulum.datetime, FormFiltreReports):
    # GET: c'est quand on pointe sur la page directement ou que l'on
    # utilise paginator
    logger.info(f"request={request.GET}")

    # Est-ce qu'il y a un filtre sur la date from_date ?
    from_date = get_america_bogota_date_from_get_request(request, "from_date")
    if from_date is None:
        # on initialise la date 12 mois avant
        # from_date = pendulum.now("America/Bogota").add(months=-12)
        from_date = pendulum.now("America/Bogota").add(months=-1)

    # https://docs.djangoproject.com/en/dev/ref/forms/api/#dynamic-initial-values
    form_filtres_reports = FormFiltreReports(
        initial={
            "date_from": from_date,
            "export_excel": False,
        }
    )

    # logger.info(f"ReadGetVueGrapheStatsDayAgence() form_filtre_reports:{form_filtre_reports}")

    return from_date, form_filtres_reports


def ReadPostVueReportGeneralInfoMonth(
    request: HttpRequest,
) -> (pendulum.datetime, FormFiltreReports):
    # POST c'est quand l'utilisateur appuie sur le bouton 'Appliquer'
    # A revoir car "Search forms that are idempotent should use the GET method"
    # p. 142, chapitre 11 "Form fundamentals" Two scoops of Django
    logger.info(f"POST={request.POST}")
    if "btn_form_filtre" in request.POST:
        # Instanciation du formulaire
        form_filtre_reports = FormFiltreReports(request.POST)
        if form_filtre_reports.is_valid():
            # indispensable pour avoir le tableau 'cleaned_data'
            # qui remet les dates anglaises en place:
            date_naive = form_filtre_reports.cleaned_data["date_from"]
            date_from_colombia = get_time_from_america_bogota(date_naive)
            logger.info("Colombia Date :{date_from_colombia=}")

    return date_from_colombia, form_filtre_reports


@login_required(login_url="home")
def VueReportGeneralInfoMonth(
    request: HttpRequest,
    template_name="log_transaction_notariale/report_choose_date.html",
) -> HttpResponse:
    """Production du rapport General info report month ("Informe general")

      Dans un premier temps on choisit la date et ensuite on exporte le fichier csv.

    - https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html
    """
    logger.info("Begin VueReportGeneralInfoMonth()")
    form_filtre_reports = None
    if request.method == "POST":
        start_date, form_filtre_reports = ReadPostVueReportGeneralInfoMonth(request)
        start_date = start_date.start_of("month")
        end_date = start_date.end_of("month")
        logger.info(f"{start_date=} => {end_date=}")

        format_year_month = f"{start_date.strftime('%Y-%m')}"
        filename = f"{format_year_month}_Informe_generale.csv"
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = f"attachment; filename={filename}"

        writer = csv.writer(response)
        entete_date = f"Fecha ({format_year_month})"
        writer.writerow([entete_date, "Con biometria", "Sin biometria"])
        current_day = start_date
        while current_day <= end_date:
            end_day = current_day.add(days=1)
            nb_transaction_days = 0
            nb_transactions = 0
            transactions_day = LogTransactionNotariale.objects.filter(
                date_transaction__range=[current_day, end_day]
            )
            nb_transactions_avec_biometrie = transactions_day.filter(
                match_rnec_enabled=1
            ).count()
            nb_transactions_sans_biometrie = transactions_day.filter(
                match_rnec_enabled=0
            ).count()
            format_current_day = current_day.strftime("%Y-%m-%d")
            # logger.info(
            #    f"{format_current_day};{nb_transactions_avec_biometrie};{nb_transactions_sans_biometrie}"
            # )
            logger.info(f"\n{current_day=}\nSQL query:\n{str(transactions_day.query)}")
            writer.writerow(
                (
                    format_current_day,
                    nb_transactions_avec_biometrie,
                    nb_transactions_sans_biometrie,
                )
            )
            current_day = end_day

        return response

    elif request.method == "GET":
        from_date, form_filtre_reports = ReadGetVueReportGeneralInfoMonth(request)
        context_commentaire = "general info report month"
        return render(
            request,
            template_name,
            context={
                "form_filtre_reports": form_filtre_reports,
                "url_post": reverse(
                    "log_transaction_notariale:report_general_info_month"
                ),
                # Filtres  on emploie "0" car None n'est pas reconnu par
                # le langage de template Django
                "from_date": "0" if from_date is None else from_date,
                "commentaire": context_commentaire,
            },
        )


@login_required(login_url="home")
def VueReportDetailedInfoDay(
    request: HttpRequest,
    template_name="log_transaction_notariale/report_choose_date.html",
) -> HttpResponse:
    """Detailed info report day ("Informe detallado")

      Dans un premier temps on choisit la date et ensuite on exporte le fichier csv.

    - https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html
    """
    logger.info("Begin VueReportDetailedInfoDay()")
    form_filtre_reports = None
    if request.method == "POST":
        day_date, form_filtre_reports = ReadPostVueReportGeneralInfoMonth(request)
        logger.info(f"{day_date=}")
        format_day_date = f"{day_date.strftime('%Y-%m-%d')}"
        filename = f"{format_day_date}_Informe_detallado.csv"
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = f"attachment; filename={filename}"
        writer = csv.writer(response)
        writer.writerow(
            [
                "Codigo Notaria",
                "#Total transacciones",
                "#Cotejos Hit",
                "#Cotejos No-Hit",
                "Sin biometria",
            ]
        )
        # tri par ordre descendant sur le nombre total de transactions, i.e du plus grand au plus petit
        liste = StatsDayAgence.objects.filter(date=day_date).order_by(
            "-nb_transactions"
        )
        logger.info(f"\n{day_date=}\nSQL query:\n{str(liste.query)}")
        for i, t in enumerate(liste):
            # logger.info(
            #    f"{t.agence.id_etude_notariale=} {t.nb_transactions=} HIT:{t.nb_global_match_rnec_ok} No-HIT:{t.nb_global_match_rnec_pb} Sans biométrie:{t.nb_match_rnec_disabled}"
            # )
            writer.writerow(
                (
                    t.agence.id_etude_notariale,
                    t.nb_transactions,
                    t.nb_global_match_rnec_ok,
                    t.nb_global_match_rnec_pb,
                    t.nb_match_rnec_disabled,
                )
            )

        return response

    elif request.method == "GET":
        from_date, form_filtre_reports = ReadGetVueReportGeneralInfoMonth(request)
        context_commentaire = "detailed info report day"
        return render(
            request,
            template_name,
            context={
                "form_filtre_reports": form_filtre_reports,
                "url_post": reverse(
                    "log_transaction_notariale:report_detailed_info_day"
                ),
                # Filtres  on emploie "0" car None n'est pas reconnu par
                # le langage de template Django
                "from_date": "0" if from_date is None else from_date,
                "commentaire": context_commentaire,
            },
        )


@login_required(login_url="home")
def VueReportDetailedInfoMonth(
    request: HttpRequest,
    template_name="log_transaction_notariale/report_choose_date.html",
) -> HttpResponse:
    """Detailed info report month ("Informe detallado"):

      Dans un premier temps on choisit la date et ensuite on exporte le fichier csv.

    - https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html
    """
    logger.info("Begin VueReportDetailedInfoMonth()")
    form_filtre_reports = None
    if request.method == "POST":
        month_date, form_filtre_reports = ReadPostVueReportGeneralInfoMonth(request)
        logger.info(f"{month_date=}")
        format_month_date = f"{month_date.strftime('%Y-%m')}"
        filename = f"{format_month_date}_Informe_detallado.csv"
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = f"attachment; filename={filename}"
        writer = csv.writer(response)
        writer.writerow(
            [
                "Codigo Notaria",
                "#Total transacciones",
                "#Cotejos Hit",
                "#Cotejos No-Hit",
                "Sin biometria",
            ]
        )
        liste = StatsMonthAgence.objects.filter(
            year=month_date.year, month=month_date.month
        ).order_by("-nb_transactions")
        logger.info(f"\nSQL query:\n{str(liste.query)}")
        for i, t in enumerate(liste):
            # logger.info(
            #    f"{t.agence.id_etude_notariale=} {t.nb_transactions=} HIT:{t.nb_global_match_rnec_ok} No-HIT:{t.nb_global_match_rnec_pb} Sans biométrie:{t.nb_match_rnec_disabled}"
            # )
            writer.writerow(
                (
                    t.agence.id_etude_notariale,
                    t.nb_transactions,
                    t.nb_global_match_rnec_ok,
                    t.nb_global_match_rnec_pb,
                    t.nb_match_rnec_disabled,
                )
            )

        return response

    elif request.method == "GET":
        from_date, form_filtre_reports = ReadGetVueReportGeneralInfoMonth(request)
        context_commentaire = "detailed info report month"
        return render(
            request,
            template_name,
            context={
                "form_filtre_reports": form_filtre_reports,
                "url_post": reverse(
                    "log_transaction_notariale:report_detailed_info_month"
                ),
                # Filtres  on emploie "0" car None n'est pas reconnu par
                # le langage de template Django
                "from_date": "0" if from_date is None else from_date,
                "commentaire": context_commentaire,
            },
        )
