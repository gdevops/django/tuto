.. index::
   pair: Django ; csv

.. _django_csv:

============================================
Django **csv** export
============================================

.. seealso::

   - https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html

.. toctree::
   :maxdepth: 3

   examples/examples
