
.. index::
   pair: Django ; git


.. _django_git:

============================================
Django git
============================================


.. toctree::
   :maxdepth: 3

   branches/branches
   pre_commit/pre_commit
