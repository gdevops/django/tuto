
.. index::
   pair: pre-commit ; git
   pair: pre-commit ; hook
   pair: pre-commit ; trailing-whitespace
   pair: pre-commit ; autoupdate


.. _pre-commit_hook:

============================================
pre-commit hook
============================================


.. seealso::

   - https://pre-commit.com/
   - https://github.com/pre-commit/pre-commit




Add pre-commit-hooks
======================

::


  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v1.2.3
    hooks:
    - id: trailing-whitespace


::

    pvergain@UC004 > ~/projects/intranet > master > git commit . -m "add pre-commit hooks"


::

    [INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
    [INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
    [INFO] Once installed this environment will be reused.
    [INFO] This may take a few minutes...
    black................................................(no files to check)Skipped
    Trim Trailing Whitespace.................................................Passed
    [master 05fb9ab] add pre-commit hooks
     1 file changed, 6 insertions(+)


pre-commit autoupdate
=======================

::

    pvergain@UC004 > ~/projects/intranet > master > pre-commit autoupdate

::

    Updating https://github.com/ambv/black...[INFO] Initializing environment for https://github.com/ambv/black.
    updating stable -> 18.9b0.
    Updating https://github.com/pre-commit/pre-commit-hooks...[INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
    updating v1.2.3 -> v2.1.0.
