
.. index::
   pair: branch ; git


.. _git_branch:

============================================
git branch
============================================



::

    Creation of a XXX git branch
    ===============================

    git checkout -b feature-XXX  master
    git push --set-upstream origin feature-XXX
    git add .
    git commit . -m "OK feature XXX "
    git push
    ...
    ...
    git checkout master
    git merge --no-ff feature-XXX
    git push origin master
    git push --delete origin feature-XXX
    git branch --delete feature-XXX
