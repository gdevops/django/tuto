.. index::
   pair: Django ; Database

.. _django_database:

============================================
Django database
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/db/

.. module:: django.db

A model is the single, definitive source of data about your data.

It contains the essential fields and behaviors of the data you're storing.
Generally, each model maps to a single database table.


.. toctree::
   :maxdepth: 1

   examples/examples
   transactions/transactions
