.. index::
   ! Transactions

.. _django_database_transactions:

============================================
Django database transactions
============================================

.. toctree::
   :maxdepth: 1

   examples/examples
