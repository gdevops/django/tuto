
.. _django_database_transaction_examples:

============================================
Django database transaction examples
============================================

.. toctree::
   :maxdepth: 1

   charemza/charemza
   haki_benita/haki_benita
