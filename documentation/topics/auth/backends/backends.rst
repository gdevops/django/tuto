
.. index::
   pair: Django Auth; Backends
   pair: Moteurs ; authentification


.. _django_auth_backends:

===============================================================================
Moteurs d’authentification
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/ref/contrib/auth/#module-django.contrib.auth.backends
   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/#authentication-backends



::

    ########### Paramètrage Django pour l'utilisation de LDAP ##############
    # https://docs.djangoproject.com/fr/2.1/ref/settings/#std:setting-AUTHENTICATION_BACKENDS
    AUTHENTICATION_BACKENDS = (
        "django.contrib.auth.backends.ModelBackend",
        "accounts.backends.Id3LDAPBackend",
        "accounts.backends.Id3AuthBackend",
    )


.. toctree::
   :maxdepth: 3

   django/django
   ldap/ldap
