
.. index::
   pair: Moteur authentification; Django


.. _django_auth_django:

===============================================================================
Le moteur d’authentification Django (default)
===============================================================================




Le module Python https://github.com/django/django/blob/master/django/contrib/auth/backends.py
=================================================================================================

.. literalinclude:: backends.py
   :linenos:
