
.. index::
   pair: Moteur ; LDAP


.. _moteur_ldap:

===============================================================================
Moteur LDAP
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/ref/contrib/auth/#module-django.contrib.auth.backends
   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/#authentication-backends





Paramètrage LDAP
==================

::

    # https://github.com/pyldap/pyldap/
    import ldap

    from django_auth_ldap.config import (
        # https://django-auth-ldap.readthedocs.io/en/latest/authentication.html
        LDAPSearch,
        LDAPSearchUnion,
        # https://django-auth-ldap.readthedocs.io/en/latest/groups.html
        GroupOfNamesType,
    )

    ...

    ############# Authentification LDAP avec django-auth-ldap ##############
    #
    # https://django-auth-ldap.readthedocs.io/en/latest/
    #
    AUTH_LDAP_SERVER_URI = "ldap://XX.YYY.ZZZ"
    # LDAP Search/Bind
    AUTH_LDAP_BIND_DN = "cn=apache,cn=users,dc=int,dc=XXX,dc=eu"
    AUTH_LDAP_BIND_PASSWORD = "#AAXXXYYYY"
    AUTH_LDAP_USER_SEARCH = LDAPSearch(
        "ou=users,ou=XXX Technologies,dc=int,dc=XXX,dc=eu",
        ldap.SCOPE_SUBTREE,
        "(sAMAccountName=%(user)s)",
    )
    AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
        "OU=Groups,OU=XXX Technologies,DC=int,DC=XXX,DC=eu",
        ldap.SCOPE_SUBTREE,
        "(objectClass=group)",
    )
    # What to do once the user is authenticated
    AUTH_LDAP_USER_ATTR_MAP = {
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }

    ########### Paramètrage Django pour l'utilisation de LDAP ##############
    # https://docs.djangoproject.com/fr/2.1/ref/settings/#std:setting-AUTHENTICATION_BACKENDS
    AUTHENTICATION_BACKENDS = (
        "django.contrib.auth.backends.ModelBackend",
        "accounts.backends.Id3LDAPBackend",
        "accounts.backends.Id3AuthBackend",
    )


Un moteur LDAP **accounts/backends.py**
=================================================================================================

.. literalinclude:: backends.py
   :linenos:
