"""Définition des backends d'authentification.


.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/auth/customizing/#authentication-backends
   - https://docs.djangoproject.com/fr/1.10/topics/auth/customizing/#authentication-backends
   - https://docs.djangoproject.com/en/dev/ref/contrib/auth/#module-django.contrib.auth.backends



.. seealso::

   - https://pythonhosted.org/django-auth-ldap/
   - https://bitbucket.org/psagers/django-auth-ldap/commits/tag/1.2.10


"""
import logging

import django.contrib.auth
import django_auth_ldap
import structlog
from django.contrib.auth import get_backends
from django.contrib.auth import get_user_model
from django_auth_ldap.backend import LDAPBackend
from employes.models import Employe

# https://pythonhosted.org/django-auth-ldap/
# https://docs.djangoproject.com/en/dev/ref/contrib/auth/#django.contrib.auth.backends.ModelBackend
# https://docs.djangoproject.com/en/1.10/_modules/django/contrib/auth/

# from django.contrib.auth.backends import ModelBackend


logger = logging.getLogger("django_auth_ldap")
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


class Id3LDAPBackend(django_auth_ldap.backend.LDAPBackend):
    """A custom LDAP authentication backend."""

    def authenticate(self, request=None, username=None, password=None, **kwargs):
        """Overrides LDAPBackend.authenticate to save user password in django.

        Changement de signature le 2017-06-20

        -    def authenticate(self, username, password, **kwargs):
        +    def authenticate(self, request=None, username=None, password=None, **kwargs)
        """

        logger.info(f"Id3LDAPBackend.authenticate({username})")
        user = None
        try:
            user = LDAPBackend.authenticate(
                self, username=username, password=password, request=request
            )
            logger.info(f"Found user:{user}")
            try:
                employe = Employe.objects.get(login=username)
                if employe.user is None:
                    try:
                        employe.user = user
                        employe.save()
                    except Exception as error:
                        logger.error(f"Can't save {employe}")
            except Exception as error:
                logger.error(f"Employe {username} not found:{error}")

        except Exception as error:
            logger.info(
                f"Exception in Id3LDAPBackend.authenticate({username}) exception:{error}"
            )

        # If user has successfully logged, save his password in django database
        if user:
            logger.info(f"Id3LDAPBackend.authenticate.set_password")
            user.set_password(password)
            logger.info(f"Id3LDAPBackend.authenticate() user.save()")
            user.save()
        else:
            logger.info("User is none")

        return user

    def get_or_create_user(self, username, ldap_user):
        """Overrides LDAPBackend.get_or_create_user to force from_ldap to True."""

        logger.debug("Id3LDAPBackend.get_or_create_user()")
        kwargs = {"username": username, "defaults": {"from_ldap": True}}
        user_model = get_user_model()
        user = user_model.objects.get_or_create(**kwargs)
        logger.debug(f"Id3LDAPBackend.get_or_create_user({user})")
        return user


class Id3AuthBackend(django.contrib.auth.backends.ModelBackend):
    """A custom authentication backend overriding django ModelBackend."""

    @staticmethod
    def _is_ldap_backend_activated():
        """Returns True if Id3LDAPBackend is activated."""
        return Id3LDAPBackend in [b.__class__ for b in get_backends()]

    def authenticate(self, username, password):
        """Overrides ModelBackend to refuse LDAP users if Id3LDAPBackend is activated."""

        if self._is_ldap_backend_activated():
            user_model = get_user_model()
            logger.debug(
                f"Id3AuthBackend.authenticate({username}/{password}) get_user_model():{user_model}"
            )
            try:
                user = user_model.objects.get(username=username, from_ldap=False)
                logger.debug(
                    f"Id3AuthBackend.authenticate({username}/{password}) user_model.objects.get({user})"
                )
                return user
            except:
                logger.debug("Id3AuthBackend.authenticate() return None")
                return None
        else:
            logger.debug("Ldap backend is not activated")

        return user
