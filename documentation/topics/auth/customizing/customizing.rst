
.. index::
   pair: Django Auth; customizing
   pair: Personnalisation; authentification


.. _django_auth_customizing:

===============================================================================
Personnalisation de l’authentification dans Django (Django auth customizing)
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/




Description
==============

L’authentification livrée avec Django est suffisante dans la plupart
des cas, mais vous pouvez rencontrez des besoins qui ne sont pas couverts
par la solution proposée par défaut.

Afin de personnaliser l’authentification pour les besoins de vos projets,
vous devez comprendre quels sont les points d’ancrage permettant
d’étendre et de remplacer les composants du système de Django.

Ce document fournit des détails sur la façon dont le système
d’authentification peut être adapté.


1. Les moteurs d’authentification permettent d’adapter le système
   lorsque un nom d’utilisateur et un mot de passe stockés dans le
   modèle d’utilisateur doivent servir à authentifier l’utilisateur
   avec un autre service que celui par défaut de Django.

2. Vous pouvez donner des permissions personnalisées à vos modèles qui
   peuvent être vérifiées à travers le système d’autorisation de Django.

3. Vous pouvez étendre le modèle User par défaut ou le substituer par un
   tout autre modèle personnalisé.


Autres sources d’authentification
====================================

Dans certaines situations, il peut être nécessaire de se connecter à une
autre source d’authentification, c’est-à-dire une autre source de noms
d’utilisateur et de mots de passe ou d’autres méthodes d’authentification.

Par exemple, votre entreprise possède peut-être déjà une infrastructure
LDAP stockant les noms d’utilisateur et mots de passe pour tous ses employés.

Il serait fastidieux aussi bien pour l’administrateur réseau que pour
les utilisateurs eux-mêmes de devoir gérer des comptes séparés dans
LDAP et dans les applications basées sur Django.

Ainsi, pour faire face à ce genre de situations, le système
d’authentification de Django vous permet de brancher d’autres
sources d’authentification.

Vous pouvez surcharger le système par défaut de Django basé sur la base
de données ou vous **pouvez utiliser le système par défaut en parallèle
avec d’autres systèmes**.


Définition de moteurs d’authentification (AUTHENTICATION_BACKENDS)
====================================================================

En arrière-plan, Django maintient une liste de **moteurs d’authentification**
qu’il sollicite lors de l’authentification.

Lorsque quelqu’un appelle django.contrib.auth.authenticate() comme
expliqué ci-dessus dans Comment connecter un utilisateur, Django tente
une authentification avec chaque moteur d’authentification.

Si la première méthode d’authentification échoue, Django essaie avec
la deuxième et ainsi de suite jusqu’à ce que tous les moteurs aient
été sollicités.

La liste des moteurs d’authentification à utiliser est définie dans le
réglage **AUTHENTICATION_BACKENDS**.

Il doit s’agir d’une liste de chemins Python vers des classes Python
qui sont capables d’authentifier des utilisateurs.

Ces classes peuvent se trouver n’importe où dans votre chemin Python.

Par défaut, AUTHENTICATION_BACKENDS contient::

    ['django.contrib.auth.backends.ModelBackend']


Il s’agit du moteur d’authentification de base qui vérifie la base de
données Django des utilisateurs et interroge les permissions intégrées.

Il ne protège pas contre les attaques de force brute et n’utilise pas de
mécanisme de « rate limiting » (restriction du nombre de tentatives dans
le temps).

Vous pouvez soit écrire votre propre mécanisme de restriction dans un
moteur d’authentification personnalisé ou utiliser les mécanismes
offerts par la plupart des serveurs Web.

.. warning:: L’ordre dans la liste AUTHENTICATION_BACKENDS a son importance ; si le
   même nom d’utilisateur et mot de passe est valable dans plusieurs moteurs,
   Django s’arrête dès qu’un moteur a accepté les données d’authentification.

   Si un moteur génère une exception PermissionDenied, l’authentification
   échouera immédiatement. Django ne continue pas avec les moteurs suivants.


Exemple
--------

::

    ########### Paramètrage Django pour l'utilisation de LDAP ##############
    # https://docs.djangoproject.com/fr/2.1/ref/settings/#std:setting-AUTHENTICATION_BACKENDS
    AUTHENTICATION_BACKENDS = (
        "django.contrib.auth.backends.ModelBackend",
        "accounts.backends.Id3LDAPBackend",
        "accounts.backends.Id3AuthBackend",
    )



.. note:: Dès qu’un utilisateur s’est authentifié, Django mémorise le
   moteur utilisé pour authentifier cet utilisateur dans sa session ;
   il réutilise ensuite ce même moteur pour toute la durée de la session
   chaque fois qu’il est nécessaire d’accéder à l’utilisateur actuellement
   authentifié.
   Cela signifie en pratique que les sources d’authentification sont
   mises en cache par session et que si vous modifiez AUTHENTICATION_BACKENDS,
   vous devrez réinitialiser les données de sessions dans le cas où vous
   voulez forcer les utilisateurs à s’authentifier à nouveau en utilisant
   une autre méthode.

   Une façon simple de faire cela est d’exécuter::

       Session.objects.all().delete().
