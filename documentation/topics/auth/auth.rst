
.. index::
   pair: Django ; Auth


.. _django_auth:

============================================
Django contrib auth
============================================

.. seealso::

   - https://github.com/django/django/tree/master/django/contrib/auth
   - https://docs.djangoproject.com/en/dev/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
   - https://docs.djangoproject.com/fr/2.1/topics/auth/
   - https://realpython.com/django-user-management/


.. toctree::
   :maxdepth: 3


   articles/articles
   default/default
   customizing/customizing
   backends/backends
   packages/packages
   modules/modules
   user/user
   group/group
   permissions/permissions
