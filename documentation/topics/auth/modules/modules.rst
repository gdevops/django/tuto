
.. index::
   pair: Module ; contrib/auth/models.py
   pair: Module ; contrib/auth/base_user.py
   pair: Module ; contrib/auth/mixins.py
   pair: Module ; contrib/auth/decorators.py
   pair: Module ; contrib/auth/urls.py
   pair: Module ; contrib/auth/__init__.py
   pair: Module ; contrib/auth/context_processors.py


.. _django_auth_modules:

===================================================================================================================
Modules Python django.contrib.auth.{__init__, models, base_user, mixins, urls, decorators, context_processors}.py
===================================================================================================================

.. seealso::

   - :ref:`django_user`
   - https://docs.djangoproject.com/en/dev/ref/contrib/auth/#django.contrib.auth.models.User




.. _auth_init:

Module Python **django/contrib/auth/__init__.py**
=====================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/__init__.py


.. literalinclude:: __init__.py
   :linenos:



.. _auth_models:

Module Python **django/contrib/auth/models.py**
==================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py


.. literalinclude:: models.py
   :linenos:


.. _auth_base_user:

Module Python **django/contrib/auth/base_user.py**
==================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/base_user.py


.. literalinclude:: base_user.py
   :linenos:


.. _auth_mixins:

Module Python **django/contrib/auth/mixins.py**
==================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/mixins.py


.. literalinclude:: mixins.py
   :linenos:


.. _auth_urls:

Module Python **django/contrib/auth/urls.py**
==================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/urls.py


.. literalinclude:: urls.py
   :linenos:


.. _auth_decorators:

Module Python **django/contrib/auth/decorators.py**
=====================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/decorators.py


.. literalinclude:: decorators.py
   :linenos:


.. _auth_context_processors:

Module Python **django/contrib/auth/context_processors.py**
==============================================================

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/context_processors.py


.. literalinclude:: context_processors.py
   :linenos:
