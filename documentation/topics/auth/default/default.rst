
.. index::
   pair: Django Auth; default
   pair: autorisations; permissions


.. _django_auth_default:

============================================
Django auth par défaut
============================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/default/




Introduction
==============

Ce document présente l’utilisation du système d’authentification de
Django dans sa configuration par défaut.

Cette configuration a évolué afin de pouvoir répondre aux besoins les
plus courants des projets, elle est capable de gérer une large palette
de tâches, et elle possède une implémentation soigneuse des mots de
passe et des permissions.

L’authentification de Django fournit à la fois **l’authentification
et l’autorisation (permissions)**, regroupés sous le terme général de
système d’authentification, dans la mesure où ces fonctions sont assez
étroitement liées.
