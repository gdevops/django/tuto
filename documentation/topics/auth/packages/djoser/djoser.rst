
.. index::
   pair: Architecture ; Single Page App
   ! djoser

.. _djoser:

========================================================================
djoser (REST implementation of Django authentication system)
========================================================================

.. seealso::

   - https://github.com/sunscrapers/djoser




Description
=============

REST implementation of Django authentication system.

**djoser** library provides a set of Django Rest Framework views to handle basic
actions such as registration, login, logout, password reset and account
activation.

It works with custom user model.

Instead of reusing Django code (e.g. PasswordResetForm), we reimplemented
few things to fit better into **Single Page App architecture**.
