
.. index::
   pair: fakeldap ; Python

.. _fakeldap:

===============================================
fakeldap
===============================================

.. seealso::

   - https://github.com/zulip/fakeldap




README.rst
===========

.. seealso::

   - https://github.com/zulip/fakeldap/blob/master/README.rst

.. include:: README.rst


Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
