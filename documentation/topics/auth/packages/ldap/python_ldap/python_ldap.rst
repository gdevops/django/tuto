
.. index::
   pair: python-ldap ; Python

.. _ldap_python-ldap:

===============================================
python-ldap (LDAP client API for Python)
===============================================

.. seealso::

   - https://github.com/python-ldap/python-ldap
   - https://www.python-ldap.org/en/latest/
   - https://github.com/python-ldap/python-ldap/blob/master/setup.py



.. include:: README.rst

Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
