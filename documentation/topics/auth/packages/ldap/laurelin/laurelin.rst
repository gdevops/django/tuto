
.. index::
   pair: laurelin ; Python

.. _laurelin:

===============================================
laurelin
===============================================

.. seealso::


   - https://github.com/ashafer01/laurelin
   - https://github.com/ashafer01/laurelin/blob/master/setup.py
   - https://github.com/ashafer01/laurelin/blob/master/requirements.txt



.. include:: README.rst



Blog, twitter
==============

.. seealso::

   - https://github.com/ashafer01
   - https://x.com/ashafer01
   - https://medium.com/@ashafer01/laurelin-a-new-ldap-client-for-python-675ebac78d96
   - https://medium.com/@ashafer01/a-simpler-ldap-search-filter-syntax-f2e62d0a2ee8
   - https://medium.com/@ashafer01


Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
