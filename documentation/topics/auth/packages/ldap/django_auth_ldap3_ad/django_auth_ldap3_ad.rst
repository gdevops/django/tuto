
.. index::
   pair: django-auth-ldap3_ad ; LDAP
   pair: LDAP ; Active Directory

.. _django_auth_ldap3_ad:

=============================================================================================
django-auth-ldap3_ad (Simple LDAP/AD auth module for django)
=============================================================================================

.. seealso::

   - https://github.com/Lucterios2/django_auth_ldap3_ad



.. toctree::
   :maxdepth: 3

   definition/definition
