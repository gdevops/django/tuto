
.. index::
   pair: django-auth-ldap ; Versions

.. _ldap_django_auth_ldap_versions:

=============================================================================================
django-auth-ldap versions
=============================================================================================

.. seealso::

   - https://github.com/django-auth-ldap/django-auth-ldap


.. toctree::
   :maxdepth: 3

   1.7.0/1.7.0
