
.. index::
   pair: django-auth-ldap ; LDAP
   pair: django-auth-ldap ; Authentication
   pair: django-auth-ldap ; Django

.. _ldap_django_auth_ldap:
.. _django_auth_ldap:

=============================================================================================
django-auth-ldap (Django authentication backend that authenticates against an LDAP service)
=============================================================================================

.. seealso::

   - https://github.com/django-auth-ldap/django-auth-ldap
   - https://django-auth-ldap.readthedocs.io/en/latest/
   - https://github.com/django-auth-ldap/django-auth-ldap/blob/master/django_auth_ldap/backend.py
   - https://github.com/django-auth-ldap/django-auth-ldap/blob/master/setup.py





README.rst
============

.. include:: README.rst


Blog
=====

.. seealso::

   - https://www.djm.org.uk/posts/using-django-auth-ldap-active-directory-ldaps/


Django LDAP configurations examples
=====================================

.. toctree::
   :maxdepth: 3

   configurations/configurations

Versions
=========


.. toctree::
   :maxdepth: 3

   versions/versions
