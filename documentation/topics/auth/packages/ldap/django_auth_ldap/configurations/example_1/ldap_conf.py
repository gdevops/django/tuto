"""ldap_conf.py

- https://django-auth-ldap.readthedocs.io/en/latest/



Searching sAMAccountName="msmith" and displaying all attributes
===================================================================

::

    ldapsearch -x -v -h pdc.int.jm4.eu \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="mycompany",dc=int,dc=jm4,dc=eu  \
    -s sub "(sAMAccountName=msmith)"


::

    ldap_initialize( ldap://pdc.int.jm4.eu )
    filter: (sAMAccountName=msmith)
    requesting: All userApplication attributes

::


    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=mycompany,dc=int,dc=jm4,dc=eu> with scope subtree
    # filter: (sAMAccountName=msmith)
    # requesting: ALL
    #

    # Martha SMITH, berlin, Users, mycompany, int.jm4.eu
    dn: CN=Martha SMITH,OU=berlin,OU=Users,OU=mycompany,DC=int,DC=i
     d3,DC=eu
    objectClass: top
    objectClass: person
    objectClass: organizationalPerson
    objectClass: user
    cn: Martha SMITH
    sn: SMITH
    c: DE
    l: berlin
    st:: SXPDqHJl
    description: Compte Utilisateur
    postalCode: 38120
    telephoneNumber: +33 4 76 75 75 85
    facsimileTelephoneNumber: +33 4 76 75 52 30
    givenName: Jules
    initials: PVE
    distinguishedName: CN=Martha SMITH,OU=berlin,OU=Users,OU=jm4 Technolog
     ies,DC=int,DC=jm4,DC=eu
    instanceType: 4
    whenCreated: 20170112125817.0Z
    whenChanged: 20190415144530.0Z
    displayName: Martha SMITH
    uSNCreated: 196753
    memberOf: CN=update_admin,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    memberOf: CN=ops,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    memberOf: CN=log_admin,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    memberOf: CN=jm4_all,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    memberOf: CN=jm4_etage,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    memberOf: CN=ESX Admins,CN=Users,DC=int,DC=jm4,DC=eu
    uSNChanged: 5969820
    department:: U2VydmljZXMgR8OpbsOpcmF1eA==
    company: mycompany
    streetAddress: 85, kropotkine avenue
    wWWHomePage: http://www.jm4.eu
    name: Martha SMITH
    objectGUID:: s+ejvcXxK0GaaC9WHKsmjg==
    userAccountControl: 512
    badPwdCount: 5
    codePage: 0
    countryCode: 0
    homeDirectory: \\files.srv.int.jm4.eu\msmith
    homeDrive: Z:
    badPasswordTime: 131998995971195966
    lastLogoff: 0
    lastLogon: 131995312168156184
    scriptPath: logon.bat
    pwdLastSet: 131484602237872238
    primaryGroupID: 2567
    objectSid:: AQUAAAAAAAUVAAAAWB0ASJ7NFHJC/jn0FwoAAA==
    accountExpires: 9223372036854775807
    logonCount: 3269
    sAMAccountName: msmith
    sAMAccountType: 805306368
    userPrincipalName: msmith@jm4.eu
    objectCategory: CN=Person,CN=Schema,CN=Configuration,DC=int,DC=jm4,DC=eu
    dSCorePropagationData: 20181109132941.0Z
    dSCorePropagationData: 20171017095326.0Z
    dSCorePropagationData: 16010101000000.0Z
    lastLogonTimestamp: 131998131154488876
    mail: martha.smith@jm4.eu@jm4.eu


Searching sAMAccountName=msmith  and displaying  sAMAccountName, sn and mail attributes
===========================================================================================

::

    ldapsearch -x -v -h pdc.int.jm4.eu \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="mycompany",dc=int,dc=jm4,dc=eu  \
    -s sub "(sAMAccountName=msmith)" \
    -s sub sAMAccountName sn mail

::

    ldap_initialize( ldap://pdc.int.jm4.eu )
    filter: (sAMAccountName=msmith)
    requesting: sAMAccountName sn mail
    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=mycompany,dc=int,dc=jm4,dc=eu> with scope subtree
    # filter: (sAMAccountName=msmith)
    # requesting: sAMAccountName sn mail
    #

    # Martha SMITH, berlin, Users, mycompany, int.jm4.eu
    dn: CN=Martha SMITH,OU=berlin,OU=Users,OU=mycompany,DC=int,DC=jm4,DC=eu
    sn: SMITH
    sAMAccountName: msmith
    mail: martha.smith@jm4.eu@jm4.eu

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1


Searching **update_admin** group
===========================================

::

    ldapsearch -x -v -h pdc.int.jm4.eu \
    -D "<username>" -w "<password>"  \
    -b cn="update_admin",ou=groups,ou="mycompany",dc=int,dc=jm4,dc=eu \
    -s sub "(objectclass=group)"

::

    ldap_initialize( ldap://pdc.int.jm4.eu )
    filter: (objectclass=group)
    requesting: All userApplication attributes
    # extended LDIF
    #
    # LDAPv3
    # base <cn=update_admin,ou=groups,ou=mycompany,dc=int,dc=jm4,dc=eu> with scope subtree
    # filter: (objectclass=group)
    # requesting: ALL
    #

    # update_admin, Groups, mycompany, int.jm4.eu
    dn: CN=update_admin,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    objectClass: top
    objectClass: group
    cn: update_admin
    member: CN=Martha SMITH,OU=berlin,OU=Users,OU=mycompany,DC=int,DC=jm4,DC=eu
    distinguishedName: CN=update_admin,OU=Groups,OU=mycompany,DC=int,DC=jm4,DC=eu
    instanceType: 4
    whenCreated: 20190412121023.0Z
    whenChanged: 20190412121054.0Z
    uSNCreated: 5934034
    uSNChanged: 5934046
    name: update_admin
    objectGUID:: SkBm/DnhFka+Ea78OF0dRw==
    objectSid:: AQUAAAAAAAUVAAAAWB0ASJ7NFHJC/jn0uwwAAA==
    sAMAccountName: update_admin
    sAMAccountType: 268435456
    groupType: -2147483646
    objectCategory: CN=Group,CN=Schema,CN=Configuration,DC=int,DC=jm4,DC=eu
    dSCorePropagationData: 16010101000000.0Z

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1
"""
# https://github.com/python-ldap/python-ldap
import ldap
from django_auth_ldap.config import GroupOfNamesType
from django_auth_ldap.config import LDAPSearch
from django_auth_ldap.config import LDAPSearchUnion

# DIT = Directory Information Tree
JM4_BASE_ANNUAIRE_DIT = "DC=int,DC=jm4,DC=eu"
# JM4 LDAP server
JM4_LDAP_SERVER = "ldap://pdc.int.jm4.eu"
AUTH_LDAP_SERVER_URI = JM4_LDAP_SERVER
AUTH_LDAP_BIND_DN = f"CN=apache,CN=users,{JM4_BASE_ANNUAIRE_DIT}"
AUTH_LDAP_BIND_PASSWORD = "<password>"

# LDAP research
JM4_LDAP_SEARCH_BASE = f"OU=mycompany,{JM4_BASE_ANNUAIRE_DIT}"
JM4_LDAP_SEARCH_USER_BASE = f"OU=users,{JM4_LDAP_SEARCH_BASE}"
JM4_LDAP_SEARCH_GROUP_BASE = f"OU=groups,{JM4_LDAP_SEARCH_BASE}"

AUTH_LDAP_USER_SEARCH = LDAPSearch(
    JM4_LDAP_SEARCH_USER_BASE, ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)"
)
AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    JM4_LDAP_SEARCH_GROUP_BASE, ldap.SCOPE_SUBTREE, "(objectClass=group)"
)
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType()

JM4_AUTHORIZED_GROUP = "update_admin"
AUTH_LDAP_REQUIRE_GROUP = f"CN={JM4_AUTHORIZED_GROUP},{JM4_LDAP_SEARCH_GROUP_BASE}"
print(
    f"AUTH_LDAP_REQUIRE_GROUP={AUTH_LDAP_REQUIRE_GROUP} JM4_AUTHORIZED_GROUP={JM4_AUTHORIZED_GROUP}"
)
# What to do once the user is authenticated
AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
    "email": "mail",
}

# This is the default, but I like to be explicit.
AUTH_LDAP_ALWAYS_UPDATE_USER = True

# https://django-auth-ldap.readthedocs.io/en/latest/reference.html#auth-ldap-mirror-groups
AUTH_LDAP_MIRROR_GROUPS = [JM4_AUTHORIZED_GROUP]

# Use LDAP group membership to calculate group permissions.
AUTH_LDAP_FIND_GROUP_PERMS = True


########### Paramètrage Django pour l'utilisation de LDAP ##############
AUTHENTICATION_BACKENDS = (
    "accounts.backends.Id3LDAPBackend",
    "accounts.backends.Id3AuthBackend",
)
# For ldap3 package https://ldap3.readthedocs.io/tutorial.html
LDAP3_USER = "<username>"
