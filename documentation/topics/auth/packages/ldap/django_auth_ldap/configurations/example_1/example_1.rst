
.. index::
   pair: Example 1 ; LDAP configurations

.. _django_ldap_conf_1:

=============================================================================================
LDAP conf example 1
=============================================================================================




DIT (Directory Information Tree)
==================================

.. figure:: dit_jm4.png
   :align: center

Initialize from base.py
=========================

.. code-block:: python
   :linenos:


    from . import ldap_conf
    from .ldap_conf import (
        AUTH_LDAP_SERVER_URI,
        AUTH_LDAP_BIND_DN,
        AUTH_LDAP_BIND_PASSWORD,
        AUTH_LDAP_USER_SEARCH,
        AUTH_LDAP_GROUP_SEARCH,
        AUTH_LDAP_USER_ATTR_MAP,
        AUTHENTICATION_BACKENDS,
        LDAP3_USER,
        AUTH_LDAP_REQUIRE_GROUP,
        AUTH_LDAP_GROUP_TYPE,
        AUTH_LDAP_ALWAYS_UPDATE_USER,
        AUTH_LDAP_MIRROR_GROUPS,
        AUTH_LDAP_FIND_GROUP_PERMS,
    )


ldap_conf.py
===============

.. literalinclude:: ldap_conf.py
   :linenos:
