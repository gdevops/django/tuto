
.. index::
   pair: Django ; Trench

.. _django_trench:

====================================================================================================================
Django Trench (provides a set of REST API endpoints to supplement DRF with multi-factor authentication (MFA, 2FA))
====================================================================================================================

.. seealso::

   - https://github.com/merixstudio/django-trench
   - https://django-trench.readthedocs.io/en/latest/index.html




Description
=============

**django-trench** provides a set of REST API endpoints to supplement
django-rest-framework with multi-factor authentication (MFA, 2FA).

It supports both standard built-in authentication methods, as well as
JWT (JSON Web Token).

django-trench follows the url pattern developed in :ref:`djoser library <djoser>`
and may act as its supplement.

We deliver a couple of sample secondary authentication methods including
sending OTP based code by email, SMS/text as well as through 3rd party mobile
apps or utilising YubiKey.

Developers can easily add own auth backend supporting any communication channel.
