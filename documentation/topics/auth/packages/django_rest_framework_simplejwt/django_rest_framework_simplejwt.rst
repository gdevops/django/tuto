
.. index::
   pair: django_rest_framework_simplejwt ; Authentication
   pair: JSON Web Token ; Authentication


.. _django_rest_framework_simplejwt.rst:

========================================================================================================
django_rest_framework_simplejwt (A JSON Web Token authentication plugin for the Django REST Framework)
========================================================================================================


.. seealso::

   - https://github.com/davesque/django-rest-framework-simplejwt
   - :ref:`django_trench`




Description
============

A JSON Web Token authentication plugin for the Django REST Framework.
