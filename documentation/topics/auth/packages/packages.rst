
.. index::
   pair: Django ; Authentication


.. _django_authentication:

========================================================================
Django authentication packages
========================================================================


.. seealso::

   - https://djoser.readthedocs.io/en/latest/authentication_backends.html#authentication-backends
   - https://django-trench.readthedocs.io/en/latest/backends.html


.. toctree::
   :maxdepth: 3


   django_trench/django_trench
   djoser/djoser
   django_rest_framework_simplejwt/django_rest_framework_simplejwt
   ldap/ldap
