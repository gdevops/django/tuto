
.. index::
   pair: Permission; customization
   pair: Permission; Création programmatique
   pair: Permission; personnalisée


.. _permissions_custom:

====================================================================================
Permissions personnalisées, création programmatique de permissions (customization)
====================================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/default/#programmatically-creating-permissions
   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/#custom-permissions





.. _perms_creation_program:

Création par programme des permissions
==========================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/default/#programmatically-creating-permissions


Bien que des permissions personnalisées puissent être définies dans la
classe Meta d’un modèle, il est aussi possible de créer directement des
permissions.

Par exemple, vous pouvez créer la permission can_publish d’un modèle
BlogPost dans myapp::

    from myapp.models import BlogPost
    from django.contrib.auth.models import Permission
    from django.contrib.contenttypes.models import ContentType

    content_type = ContentType.objects.get_for_model(BlogPost)
    permission = Permission.objects.create(
        codename='can_publish',
        name='Can Publish Posts',
        content_type=content_type,
    )

La permission peut ensuite être attribuée à un User par son attribut
user_permissions ou à un Group par son attribut permissions.


.. _perms_creation_meta:

Permissions personnalisées au niveau de l'attribut meta du modèle
===================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/#custom-permissions


Pour créer des permissions personnalisées pour un objet modèle donné,
utilisez l”attribut Meta permissions du modèle`.

Ce modèle d’exemple Task crée deux permissions personnalisées,
c’est-à-dire des actions que les utilisateurs peuvent effectuer
ou non avec les instances Task, spécifiquement à votre application.


.. code-block:: python
   :linenos:

    class Task(models.Model):
        ...
        class Meta:
            permissions = (
                ("change_task_status", "Can change the status of tasks"),
                ("close_task", "Can remove a task by setting its status as closed"),
            )

La seule conséquence de ce code est la création de ces permissions
supplémentaires lors du lancement de manage.py migrate (la fonction qui
crée les permissions est connectée au signal post_migrate).

Votre code a ensuite la charge de contrôler la valeur de ces permissions
lorsqu’un utilisateur essaie d’accéder à la fonctionnalité fournie par
l’application (modification de l’état des tâches ou fermeture des tâches).

En poursuivant sur l’exemple précédent, le code suivant contrôle si un
utilisateur peut fermer les tâches::

    user.has_perm('app.close_task')
