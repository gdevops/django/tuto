
.. index::
   pair: Permission; view


.. _view_permission_django_2_1:

====================================================================================
Permissions view avec django 2.1 (2018-008-01)
====================================================================================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/releases/2.1/





Quoi de neuf dans Django 2.1 : accès en **lecture seule** aux modèles dans l’interface d’administration
=========================================================================================================

Une permission d’affichage (**view**) a été ajoutée aux permissions
Meta.default_permissions des modèles.

Les nouvelles permissions seront automatiquement créées lors du lancement
de migrate.

Cela permet de donner un accès en **lecture seule** aux modèles dans
l’interface d’administration.

ModelAdmin.has_view_permission() est une nouvelle méthode.

L’implémentation est rétrocompatible dans la mesure où il n’est pas
nécessaire d’attribuer la permission view pour permettre aux utilisateurs
qui ont la permission de modification de modifier des objets.
