
.. index::
   pair: Permission; Permissions autorisations
   pair: Permission; Permissions authorization
   pair: Permission; PermissionManager
   pair: Permission; PermissionRequiredMixin
   pair: Permission; permission_required
   pair: gabarits; perms
   pair: User; has_perm
   ! Permission


.. _permissions_autorisations:

===============================================================================
Permissions et autorisations (Permission, PermissionManager)
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/auth/default/#permissions-and-authorization
   - https://github.com/django/django/blob/master/django/contrib/auth
   - :ref:`django_user`
   - :ref:`django_group`






La nouvelle permission **view** avec django 2.1
==================================================

.. toctree::
   :maxdepth: 3

   view/view


Description
==============

Django contient un système simple de permissions.

Il permet d’attribuer des permissions à des utilisateurs ou des
**groupes d’utilisateurs particuliers**.

Ce système est utilisé par le site d’administration de Django, mais vous
avez tout loisir de l’utiliser dans **votre propre code**.

Le site d’administration de Django utilise les permissions ainsi :

- L’accès à l’affichage d’un objet est limité aux utilisateurs possédant
  la permission **view** ou **change** pour ce type d’objet.
- L’accès au formulaire d’ajout et l’ajout d’objet sont limités aux
  utilisateurs possédant la permission **add** pour ce type d’objet.
- L’accès à la liste des objets pour modification, au formulaire de
  modification et la modification d’un objet sont limités aux utilisateurs
  possédant la permission **change** pour ce type d’objet.
- L’accès à la suppression d’un objet est limité aux utilisateurs
  possédant la permission **delete** pour ce type d’objet.


Opérations de base sur les permissions
==========================================

::

    In [4]: u2=User.objects.get(username='test1')


Liste de toutes les permissions
---------------------------------


::

    In [5]: u2.get_all_permissions()
    Out[5]:
    {'projets.add_projet',
     'projets.change_projet',
     'projets.delete_projet',
     'projets.view_projet'}


L'utilisateur a-t-il toutes les permissions sur une application ?
--------------------------------------------------------------------

::

    In [8]: u2.has_module_perms('projets')
    Out[8]: True

::

    In [9]: u2.has_module_perms('logs')

::

    Out[9]: False



L'utilisateur peut-il modifier un projet ?
--------------------------------------------

::

    In [6]: u2.has_perm('projets.change_projet')
    Out[6]: True


Créer une permission sur un modèle
-------------------------------------


Par exemple, vous pouvez créer la permission can_publish d’un modèle
Article dans articles::

    from articles.models import Article
    from django.contrib.auth.models import Permission
    from django.contrib.contenttypes.models import ContentType

    content_type = ContentType.objects.get_for_model(Article)
    permission = Permission.objects.create(
        codename='can_publish',
        name='Can Publish Articles',
        content_type=content_type,
    )

La permission peut ensuite être attribuée à un User par son attribut
user_permissions ou à un Group par son attribut permissions.

::

    In [21]: content_type = ContentType.objects.get_for_model(Article)

    In [22]:     permission = Permission.objects.create(
        ...:         codename='can_publish',
        ...:         name='Can Publish Articles',
        ...:         content_type=content_type,
        ...:     )

    In [23]: u2.user_permissions.add(permission)


::

    In [24]: u2.user_permissions.all()
    Out[24]: <QuerySet [<Permission: articles | article | Can Publish Articles>]>


.. _default_permissions:

Permissions par défaut
=======================

.. seealso::

   - https://docs.djangoproject.com/fr/2.1/topics/auth/default/

Quand l’application django.contrib.auth figure dans le réglage INSTALLED_APPS,
elle s’assure que les quatre permissions par défaut (« add », « change », « delete » et « view »)
soient créées pour chaque modèle Django défini dans toutes les applications installées.

Ces permissions seront créées lorsque vous lancez manage.py migrate; au
premier lancement de migrate après l’ajout de django.contrib.auth à INSTALLED_APPS,
les permissions par défaut seront créées pour tous les modèles précédemment
installés de même que pour tout nouveau modèle installé à ce moment-là.

Par la suite, chaque nouvelle exécution de manage.py migrate crée les
permissions par défaut pour les nouveaux modèles (la fonction qui crée
les permissions est connectée au signal post_migrate).

En supposant qu’une application ait l’attribut app_label foo et qu’un
modèle soit nommé Bar, voici comment il faut tester les permissions de base::

    ajout :        user.has_perm('foo.add_bar')
    modification : user.has_perm('foo.change_bar')
    suppression :  user.has_perm('foo.delete_bar')
    affichage  :   user.has_perm('foo.view_bar')

Le modèle Permission est rarement accédé directement.



Utilisation des permissions dans les gabarits (templates)
===========================================================

Variable {{ perms }}
----------------------

Les permissions de l’utilisateur actuellement connecté sont stockés dans
la variable de gabarit {{ perms }}.

C’est une instance de django.contrib.auth.context_processors.PermWrapper,
un objet mandataire de permissions adapté aux gabarits.

La consultation d’attributs simples de {{ perms }} sous forme de booléen
est une méthode mandataire de User.has_module_perms().

Par exemple, pour vérifier si l’utilisateur connecté a au moins une
permission dans l’application foo::

    {% if perms.foo %}

La consultation d’attributs à deux niveaux de {{ perms }} sous forme de
booléen est une méthode mandataire de User.has_perm().

Par exemple, pour vérifier si l’utilisateur connecté a la permission
foo.can_vote::

    {% if perms.foo.can_vote %}

Voici un exemple plus complet de contrôle de permissions dans un gabarit.


.. code-block:: django
   :linenos:

    {% if perms.foo %}
        <p>You have permission to do something in the foo app.</p>
        {% if perms.foo.can_vote %}
            <p>You can vote!</p>
        {% endif %}
        {% if perms.foo.can_drive %}
            <p>You can drive!</p>
        {% endif %}
    {% else %}
        <p>You don't have permission to do anything in the foo app.</p>
    {% endif %}



Il est aussi possible de consulter les permissions par des instructions
{% if in %}.

Par exemple::

    {% if 'foo' in perms %}
        {% if 'foo.can_vote' in perms %}
            <p>In lookup works, too.</p>
        {% endif %}
    {% endif %}



.. _PermissionManager:

class **contrib.auth.models.PermissionManager**
-------------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py


.. code-block:: python
   :linenos:

    class PermissionManager(models.Manager):
        use_in_migrations = True

        def get_by_natural_key(self, codename, app_label, model):
            return self.get(
                codename=codename,
                content_type=ContentType.objects.db_manager(self.db).get_by_natural_key(app_label, model),
            )


.. _Permission:

class **contrib.auth.models.Permission**
-----------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py


.. code-block:: python
   :linenos:

    class Permission(models.Model):
        """
        The permissions system provides a way to assign permissions to specific
        users and groups of users.

        The permission system is used by the Django admin site, but may also be
        useful in your own code. The Django admin site uses permissions as follows:

            - The "add" permission limits the user's ability to view the "add" form
              and add an object.
            - The "change" permission limits a user's ability to view the change
              list, view the "change" form and change an object.
            - The "delete" permission limits the ability to delete an object.
            - The "view" permission limits the ability to view an object.

        Permissions are set globally per type of object, not per specific object
        instance. It is possible to say "Mary may change news stories," but it's
        not currently possible to say "Mary may change news stories, but only the
        ones she created herself" or "Mary may only change news stories that have a
        certain status or publication date."

        The permissions listed above are automatically created for each model.
        """
        name = models.CharField(_('name'), max_length=255)
        content_type = models.ForeignKey(
            ContentType,
            models.CASCADE,
            verbose_name=_('content type'),
        )
        codename = models.CharField(_('codename'), max_length=100)

        objects = PermissionManager()

        class Meta:
            verbose_name = _('permission')
            verbose_name_plural = _('permissions')
            unique_together = (('content_type', 'codename'),)
            ordering = ('content_type__app_label', 'content_type__model',
                        'codename')

        def __str__(self):
            return '%s | %s' % (self.content_type, self.name)

        def natural_key(self):
            return (self.codename,) + self.content_type.natural_key()
        natural_key.dependencies = ['contenttypes.contenttype']



.. _PermissionsMixin:

class **contrib.auth.models.PermissionsMixin**
------------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py
   - :ref:`groups_for_user`
   - :ref:`users_for_group`



.. code-block:: python
   :linenos:

    class PermissionsMixin(models.Model):
        """
        Add the fields and methods necessary to support the Group and Permission
        models using the ModelBackend.
        """
        is_superuser = models.BooleanField(
            _('superuser status'),
            default=False,
            help_text=_(
                'Designates that this user has all permissions without '
                'explicitly assigning them.'
            ),
        )
        groups = models.ManyToManyField(
            Group,
            verbose_name=_('groups'),
            blank=True,
            help_text=_(
                'The groups this user belongs to. A user will get all permissions '
                'granted to each of their groups.'
            ),
            related_name="user_set",
            related_query_name="user",
        )
        user_permissions = models.ManyToManyField(
            Permission,
            verbose_name=_('user permissions'),
            blank=True,
            help_text=_('Specific permissions for this user.'),
            related_name="user_set",
            related_query_name="user",
        )

        class Meta:
            abstract = True

        def get_group_permissions(self, obj=None):
            """
            Return a list of permission strings that this user has through their
            groups. Query all available auth backends. If an object is passed in,
            return only permissions matching this object.
            """
            permissions = set()
            for backend in auth.get_backends():
                if hasattr(backend, "get_group_permissions"):
                    permissions.update(backend.get_group_permissions(self, obj))
            return permissions

        def get_all_permissions(self, obj=None):
            return _user_get_all_permissions(self, obj)

        def has_perm(self, perm, obj=None):
            """
            Return True if the user has the specified permission. Query all
            available auth backends, but return immediately if any backend returns
            True. Thus, a user who has permission from a single auth backend is
            assumed to have permission in general. If an object is provided, check
            permissions for that object.
            """
            # Active superusers have all permissions.
            if self.is_active and self.is_superuser:
                return True

            # Otherwise we need to check the backends.
            return _user_has_perm(self, perm, obj)

        def has_perms(self, perm_list, obj=None):
            """
            Return True if the user has each of the specified permissions. If
            object is passed, check if the user has all required perms for it.
            """
            return all(self.has_perm(perm, obj) for perm in perm_list)

        def has_module_perms(self, app_label):
            """
            Return True if the user has any permissions in the given app label.
            Use similar logic as has_perm(), above.
            """
            # Active superusers have all permissions.
            if self.is_active and self.is_superuser:
                return True

            return _user_has_module_perms(self, app_label)



.. _PermissionRequiredMixin:

class **contrib.auth.mixins.PermissionRequiredMixin**
---------------------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/mixins.py

.. code-block:: python
   :linenos:


    class PermissionRequiredMixin(AccessMixin):
        """Verify that the current user has all specified permissions."""
        permission_required = None

        def get_permission_required(self):
            """
            Override this method to override the permission_required attribute.
            Must return an iterable.
            """
            if self.permission_required is None:
                raise ImproperlyConfigured(
                    '{0} is missing the permission_required attribute. Define {0}.permission_required, or override '
                    '{0}.get_permission_required().'.format(self.__class__.__name__)
                )
            if isinstance(self.permission_required, str):
                perms = (self.permission_required,)
            else:
                perms = self.permission_required
            return perms

        def has_permission(self):
            """
            Override this method to customize the way permissions are checked.
            """
            perms = self.get_permission_required()
            return self.request.user.has_perms(perms)

        def dispatch(self, request, *args, **kwargs):
            if not self.has_permission():
                return self.handle_no_permission()
            return super().dispatch(request, *args, **kwargs)


.. _permission_required:

class contrib.auth.decorators.permission_required
--------------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/decorators.py

.. code-block:: python
   :linenos:


    from django.core.exceptions import PermissionDenied

    ...


    def permission_required(perm, login_url=None, raise_exception=False):
        """
        Decorator for views that checks whether a user has a particular permission
        enabled, redirecting to the log-in page if necessary.
        If the raise_exception parameter is given the PermissionDenied exception
        is raised.
        """
        def check_perms(user):
            if isinstance(perm, str):
                perms = (perm,)
            else:
                perms = perm
            # First check if the user has the permission (even anon users)
            if user.has_perms(perms):
                return True
            # In case the 403 handler should be called raise the exception
            if raise_exception:
                raise PermissionDenied
            # As the last resort, show the login form
            return False
        return user_passes_test(check_perms, login_url=login_url)


Permissions personnalisées
=============================

.. toctree::
   :maxdepth: 3

   customizing/customizing
