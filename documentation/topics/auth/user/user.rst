
.. index::
   pair: Django; CustomUser
   pair: Django; User
   pair: Django; AbstractBaseUser
   pair: Django; AbstractUser
   pair: Django; AnonymousUser
   pair: Django; BaseUserManager
   pair: Django; UserManager
   pair: groups; User


.. _django_user:

=================================================
Django **User** and **CustomUser**
=================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/auth/#django.contrib.auth.models.User
   - https://docs.djangoproject.com/en/dev/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
   - :ref:`django_group`
   - :ref:`permissions_autorisations`





Custom users examples
===========================

.. toctree::
   :maxdepth: 3

   customizing/customizing


Bas operations with a custom user
======================================


::

    [8]: user = User.objects.get(username="pvergain")

    In [9]: user.get_all_permissions()
    Out[9]:
    {'accounts.add_user',
     'accounts.change_user',
     'accounts.delete_user',
     'accounts.view_user',
     'admin.add_logentry',
     'admin.change_logentry',
     'admin.delete_logentry',
     'admin.view_logentry',



.. _add_group_for_user:

Ajout d'un groupe pour un utilisateur
----------------------------------------


::

    In [1]: g1 = Group.objects.create(name='test_create_groupe')

    In [2]: g1
    Out[2]: <Group: test_create_groupe>

    In [3]: u=User.objects.get(username='pvergain')

    In [7]: u.groups.add(g1)


.. _groups_for_user:

Lister tous les groupes d'un utilisateur (**groups**)
--------------------------------------------------------

::

    In [19]: u.groups.all()
    Out[19]: <QuerySet [<Group: test_create_groupe>]>


Description
==============

.. seealso::

   - :ref:`auth_models`


.. _BaseUserManager:

class contrib.auth.base_user.BaseUserManager
----------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/base_user.py

.. code-block:: python
   :linenos:


    from django.utils.crypto import get_random_string, salted_hmac

    class BaseUserManager(models.Manager):

        @classmethod
        def normalize_email(cls, email):
            """
            Normalize the email address by lowercasing the domain part of it.
            """
            email = email or ''
            try:
                email_name, domain_part = email.strip().rsplit('@', 1)
            except ValueError:
                pass
            else:
                email = email_name + '@' + domain_part.lower()
            return email

        def make_random_password(self, length=10,
                                 allowed_chars='abcdefghjkmnpqrstuvwxyz'
                                               'ABCDEFGHJKLMNPQRSTUVWXYZ'
                                               '23456789'):
            """
            Generate a random password with the given length and given
            allowed_chars. The default value of allowed_chars does not have "I" or
            "O" or letters and digits that look similar -- just to avoid confusion.
            """
            return get_random_string(length, allowed_chars)

        def get_by_natural_key(self, username):
            return self.get(**{self.model.USERNAME_FIELD: username})


.. _AbstractBaseUser:

class **contrib.auth.base_user.AbstractBaseUser**
----------------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/base_user.py

.. code-block:: python
   :linenos:


    class AbstractBaseUser(models.Model):
        password = models.CharField(_('password'), max_length=128)
        last_login = models.DateTimeField(_('last login'), blank=True, null=True)

        is_active = True

        REQUIRED_FIELDS = []

        # Stores the raw password if set_password() is called so that it can
        # be passed to password_changed() after the model is saved.
        _password = None

        class Meta:
            abstract = True

        def __str__(self):
            return self.get_username()

        def save(self, *args, **kwargs):
            super().save(*args, **kwargs)
            if self._password is not None:
                password_validation.password_changed(self._password, self)
                self._password = None

        def get_username(self):
            """Return the username for this User."""
            return getattr(self, self.USERNAME_FIELD)

        def clean(self):
            setattr(self, self.USERNAME_FIELD, self.normalize_username(self.get_username()))

        def natural_key(self):
            return (self.get_username(),)

        @property
        def is_anonymous(self):
            """
            Always return False. This is a way of comparing User objects to
            anonymous users.
            """
            return False

        @property
        def is_authenticated(self):
            """
            Always return True. This is a way to tell if the user has been
            authenticated in templates.
            """
            return True

        def set_password(self, raw_password):
            self.password = make_password(raw_password)
            self._password = raw_password

        def check_password(self, raw_password):
            """
            Return a boolean of whether the raw_password was correct. Handles
            hashing formats behind the scenes.
            """
            def setter(raw_password):
                self.set_password(raw_password)
                # Password hash upgrades shouldn't be considered password changes.
                self._password = None
                self.save(update_fields=["password"])
            return check_password(raw_password, self.password, setter)

        def set_unusable_password(self):
            # Set a value that will never be a valid hash
            self.password = make_password(None)

        def has_usable_password(self):
            """
            Return False if set_unusable_password() has been called for this user.
            """
            return is_password_usable(self.password)

        def get_session_auth_hash(self):
            """
            Return an HMAC of the password field.
            """
            key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
            return salted_hmac(key_salt, self.password).hexdigest()

        @classmethod
        def get_email_field_name(cls):
            try:
                return cls.EMAIL_FIELD
            except AttributeError:
                return 'email'

        @classmethod
        def normalize_username(cls, username):
            return unicodedata.normalize('NFKC', username) if isinstance(username, str) else username


.. _AbstractUser:

class **contrib.auth.models.AbstractUser**
---------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py



.. code-block:: python
   :linenos:

    class AbstractUser(AbstractBaseUser, PermissionsMixin):
        """
        An abstract base class implementing a fully featured User model with
        admin-compliant permissions.

        Username and password are required. Other fields are optional.
        """
        username_validator = UnicodeUsernameValidator()

        username = models.CharField(
            _('username'),
            max_length=150,
            unique=True,
            help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
            validators=[username_validator],
            error_messages={
                'unique': _("A user with that username already exists."),
            },
        )
        first_name = models.CharField(_('first name'), max_length=30, blank=True)
        last_name = models.CharField(_('last name'), max_length=150, blank=True)
        email = models.EmailField(_('email address'), blank=True)
        is_staff = models.BooleanField(
            _('staff status'),
            default=False,
            help_text=_('Designates whether the user can log into this admin site.'),
        )
        is_active = models.BooleanField(
            _('active'),
            default=True,
            help_text=_(
                'Designates whether this user should be treated as active. '
                'Unselect this instead of deleting accounts.'
            ),
        )
        date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

        objects = UserManager()

        EMAIL_FIELD = 'email'
        USERNAME_FIELD = 'username'
        REQUIRED_FIELDS = ['email']

        class Meta:
            verbose_name = _('user')
            verbose_name_plural = _('users')
            abstract = True

        def clean(self):
            super().clean()
            self.email = self.__class__.objects.normalize_email(self.email)

        def get_full_name(self):
            """
            Return the first_name plus the last_name, with a space in between.
            """
            full_name = '%s %s' % (self.first_name, self.last_name)
            return full_name.strip()

        def get_short_name(self):
            """Return the short name for the user."""
            return self.first_name

        def email_user(self, subject, message, from_email=None, **kwargs):
            """Send an email to this user."""
            send_mail(subject, message, from_email, [self.email], **kwargs)



.. _User_model:

class contrib.auth.models.User
--------------------------------

.. code-block:: python
   :linenos:

    class User(AbstractUser):
        """
        Users within the Django authentication system are represented by this
        model.

        Username and password are required. Other fields are optional.
        """
        class Meta(AbstractUser.Meta):
            swappable = 'AUTH_USER_MODEL'



.. _AnonymousUser:

class contrib.auth.models.AnonymousUser
------------------------------------------

.. code-block:: python
   :linenos:

    class AnonymousUser:
        id = None
        pk = None
        username = ''
        is_staff = False
        is_active = False
        is_superuser = False
        _groups = EmptyManager(Group)
        _user_permissions = EmptyManager(Permission)

        def __str__(self):
            return 'AnonymousUser'

        def __eq__(self, other):
            return isinstance(other, self.__class__)

        def __hash__(self):
            return 1  # instances always return the same hash value

        def __int__(self):
            raise TypeError('Cannot cast AnonymousUser to int. Are you trying to use it in place of User?')

        def save(self):
            raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

        def delete(self):
            raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

        def set_password(self, raw_password):
            raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

        def check_password(self, raw_password):
            raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

        @property
        def groups(self):
            return self._groups

        @property
        def user_permissions(self):
            return self._user_permissions

        def get_group_permissions(self, obj=None):
            return set()

        def get_all_permissions(self, obj=None):
            return _user_get_all_permissions(self, obj=obj)

        def has_perm(self, perm, obj=None):
            return _user_has_perm(self, perm, obj=obj)

        def has_perms(self, perm_list, obj=None):
            return all(self.has_perm(perm, obj) for perm in perm_list)

        def has_module_perms(self, module):
            return _user_has_module_perms(self, module)

        @property
        def is_anonymous(self):
            return True

        @property
        def is_authenticated(self):
            return False

        def get_username(self):
            return self.username



.. _UserManager:

UserManager
---------------

.. code-block:: python
   :linenos:


    class UserManager(BaseUserManager):
        use_in_migrations = True

        def _create_user(self, username, email, password, **extra_fields):
            """
            Create and save a user with the given username, email, and password.
            """
            if not username:
                raise ValueError('The given username must be set')
            email = self.normalize_email(email)
            username = self.model.normalize_username(username)
            user = self.model(username=username, email=email, **extra_fields)
            user.set_password(password)
            user.save(using=self._db)
            return user

        def create_user(self, username, email=None, password=None, **extra_fields):
            extra_fields.setdefault('is_staff', False)
            extra_fields.setdefault('is_superuser', False)
            return self._create_user(username, email, password, **extra_fields)

        def create_superuser(self, username, email, password, **extra_fields):
            extra_fields.setdefault('is_staff', True)
            extra_fields.setdefault('is_superuser', True)

            if extra_fields.get('is_staff') is not True:
                raise ValueError('Superuser must have is_staff=True.')
            if extra_fields.get('is_superuser') is not True:
                raise ValueError('Superuser must have is_superuser=True.')

            return self._create_user(username, email, password, **extra_fields)



Gestion des utilisateurs dans le site d’administration
========================================================

Quand django.contrib.admin et django.contrib.auth sont les deux installés,
le site d’administration offre une interface pratique pour afficher et
gérer les utilisateurs, les groupes et les permissions.

Les utilisateurs peuvent être créés et supprimés comme tout autre modèle
Django.

Des groupes peuvent être créés et des permissions peuvent être attribuées
aux utilisateurs et aux groupes.

Les modifications de modèles effectuées dans l’administration par les
utilisateurs sont également journalisées et affichées.
