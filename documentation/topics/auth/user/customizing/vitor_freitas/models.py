"""Définition d'un modèle User personnalisé User(table auth_user).

Django permet de surcharger le modèle d’utilisateur par défaut en attribuant
une valeur au réglage AUTH_USER_MODEL se référant à un modèle personnalisé

    AUTH_USER_MODEL = 'accounts.User'

Voir

- https://docs.djangoproject.com/fr/1.11/ref/contrib/auth/
- https://docs.djangoproject.com/fr/1.10/topics/auth/customizing/#auth-custom-user

- Source: https://makina-corpus.com/blog/metier/2014/combiner-une-authentification-ldap-et-lauthentification-classique-django


"""
import logging

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _

# https://docs.djangoproject.com/en/dev/topics/auth/customizing/#extending-the-existing-user-model

# Get an instance of a logger
logger = logging.getLogger(__name__)


class CustomUser(AbstractUser):
    """Un utilisateur authentifié intranet (auth_user)

    .. seealso:: https://code.djangoproject.com/ticket/25313


    I did it at least twice. Unfortunately I don't remember all the details.

    I think a reasonable procedure is:

    - Create a custom user model identical to auth.User, call it User
      (so many-to-many tables keep the same name) and
      set db_table='auth_user' (so it uses the same table)
    - Throw away all your migrations
    - Recreate a fresh set of migrations
    - Sacrifice a chicken, perhaps two if you're anxious; also make a backup
      of your database
    - Truncate the django_migrations table
    - Fake-apply the new set of migrations
    - Unset db_table, make other changes to the custom model,
      generate migrations, apply them

    It is highly recommended to do this on a database that enforces
    foreign key constraints.
    Don't try this on SQLite on your laptop and expect it to work on
    Postgres on the servers!

    - username

    """

    class Meta:
        """Meta class accounts.User."""

        managed = True
        db_table = "auth_user"
        ordering = ["username"]
        verbose_name = _("Comptes utilisateur")
        verbose_name_plural = _("Comptes utilisateur")

    from_ldap = models.BooleanField(
        editable=False,
        default=False,
        help_text=_("Issu de la base LDAP ?"),
        verbose_name=_("Issu de la base LDAP ?"),
    )

    def __str__(self):
        infos = f"{self.first_name} {self.last_name}"
        return infos

    def save(self, *args, **kwargs):
        """Sauvegarde de User."""
        try:
            logger.info("User.save({})".format(self))
            super().save(*args, **kwargs)
            logger.info("... {} was successfully saved.".format(self))
        except Exception as error:
            logger.info("...{} was NOT saved. PROBLEM:{}".format(self, error))

    def updated_from_ldap(self, nom, prenom, email):
        """Les informations en provenance de LDAP different-elles
        des informations courantes ?
        """
        updated_from_ldap = False

        nom = nom.strip()
        prenom = prenom.strip()

        identifiant = self.username
        sprenom = self.first_name
        snom = self.last_name
        semail = self.email

        # analyse des cas de modifications

        # 1) le nom, prénom ou email a été changé
        if snom != nom:
            self.last_name = nom
            logger.info(f"{identifiant} a changé de nom <{snom}>=><{nom}>")
            updated_from_ldap = True

        if sprenom != prenom:
            self.first_name = prenom
            logger.info(f"Le prénom de {identifiant} a changé {sprenom}=>{prenom}")
            updated_from_ldap = True

        if semail != email:
            self.email = email
            logger.info(
                f"L'adresse couriel de {identifiant} a changé {semail}=>{email}"
            )
            updated_from_ldap = True

        # 2) le email passe à 'nomail@id3.eu' ce qui signifie que cet utilisateur
        # ne fait plus partie d'id3
        employe_id3 = True
        if (self.is_active) and (email == "nomail@id3.eu"):
            # l'utilisateur n'est plus actif
            self.is_active = False
            self.is_staff = False
            self.email = email
            logger.info(f"{identifiant} n'est plus actif")
            employe_id3 = False
            updated_from_ldap = True

        if updated_from_ldap:
            try:
                self.save()
                if not employe_id3:
                    # l'employé ne fait plus partie d'id3
                    self.a_quitte_id3()

            except:
                updated_from_ldap = False

        return updated_from_ldap

    def a_quitte_id3(self):
        """Départ d'un employé."""
        from employes.models import Employe

        try:
            employe = Employe.objects.get(user=self)
            employe.a_quitte_id3()
            return employe
        except Exception as error:
            logger.error("No employe for LDAP user: {self}")
            return None

    def get_employe(self):
        """Retourne l'employé associé à l'utilisateur LDAP."""
        from employes.models import Employe

        # logger.info(f"account <{self}>")
        try:
            employe = Employe.objects.get(login=self.username)
            return employe
        except Exception as error:
            logger.error(f"No employe for user: <{self}>")
            return None

    def get_employe_id(self):
        """Retourne l'id de l'employé associé à l'utilisateur LDAP."""
        from employes.models import Employe

        employe_id = None
        employe = self.get_employe()
        if employe != None:
            employe_id = employe.id

        return employe_id
