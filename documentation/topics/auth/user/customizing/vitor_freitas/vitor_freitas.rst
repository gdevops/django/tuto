
.. index::
   pair: custom-user-model; Vitor Freitas

.. _vitor_freitas_2018_01_18:

===========================================================================================
(2018-01-18) **Vitor Freitas** => **Never use the built-in Django User model** directly
===========================================================================================


.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
   - https://simpleisbetterthancomplex.com/tutorial/2018/01/18/how-to-implement-multiple-user-types-with-django.html
   - https://github.com/sibtc/django-multiple-user-types-example
   - https://x.com/vitorfs




Advice
=======

**Never user the built-in Django User model directly**, even if the built-in
Django User implementation fulfill all the requirements of your application.

**At least extend the AbstractUser model and switch the AUTH_USER_MODEL on your
settings**.

**Requirements always change**.

You may need to customize the User model in the future, and switching the
AUTH_USER_MODEL after your application is in production will be very painful.

Mainly because you will need to update all the foreign keys to the User model.

It can be done, but this simple measure (which, honestly, is effortless in the
beginning of the project) can save you from headaches in the future.


.. code-block:: python
   :linenos:


    class User(AbstractUser):
      USER_TYPE_CHOICES = (
          (1, 'student'),
          (2, 'teacher'),
          (3, 'secretary'),
          (4, 'supervisor'),
          (5, 'admin'),
      )

      user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)


.. code-block:: python
   :linenos:

    class Role(models.Model):
      '''
      The Role entries are managed by the system,
      automatically created via a Django data migration.
      '''
      STUDENT = 1
      TEACHER = 2
      SECRETARY = 3
      SUPERVISOR = 4
      ADMIN = 5
      ROLE_CHOICES = (
          (STUDENT, 'student'),
          (TEACHER, 'teacher'),
          (SECRETARY, 'secretary'),
          (SUPERVISOR, 'supervisor'),
          (ADMIN, 'admin'),
      )

      id = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, primary_key=True)

      def __str__(self):
          return self.get_id_display()


    class User(AbstractUser):
      roles = models.ManyToManyField(Role)


Description
==============

.. literalinclude:: models.py
   :linenos:
