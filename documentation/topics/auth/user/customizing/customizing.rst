
.. index::
   pair: Custom; User
   pair: Django; Custom
   !  Custom; User

.. _django_user_custom:

===============================================================================
Custom user examples
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
   - https://docs.djangoproject.com/en/2.1/topics/auth/customizing/#specifying-custom-user-model
   - https://docs.djangoproject.com/fr/2.1/topics/auth/customizing/#extending-the-existing-user-model

.. toctree::
   :maxdepth: 4

   caktus/caktus
   michael_herman/michael_herman
   pretalx/pretalx
   vitor_freitas/vitor_freitas
   william_vincent/william_vincent
