
.. index::
   pair: custom-user-model; William S. Vincent

.. _william_vincent_2018_10_26:

===========================================================================
(2018-10-26) **William S. Vincent** => **always** use a custom user model
===========================================================================


.. seealso::

   - https://wsvincent.com/django-custom-user-model-tutorial/
   - https://x.com/wsv3000




Advice
=======

Django ships with a built-in User model for authentication, however the
official Django documentation highly recommends using a custom user model
for new projects.

The reason is if you want to make any changes to the User model down the
road–for example adding a date of birth field–using a custom user model
from the beginning makes this quite easy.

But if you do not, updating the default User model in an existing Django
project is very, very challenging.

.. warning:: So always use a custom user model for all new Django projects.

.. code-block:: python
   :linenos:


    # users/models.py
    from django.contrib.auth.models import AbstractUser
    from django.db import models

    class CustomUser(AbstractUser):
        # add additional fields in here

        def __str__(self):
            return self.email
