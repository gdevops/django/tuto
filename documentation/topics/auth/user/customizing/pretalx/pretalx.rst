
.. index::
   pair: custom-user-model; pretalx

.. _pretalx_custom_user_model:

===============================================================================
**pretalx** person.models.user.py
===============================================================================

.. seealso::

   - https://github.com/pretalx/pretalx/blob/master/src/pretalx/person/models/user.py




user.py
==============

.. literalinclude:: user.py
   :linenos:


test_user_model.py
=====================

.. literalinclude:: test_user_model.py
   :linenos:
