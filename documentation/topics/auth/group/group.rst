
.. index::
   pair: Django; User
   pair: Django; Group
   pair: Group; user_set

.. _django_group:

===============================================================================
Django Group
===============================================================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/contrib/auth/#group-model
   - https://docs.djangoproject.com/fr/2.1/topics/auth/default/#groups
   - :ref:`permissions_autorisations`
   - :ref:`django_user`





Opération de base avec les groupes
======================================


Création d'un groupe
-----------------------

::

    In [1]: g1 = Group.objects.create(name='test_create_groupe')



Liste de tous les groupes
----------------------------

::

    In [4]: from django.contrib.auth.models import Group
    In [5]: groups = Group.objects.all()

    In [21]: groups
    Out[21]: <QuerySet [<Group: groupe_gerer_projets>, <Group: groupe_creer_demandes>, <Group: groupe_traiter_demandes>, <Group: test_create_groupe>]>



.. _add_user_for_group:

Ajouter un user dans un groupe
--------------------------------

::

    from django.contrib.auth.models import Group
    my_group = Group.objects.get(name='my_group_name')
    my_group.user_set.add(your_user)



.. _users_for_group:

Lister les utilisateurs d'un groupe (**user_set**)
-----------------------------------------------------

::

    In [19]: g1.user_set.all()
    Out[19]: <QuerySet [<User: Patrick VERGAIN>]>


Description
==============

.. seealso::

   - :ref:`auth_models`


Les modèles **django.contrib.auth.models.Group** sont une manière générique
de segmenter les utilisateurs afin de pouvoir leur attribuer des permissions
ou de les désigner par un nom défini.

Un utilisateur peut appartenir à autant de groupes que nécessaire.

Un utilisateur d’un groupe reçoit automatiquement les permissions
attribuées à ce groupe. Par exemple, si le groupe Éditeurs du site
possède la permission can_edit_home_page, tout utilisateur membre du
groupe possédera cette permission.

Au-delà des permissions, les groupes sont un bon moyen de regrouper les
utilisateurs sous un nom défini, afin de leur attribuer certaines
fonctionnalités particulières.

Par exemple, vous pourriez créer un groupe Utilisateurs spéciaux et
écrire ensuite du code qui leur donne accès à une section réservée
aux membres sur le site, ou leur envoyer des courriels s’adressant
uniquement aux membres.


.. _Group_model:

class **contrib.auth.models.Group**
---------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py



.. code-block:: python
   :linenos:


    class Group(models.Model):
        """
        Groups are a generic way of categorizing users to apply permissions, or
        some other label, to those users. A user can belong to any number of
        groups.

        A user in a group automatically has all the permissions granted to that
        group. For example, if the group 'Site editors' has the permission
        can_edit_home_page, any user in that group will have that permission.

        Beyond permissions, groups are a convenient way to categorize users to
        apply some label, or extended functionality, to them. For example, you
        could create a group 'Special users', and you could write code that would
        do special things to those users -- such as giving them access to a
        members-only portion of your site, or sending them members-only email
        messages.
        """
        name = models.CharField(_('name'), max_length=150, unique=True)
        permissions = models.ManyToManyField(
            Permission,
            verbose_name=_('permissions'),
            blank=True,
        )

        objects = GroupManager()

        class Meta:
            verbose_name = _('group')
            verbose_name_plural = _('groups')

        def __str__(self):
            return self.name

        def natural_key(self):
            return (self.name,)


.. _GroupManager:

class **contrib.auth.models.GroupManager**
--------------------------------------------

.. seealso::

   - https://github.com/django/django/blob/master/django/contrib/auth/models.py


.. code-block:: python
   :linenos:

    class GroupManager(models.Manager):
        """
        The manager for the auth's Group model.
        """
        use_in_migrations = True

        def get_by_natural_key(self, name):
            return self.get(name=name)
