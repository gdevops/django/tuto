
.. index::
   pair: Django for professionals; 2019

.. _django_for_prof:

====================================================
**Django for professionnals** from William Vincent
====================================================

.. seealso::

   - https://github.com/wsvincent/djangoforprofessionals
   - :ref:`william_vincent`


.. figure:: djangoforprofessionals.jpg
   :align: center
