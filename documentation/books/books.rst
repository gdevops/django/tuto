
.. index::
   ! Books

.. _django_books:

============================================
Django books
============================================

.. seealso::

   - https://wsvincent.com/books/

.. toctree::
   :maxdepth: 3

   2019/2019
   2018/2018
