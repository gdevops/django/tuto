
.. index::
   pair: Django for beginners ; Will Vincent


.. _django_for_beginners:

============================================
**Django for beginners** by Will Vincent
============================================

.. seealso::

   - https://github.com/wsvincent/djangoforbeginners


.. figure:: cover_django_for_beginners.jpg
   :align: center
   :width: 300
