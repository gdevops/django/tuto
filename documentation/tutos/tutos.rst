.. index::
   pair: Django ; Style guides

.. _other_django_tutos:

============================================
Django tutorials/ style guides
============================================


.. toctree::
   :maxdepth: 3

   django-cheat-sheet/django-cheat-sheet
   admin_cookbook/admin_cookbook
   andrew_pinkham/andrew_pinkham
   hacksoft/hacksoft
   haki_benita/haki_benita
   kode_blog/kode_blog
   views_luke_plant/views_luke_plant
