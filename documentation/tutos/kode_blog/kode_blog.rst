.. index::
   pair: Django Tuto; Kode blog


.. _django_tuto_kode_blog:

============================================
**Django Tutorial, Django 3**
============================================


.. seealso::

   - https://kode-blog.io/django-tutorial
   - https://www.reddit.com/r/django/comments/f3o9av/complete_free_course_on_django_3/




Introduction
============


Django is a free and open-source framework based on python programming language
to develop web applications.

The architecture of Django is Model-Template-View (MTV).

The model layer relates to the logical database structure.

The view layer is for data formatting and the template layer is the presentation layer.

This tutorial series shows you how to use Django to create a simple web application.
The following image shows the application that we will create.


Critical
=========

.. seealso::

   - https://www.reddit.com/r/django/comments/f3o9av/complete_free_course_on_django_3/
