.. index::
   pair: Django ; IVR (Interactive Voice Response)
   pair: IVR ; Interactive Voice Response

.. _twilio_ivr:

===========================================================================================
**Building an IVR (Interactive Voice Response) System with Python, Django and Twilio**
===========================================================================================

.. seealso::

   - https://hakibenita.com/python-django-twilio-ivr
   - https://www.twilio.com/blog/building-interactive-voice-response-ivr-system-python-django-twilio
   - https://ngrok.com
   - :ref:`haki_benita_ref`





Introduction
============

Last year my team and I worked on a very challenging IVR system.

After almost a year in production and thousands of processed transactions,
I teamed up with the great people over at the Twilio blog to write an
introductory tutorial for developing IVR systems using Django and Twilio IVR.

Aside from "making your server talk" and diving into the cool speech features,

**I found the most challenging part working on IVR is designing the views**.

Unlike APIs and Forms, IVR is very limited in the type of input it takes
(DTMF tones, transcribed speech), and the amount of data it can communicate
and process is limited.


IVR stands for Interactive Voice Response system.

It's a way for you to communicate with your users over the phone.

IVR is operated by voice and by the DTMF tones that phones produce when
pressing keys on the keypad.

Just like your web site, mobile app or chatbot, IVR is another way for you
to interact with your users.
IVR holds a unique set of features that makes it ideal under some circumstances:

- Visual impairments: Users that rely on screen readers to navigate websites
  and mobile apps are used to interact with voice based interfaces.
- No Internet access: Users in remote areas or in places where an Internet
  connection is not constantly available or very expensive.
- Technologically challenged: Users that have trouble dealing with websites
  and apps, like the elderly, often find IVR much easier to operate.
- No access to smartphones or a computer: Users that don't own a computer
  or a smartphone at all, or users such as drivers that at certain times cannot
  handle a smartphone.


Requirements
=================

.. seealso::

   - https://ngrok.com

To follow along with this tutorial you are going to need:

- Python: you can find installation instructions on the Python website.
- Django: a web framework for Python.
- ngrok: a utility program to create a secure tunnel from the internet to your
  local development environment.
  In production your server is most likely already exposed to the internet, so
  this is necessary only for development.
- A Twilio Account: If you are new to Twilio create a free account now.
  You can review the features and limitations of a free Twilio account.
- A phone line: to test your IVR system you'll need a phone to make voice calls
  to your application (just not a rotary dial telephone!).
