
.. index::
   ! Advanced django cheat sheet

.. _advanced_django_cheat_sheet:

============================================
Advanced django cheat sheet
============================================

- https://github.com/ju-c/advanced-django-cheat-sheet
- https://github.com/ju-c
- https://github.com/ju-c.atom
- https://github.com/ju-c/advanced-django-cheat-sheet
- https://github.com/ju-c/advanced-django-cheat-sheet/commits.atom
- https://www.julienc.net/posts/django-cheatsheet
