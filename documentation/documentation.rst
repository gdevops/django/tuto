.. index::
   pair: Django ; Source documentation
   ! Django source documentation


.. _django_source_doc:

============================================
Doc
============================================

.. seealso::

   - https://github.com/django/django/tree/master/docs


.. toctree::
   :maxdepth: 3

   asynchrone/asynchrone
   books/books
   meta_postgresql/meta_postgresql
   ref/ref
   topics/topics
   tutos/tutos
