
.. _django_architecture:

============================================
Django architecture
============================================


.. toctree::
   :maxdepth: 3

   django_readers/django_readers
   django_zen_queries/django_zen_queries
   django_forms_dynamic/django_forms_dynamic
