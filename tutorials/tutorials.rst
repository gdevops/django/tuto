.. index::
   pair: Django ; Tutorials

.. _django_tutorials:

============================================
**Django tutorials**
============================================


w3schools
===========

- https://www.w3schools.com/django/index.php


djangonaut.space
===================

- https://djangonaut.space/
