.. Django tutorials documentation master file, created by
   sphinx-quickstart on Sun Apr  8 18:41:30 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


.. figure:: images/django-logo-negative.png
   :align: center
   :width: 300

|FluxWeb| `RSS <https://gdevops.frama.io/django/tuto/rss.xml>`_


.. _django_tuto:

============================================
**Django tutorial**
============================================


- https://github.com/django/django
- https://x.com/djangoproject
- https://github.com/django/django/commits/main
- https://github.com/django/django/graphs/contributors
- https://github.com/django/django/tree/main/docs
- https://forum.djangoproject.com/
- https://discord.gg/django
- https://awesomedjango.org/
- https://star-history.com/#django/django
- https://groups.google.com/g/django-developers
- https://code.djangoproject.com/wiki
- https://github.com/django/deps
- https://github.com/django/django/pulse
- https://github.com/sponsors/django
- :ref:`django_people`
- https://djangoblogs.com/
- :ref:`genindex`
- https://github.com/ju-c/advanced-django-cheat-sheet

:Django Dev: :ref:`django_dev`
:Djago pre-release: :ref:`django_prerelease`
:Last Django release: :ref:`django_versions:django_stable`

.. warning:: :ref:`django_user_custom`
   ! Never user the built-in Django User model directly !


.. toctree::
   :maxdepth: 5
   :caption: Django informations

.. toctree::
   :maxdepth: 3

   architecture/architecture
   deps/deps
   install/install
   deploiement/deploiement
   documentation/documentation
   people/people
   community/community
   dates-management/dates-management  
   htmx/htmx
   performance/performance
   tests/tests
   tips/tips
   tools/tools
   tutorials/tutorials


.. toctree::
   :maxdepth: 3
   :caption: Django versions

   versions/versions
