# Minimal makefile
#

# You can set these variables from the command line.
THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

# Put it first so that "make" without argument is like "make help".
help:
	@echo " "
	@echo "Targets:"
	@echo " "
	@echo "=========================="
	@echo "- make check_all"
	@echo "- make build_docker_staging"
	@echo "=========================="
	@echo "- make build_dev"
	@echo "- make up_dev"
	@echo "- make up_db_dev"
	@echo "- make exec_db_dev"
	@echo "- make exec_django_dev"
	@echo "- make log_dev"
	@echo "- make ps_dev"
	@echo "- make top_dev"
	@echo "- make down_dev"
	@echo "- make env_db_dev"
	@echo "- make env_django_dev"
	@echo "  ------------------------"
	@echo "- make test_dev"
	@echo "=========================="
	@echo "- make build_prod "
	@echo "- make up_prod "
	@echo "- make up_db_prod "
	@echo "- make exec_db_prod "
	@echo "- make exec_django_prod "
	@echo "- make log_prod "
	@echo "- make ps_prod "
	@echo "- make top_prod "
	@echo "- make down_prod "
	@echo "- make env_db_prod "
	@echo "- make env_django_prod "
	@echo "  ------------------------"
	@echo "- make test_prod "
	@echo "=========================="
	@echo " "


build_docker_staging:
	cd docker-django; docker build -f Dockerfile --tag docker.id3.eu/informatique/update_id3_staging:master .

######################################## DEV ###########################
build_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml build

up_db_dev:
	docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml up db

up_dev:
	@$(MAKE) -f $(THIS_MAKEFILE) down_prod
	docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml up

exec_db_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml exec db bash

exec_django_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml exec django bash

down_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml down

env_django_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml exec django env

env_db_dev:
	docker-compose -f docker-compose_base.yml  -f docker-compose_dev.yml exec db env

log_dev:
	docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml logs

ps_dev:
	docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml ps

top_dev:
	docker-compose -f docker-compose_base.yml -f docker-compose_dev.yml top

######################################## PROD ##########################

build_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml build

up_prod:
	@$(MAKE) -f $(THIS_MAKEFILE) down_dev
	docker-compose -f docker-compose_base.yml -f docker-compose_production.yml up -d

down_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml down

up_db_prod:
	docker-compose -f docker-compose_base.yml -f docker-compose_production.yml up db

exec_db_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml exec db bash

exec_django_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml exec django bash

env_django_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml exec django env

env_db_prod:
	docker-compose -f docker-compose_base.yml  -f docker-compose_production.yml exec db env

log_prod:
	docker-compose -f docker-compose_base.yml -f docker-compose_production.yml logs

ps_prod:
	docker-compose -f docker-compose_base.yml -f docker-compose_production.yml ps

top_prod:
	docker-compose -f docker-compose_base.yml -f docker-compose_production.yml top

########################################################################

.PHONY: help Makefile
