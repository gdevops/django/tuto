
.. _django_docker_compose_example_1:

==================================
Django docker-compose example 1
==================================

.. seealso::

   - :ref:`python_docker_multi_example_2`
   - :ref:`python_docker_multi_example_3`





::

    docker_compose/examples/example_1:
    total 48
    4 drwxrwxr-x 3  4096 mai   15 13:28 .
    4 drwxrwxr-x 3  4096 mai   15 11:42 ..
    4 -rw-r--r-- 1   480 mai   15 12:49 docker-compose_base.yml
    4 -rw-r--r-- 1   741 mai   15 12:49 docker-compose_dev.yml
    4 -rw-r--r-- 1   501 mai    7 10:45 docker-compose_production.yml
    4 drwxrwxr-x 2  4096 mai   15 13:28 docker_django
    4 -rw-r--r-- 1   113 mai   15 12:49 .env_db_dev
    4 -rw-r--r-- 1   113 mai   15 12:49 .env_db_production
    4 -rw-r--r-- 1   244 mai   15 13:14 .env_django_dev
    4 -rw-r--r-- 1   244 mai   15 13:16 .env_django_production
    4 -rw-rw-r-- 1  1867 mai   15 13:12 example_1.rst
    4 -rw-r--r-- 1  3703 mai   15 13:05 Makefile

    docker_compose/examples/example_1/docker_django:
    total 16
    4 drwxrwxr-x 2  4096 mai   15 13:28 .
    4 drwxrwxr-x 3  4096 mai   15 13:28 ..
    4 -rw-rw-r-- 1  3075 mai   15 13:20 Dockerfile
    4 -rw-rw-r-- 1  2065 mai   15 13:18 Dockerfile_dev


Makefile
===========

.. literalinclude:: Makefile
   :linenos:


docker-compose_base.yml
==========================

.. literalinclude:: docker-compose_base.yml
   :linenos:


Development
=============

.env_db_dev
--------------

.. literalinclude:: .env_db_dev
   :linenos:

.env_django_dev
----------------

.. literalinclude:: .env_django_dev
   :linenos:


docker_django/Dockerfile_dev
-------------------------------

.. literalinclude:: docker_django/Dockerfile_dev
   :linenos:
   :language: docker


docker-compose_dev.yml
--------------------------

.. literalinclude:: docker-compose_dev.yml
   :linenos:


Production
=============

.env_db_production
--------------------

.. literalinclude:: .env_db_production
   :linenos:


.env_django_production
------------------------

.. literalinclude:: .env_django_production
   :linenos:


docker_django/Dockerfile
-------------------------------

.. literalinclude:: docker_django/Dockerfile
   :linenos:
   :language: docker


docker-compose_production.yml
--------------------------------

.. literalinclude:: docker-compose_production.yml
   :linenos:
