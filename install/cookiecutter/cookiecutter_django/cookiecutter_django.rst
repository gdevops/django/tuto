
.. index::
   ! Cookiecutter Django

.. _django_cookicutter_django:

===============================================================================================
Cookiecutter Django is a framework for jumpstarting production-ready Django projects quickly
===============================================================================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/pydanny/cookiecutter-django


.. toctree::
   :maxdepth: 3

   definition/definition
