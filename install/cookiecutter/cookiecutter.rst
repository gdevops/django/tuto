
.. index::
   pair: Django ; layout; cookicutter
   pair: Django ; cookicutter
   ! cookicutter
   ! layout


.. _django_cookicutter:

======================================================
Django cookicutter templates (physical architecture)
======================================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/audreyr
   - https://github.com/audreyr/cookiecutter
   - :ref:`python_cookiecutter`
   - :ref:`python_layout`


.. toctree::
   :maxdepth: 3

   cookiecutter_django/cookiecutter_django
   cookiecutter_django_vue/cookiecutter_django_vue
   ddpt/ddpt
   wemake_django_template/wemake_django_template
