
.. index::
   pair: django ; vue

.. _cookiecutter_django_vue:

===============================================================================================
cookiecutter-django-vue
===============================================================================================

.. seealso::

   - https://github.com/vchaptsev/cookiecutter-django-vue
