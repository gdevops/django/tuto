
.. index::
   ! We make template
   pair: We make template ; cookicutter

.. _we_make_template:

===============================================================================================
**wemake-django-template** Bleeding edge django template focused on code quality and security
===============================================================================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/wemake-services/wemake-django-template
   - https://wemake-django-template.readthedocs.io/en/latest/
