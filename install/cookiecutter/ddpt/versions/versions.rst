
.. index::
   pair: ddpt ; versions

.. _cookicutter_ddpt_versions:

================================
cookicutter ddpt versions
================================

.. seealso::

   - https://github.com/douglasmiranda/ddpt/releases


.. toctree::
   :maxdepth: 3

   2019_04_09/2019_04_09
