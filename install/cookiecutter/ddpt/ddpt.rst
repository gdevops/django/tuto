
.. index::
   pair: ddpt ; cookicutter
   ! ddpt


.. _cookicutter_ddpt:

======================================================================================================
cookicutter ddpt : template for Django Projects - From development to production with Docker Swarm
======================================================================================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/douglasmiranda/ddpt


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
