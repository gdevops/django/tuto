.. index::
   pair: Django ; Installation


.. _install_django:

============================================
Installation
============================================

.. toctree::
   :maxdepth: 3

   cookiecutter/cookiecutter
   docker_compose/docker_compose
