.. index::
   pair: Django ; Performances

.. _django_perf:

=========================================================
**Django performances**
=========================================================


.. toctree::
   :maxdepth: 3

   relateds/relateds
   tools/tools
   articles/articles
