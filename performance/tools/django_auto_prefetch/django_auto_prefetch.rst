.. index::
   pair: Package; django-auto-prefetch
   !  django-auto-prefetch

.. _django_auto_prefetch:

=========================================================
** django-auto-prefetch**
=========================================================

- https://x.com/tolomea
- https://groups.google.com/g/django-developers/c/EplZGj-ejvg
- https://github.com/tolomea/django-auto-prefetch
- https://x.com/AdamChainz/status/1261025760992735240?s=20&t=uh07z4PZQaglNCihVXA9sg


Adam Johnson non lue, 15 août 2017, 23:30:10 à django-d...@googlegroups.com
===============================================================================


- https://groups.google.com/g/django-developers/c/EplZGj-ejvg/m/yHLjxmNlCAAJ


I'm biased here, Gordon was my boss for nearly three years, 2014-2016.

I'm in favour of adding the auto-prefetching, I've seen it work.

It was created around midway through last year and applied to our application's
Admin interface, which was covered in tests with django-perf-rec.

After adding the automatic prefetch, we not only identified some
existing N+1 query problems that hadn't been picked up (they were there
in the performance record files, just there were so many queries humans
had missed reading some), we also found a number of stale prefetch queries
that could be removed because the data they fetched wasn't being used.

Additionally not all of the admin interface was perf-rec-tested/optimized
so some pages were "just faster" with no extra effort.

I think there's a case for adding it to core - releasing as a third party
package would make it difficult to use since it requires changes
- mostly small - to QuerySet, ForeignKey, and the descriptor for ForeignKey.

Users of such a package would have to use subclasses of all of these,
the trickiest being ForeignKey since it triggers migrations...

We had the luxury in our codebase of already having these subclasses for
other customizations, so it was easier to roll out.


Luke Plant non lue, 16 août 2017, 00:15:11 à django-d...@googlegroups.com
============================================================================

- https://groups.google.com/g/django-developers/c/EplZGj-ejvg/m/Bf-6tthnCAAJ


I agree with Marc here that the proposed optimizations are 'magical'.

I think when it comes to optimizations like these you simply cannot know
in advance whether doing extra queries is going to a be an optimization
or a **pessimization**.

If I can come up with a single example where it would significantly
decrease performance (either memory usage or speed) compared to the
default (and I'm sure I can), then I would be strongly opposed to it
ever being default behaviour.

Concerning implementing it as an additional  QuerySet method like
`auto_prefetch()` - I'm not sure what I think, I feel like it could
get icky (i.e. increase our technical debt), due to the way it couples
things together.

**I can't imagine ever wanting to use it, though, I would always prefer
the manual option**.

Luke
