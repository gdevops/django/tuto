.. index::
   pair: Tools; Django Performance

.. _django_perf_tools:

=========================================================
**Django performance tools**
=========================================================


.. toctree::
   :maxdepth: 3

   decorators/decorators
   django_auto_prefetch/django_auto_prefetch
   django_silk/django_silk
