

.. _go_fast_with_django:

=========================================================
**Go Fast With Django**
=========================================================


- https://www.mattlayman.com/understand-django/go-fast/


On the flip side of doing more work, what does it mean to do less work
to improve performance ?

::

    The fastest code is no code.
