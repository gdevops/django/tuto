

.. _akshaythekkath_2021:

==========================================================================================================================
**Understanding select_related and prefetch_related in Django** by akshaythekkath (https://hashnode.com/@akshaythekkath)
==========================================================================================================================

- https://blog.akshaythekkath.com/understanding-selectrelated-and-prefetchrelated-in-django

Introduction
=============

When I started learning Django, the one thing that fascinated me a lot
was the Django ORM.

Maybe I was happy that I will not have to write SQL queries and can skip
all the pain of writing crazy joins to fetch the data.

I was throwing in objects.all() and objects.filter(...) with joy all
around my application. **Until I reached a point where I had a lot of data
in the system and it started to lag**.

Sometimes as ORM users, we don't bother to see the underlying SQL query
that is being built, or how many queries are fired ? which becomes
expensive over time.

**My perspective started to change**. I started thinking **in SQL first**
and then how to query the Django ORM so that it builds the desired SQL
query.
**With this approach, I started optimizing the queries and the latency
of my APIs began to drop.**
