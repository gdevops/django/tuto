

.. haki_2017_prefetch:

=========================================================
**All You Need To Know About Prefetching in Django**
=========================================================

- https://hakibenita.com/all-you-need-to-know-about-prefetching-in-django


Introduction
=============

I have recently worked on a ticket ordering system for a conference.

It was very important for the customer to see a table of orders including
a column with a list of program names in each order:
