.. index::
   pair: Performance; prefetch class
   !  prefetch

.. _prefetch_class:

=========================================================
**class Prefetch(lookup, queryset=None, to_attr=None)**
=========================================================

- https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.Prefetch

Other links
=============

- https://hakibenita.com/things-you-must-know-about-django-admin-as-your-app-gets-bigger

Django documentation
=======================

- https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.Prefetch


The **Prefetch() object** can be used to control the operation of **prefetch_related()**.

The lookup argument describes the relations to follow and works the same
as the string based lookups passed to prefetch_related().

For example:


.. code-block:: python

    >>> from django.db.models import Prefetch
    >>> Question.objects.prefetch_related(Prefetch('choice_set')).get().choice_set.all()
    <QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>
    # This will only execute two queries regardless of the number of Question
    # and Choice objects.
    >>> Question.objects.prefetch_related(Prefetch('choice_set')).all()
    <QuerySet [<Question: What's up?>]>
