.. index::
   pair: Package; django-auto-prefetch
   !  django-auto-prefetch

.. _select_related_and_prefetch_related:

=========================================================================================
**Django select_related and prefetch_related** by https://goutomroy.medium.com/
=========================================================================================

- https://betterprogramming.pub/django-select-related-and-prefetch-related-f23043fd635d


Other links
==============

- https://betterprogramming.pub/django-database-optimization-tips-4e11631dbc2c
- https://betterprogramming.pub/understanding-django-database-querysets-and-its-optimizations-1765cb9c36e5

Description
===========

In Django, select_related and prefetch_related are designed to stop the
deluge of database queries that are caused by accessing related objects
