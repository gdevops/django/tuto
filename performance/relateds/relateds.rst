.. index::
   pair: Performance; relateds
   !  django-relateds

.. _django_relateds:

=========================================================
**related + prefetch class**
=========================================================

.. toctree::
   :maxdepth: 3

   prefetch/prefetch
   prefetch_related/prefetch_related
   select_related/select_related
   liste_select_related/list_select_related
   articles/articles
