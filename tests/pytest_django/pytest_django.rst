.. index::
   pair: Django ; pytest
   pair: pytest-django; conftest.py
   ! pytest-django


.. _pytest_django:

============================================
**pytest-django**
============================================

.. seealso::

   - http://pytest-django.readthedocs.io/en/latest/index.html
   - https://docs.pytest.org/en/latest/plugins.html
   - https://github.com/pytest-dev/pytest-django
   - https://pytest-django.readthedocs.io/en/latest/tutorial.html


.. toctree::
   :maxdepth: 3

   test_database/test_database
   pytest_cov/pytest_cov
   pytest_willison/pytest_willison
