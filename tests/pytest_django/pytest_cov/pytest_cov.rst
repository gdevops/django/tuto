
.. index::
   pair: pytest-cov; django

.. _pytest_cov:

================================================
ptest-cov
================================================

.. seealso::

   - https://pytest-cov.readthedocs.io/en/latest/
   - https://github.com/pytest-dev/pytest-cov/
   - https://docs.pytest.org/en/latest/plugins.html





Description
===============
