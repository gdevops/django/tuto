
.. index::
   pair: pytest-django; conftest.py


.. _pytest_external_base:

================================================
Using an existing, external database for tests
================================================

.. seealso::

   - http://pytest-django.readthedocs.io/en/latest/index.html
   - https://github.com/pytest-dev/pytest-django
   - https://pytest-django.readthedocs.io/en/latest/tutorial.html





Description
===============

This example shows how you can connect to an existing database and use
it for your tests.
This example is trivial, you just need to disable all of pytest-django
and Django’s test database creation and point to the existing database.

This is achieved by simply implementing a no-op django_db_setup fixture.

Put this into conftest.py::

    # conftest.py
    # DATABASE_URL=postgresql://<username>:<password>@XX.0.XX.XX:5432/db_intranet

    import pytest
    from django.conf import settings

    @pytest.fixture(scope="session")
    def django_db_setup():
        settings.DATABASES["default"] = {
            "ENGINE": "django.db.backends.postgresql",
            # Test database
            "HOST": "XX.0.XX.XX",
            "NAME": "db_intranet",
        }
