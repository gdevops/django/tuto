

.. _advices_simonsw:

============================================
Advices from Simon Willison (2020-02-11)
============================================

.. seealso::

   - :ref:`cheating_pytest_2020_02_11`
   - https://simonwillison.net/2020/Feb/11/cheating-at-unit-tests-pytest-black/
   - https://simonwillison.net/2018/Jul/28/documentation-unit-tests/
   - https://x.com/simonw
   - https://github.com/simonw
   - https://simonwillison.net/tags/datasettecloud/
   - https://github.com/simonw/datasette
   - https://datasette.readthedocs.io/en/latest/ecosystem.html#ecosystem





Extract
==========

I’ve used this technique to write many of the tests in both Datasette_ and
sqlite-utils_, and **those are by far the best tested pieces of software
I’ve ever released**.

I started doing this around two years ago, and I’ve held off writing about it
until I was confident I understood the downsides.

I haven’t found any yet: **I end up with a robust, comprehensive test suite and
it takes me less than half the time to write the tests** than if I’d been
hand-crafting all of those comparisons from scratch.


.. _Datasette: https://github.com/simonw/datasette
.. _sqlite-utils:  https://github.com/simonw/sqlite-utils
