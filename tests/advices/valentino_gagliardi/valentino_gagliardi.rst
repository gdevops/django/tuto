

.. _django_tests_valentino:


===============================================================
Advices from Valentino Gagliardi
===============================================================

.. seealso::

   - https://x.com/gagliardi_vale

I’ve seen a ton of Django tutorials starting like so::

    class SomeModelModelTest(TestCase):
        def setUp(self):
            SomeModel.objects.create(
                name=fake.name(),
                email=fake.email(),
                phone=fake.phone_number(),
                message=fake.text(),
                source=fake.url()
            )
        def test_save_model(self):
            saved_models = SomeModel.objects.count()
            self.assertEqual(saved_models, 2)

**Don’t do that**.

There’s no point in testing neither a vanilla Django model nor the Django ORM.

Here’s a good starting point for testing in Django:

- do not test Django built-in code (models, views, etc)
- do not test Python built-in functions

To recap: do not test what is already tested !
