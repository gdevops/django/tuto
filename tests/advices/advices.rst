.. index::
   pair: Advices ; Tests

.. _django_tests_advices:

============================================
**Django tests advices**
============================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/topics/testing/
   - https://realpython.com/tutorials/testing/


.. toctree::
   :maxdepth: 3

   haki_benita/haki_benita
   luke_plant/luke_plant
   simon_willinson/simon_willinson
   valentino_gagliardi/valentino_gagliardi
   william_vincent/william_vincent
