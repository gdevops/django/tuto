.. index::
   pair: Django ; Tests

.. _django_tests:

============================================
**Tests**
============================================

- https://docs.djangoproject.com/en/dev/topics/testing/
- http://douche.name/blog/nomenclature-des-tests-logiciels/
- https://realpython.com/tutorials/testing/


**Tests can be a bummer to write but even a bigger nightmare to maintain** bu Haki Benita

The **smarter not harder mindset** is also essential for writing good automated software tests. by Luke Plant

.. toctree::
   :maxdepth: 3


   advices/advices
   django_test/django_test
   django_functest/django_functest
   pytest_django/pytest_django
   tutorials/tutorials
