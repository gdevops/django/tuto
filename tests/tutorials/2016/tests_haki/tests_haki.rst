
.. index::
   pair: 2016; Haki Benita


.. _haki_tests_2016:

=======================================================================================================
Keeping Tests DRY with Class Based Tests In Python, a class based approach to writing tests in Python
=======================================================================================================


**Tests can be a bummer to write but even a bigger nightmare to maintain**




Introduction
=============

**Tests can be a bummer to write but even a bigger nightmare to maintain**

When we noticed we are putting off simple tasks just because **we were
afraid to update some monster test case**, we started looking for more
creative ways to simplify the process of writing and maintaining tests.

In this article I will describe a class based approach to writing tests.

Before we start writing code let's set some goals:

- **Extensive** - We want our tests to cover as many scenarios as possible.
  We hope a solid platform for writing tests will make it easier for us
  to adapt to changes and cover more grounds.
- **Expressive** - Good tests tell a story. Issues become irrelevant and
  documents get lost but tests must always pass - this is why we treat
  our tests as specs. Writing good tests can help newcomers (and future self)
  to understand all the edge cases and micro-decisions made during development.
- **Maintainable** - As requirements and implementations change we want to
  adapt quickly with as little effort as possible.
