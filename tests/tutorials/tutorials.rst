
.. index::
   pair: Tests ; Tutorials


.. _django_tests_tutorials:

============================================
**Django tests tutorials**
============================================

.. seealso::

   - :ref:`tests`
   - https://docs.djangoproject.com/en/dev/topics/testing/
   - https://realpython.com/tutorials/testing/


.. toctree::
   :maxdepth: 3

   2020/2020
   2018/2018
   2017/2017
   2016/2016
   2013/2013
