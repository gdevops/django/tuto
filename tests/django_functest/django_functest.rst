.. index::
   pair: Django-functest ; Tests

.. _django_functest:

============================================
**django-functest**
============================================

- https://github.com/django-functest/django-functest


Features
===========

- A simplified API for writing functional tests in Django (tests that
  check the behaviour of entire views, or sets of views, e.g. a checkout process).
- A unified API that abstracts over both WebTest and Selenium - write
  two tests at once!
- Many of the gotchas and difficulties of using WebTest and Selenium
  ironed out for you.
- Well tested - as well as its own test suite, which is run against Firefox
  and Chrome, it is also used by Wolf & Badger for tests covering many
  business critical functionalities.

Typical usage
================

In your tests.py:

.. code-block:: python

    from django.test import LiveServerTestCase, TestCase
    from django_functest import FuncWebTestMixin, FuncSeleniumMixin, FuncBaseMixin

    class ContactTestBase(FuncBaseMixin):
        # Abstract class, doesn't inherit from TestCase

        def test_contact_form(self):
            self.get_url('contact_form')
            self.fill({'#id_name': 'Joe',
                       '#id_message': 'Hello'})
            self.submit('input[type=submit]')
            self.assertTextPresent("Thanks for your message")

     class ContactWebTest(ContactTestBase, FuncWebTestMixin, TestCase):
         pass

     class ContactSeleniumTest(ContactTestBase, FuncSeleniumMixin, LiveServerTestCase):
         pass


In this way, you can write a single test with a high-level API, and run
it in two ways - using a fast, WSGI-based method which emulates typical
HTTP usage of a browser, and using a full browser that actually executes
Javascript (if present) etc.

Under the hood, the WSGI-based method uses and builds upon WebTest and
django-webtest.

django-functest provides its functionality as mixins, so that you can
have your own base class for tests.
