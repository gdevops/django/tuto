.. index::
   pair: django.test ; Tests

.. _django_test:

============================================
**django.test**
============================================

- https://github.com/django-functest/django-functest


.. toctree::
   :maxdepth: 3

   how-to-unit-test-a-django-form/how-to-unit-test-a-django-form/
