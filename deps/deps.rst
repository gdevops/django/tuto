
.. index::
   pair: Django ; DEP
   ! DEPs

.. _django_deps:

==================================================
**DEPs**
==================================================

.. seealso::

   - https://github.com/django/deps


**Django Enhancement Proposals  (DEPs)**

.. toctree::
   :maxdepth: 3

   0008/0008
   0009/0009
   0010/0010
