.. index::
   pair: Deploiement; HackInScience
   pair: Ansible ; HackInScience
   pair: Deploiement; Julien Palard

.. _hkis_ansible:

===========================================================================
**Deploiement de HackInScience avec ansible** (par Julien Palard)
===========================================================================

- https://discuss.afpy.org/u/mdk/summary
- https://discuss.afpy.org/t/deploiement-d-une-application-django/1643
- https://framagit.org/hackinscience/hkis-ansible/-/blob/main/roles/website/tasks/main.yml

Pour ma part je déploie tout systématiquement avec Ansible.

Mais que ce soit Ansible, Salt, Puppet, Chef, un Makefile, un shell script,
ou n’importe quoi d’autre, peu importe en fait, ce qui compte pour moi
c’est le côté reproductible : ton serveur brûle ?

Pas de soucis, tu relances le playbook, tu recharges une sauvegarde, et
c’est reparti.

J’ai par exemple HackInScience qui est déployé comme ça https://framagit.org/hackinscience/hkis-ansible/-/blob/main/roles/website/tasks/main.yml


.. literalinclude:: main.yml
   :language: yaml
   :linenos:


Ça s’occupe des statiques, de l’i18n, du certificat HTTPS, de la configuration
de nginx…

Libre à toi de cp -r, tout relire, élaguer ce qui ne t’es pas utile,
renommer ce qui le doit, n’hésite pas à me poser des questions, te faire
relire (je ne suis pas le seul ici à lire de l’Ansible, d’ailleurs je ne
suis pas du tout le meilleur ici, je me contente de peu avec Ansible).

Attention ne cherche pas de gunicorn ou de uwsgi dans ma config hkis,
j’utilise daphne car j’ai des websockets (et donc vite de l’asyncio),
donc ça passe par asgi.py et pas wsgi.py.

Mais ça marche pareil sinon, ça sait très bien délivrer des vues synchrones
aussi.

Je te déconseille de déployer automatiquement via de la CI/CI, du moins
pas maintenant : KISS.

Pour héberger ça il te faut un serveur et un nom de domaine, dis-nous
si tu as aussi besoin de guidage sur le sujet.
