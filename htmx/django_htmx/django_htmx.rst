.. index::
   pair: Django; django-htmx


.. _django_htmx:

===========================================================
**django-htmx** (Extensions for using Django with **htmx**)
===========================================================

- https://github.com/adamchainz/django-htmx
- https://htmx.org/
- https://htmx.org/docs/#introduction
- :ref:`django_htmx_2021_02_01`


Introduction
============

- https://htmx.org/docs/#introduction

htmx allows you to access AJAX, CSS Transitions, WebSockets and
Server Sent Events directly in HTML, using attributes, so you can
build modern user interfaces with the simplicity and power of hypertext

htmx is small (~9k min.gz'd), dependency-free, extendable & IE11 compatible
