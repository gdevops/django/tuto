.. index::
   pair: HTMX; simple django htmx
   pair: Yaakov Lowenstein; simple django htmx

.. _simple_django_htmx:

============================================================================================
**Simple library to facilitate use of HTMX with Django** by Yaakov Lowenstein
============================================================================================

- https://github.com/yaakovLowenstein
- https://github.com/yaakovLowenstein/simple-django-htmx
- :ref:`new_simple_django_htmx`


Overview
=============

A package to simplify the usage of HTMX with Django.

Easily add HTMX reuqests without needing additional urls, and reduce
clutter in views by offloading all responsibility to an hx_request.


Announce
==========

- https://discord.com/channels/725789699527933952/864934037381971988/993328870017212447

I created a simple package to simplify the usage of htmx with django,
check it out if you have a chance and let me know if you think there
could be improvements or if there are any bugs! Thanks! https://pypi.org/project/simple-django-htmx/

FAQ
=====

Also how does this compare to django-htmx library ? Can you share a brief overview ?
---------------------------------------------------------------------------------------

- https://discord.com/channels/725789699527933952/864934037381971988/993767858922344488
- https://discord.com/channels/725789699527933952/864934037381971988/994026196994097192

The only comparison to django-htmx is pretty much the name.
There are really 2 main points to the library :

1) To avoid the messy views you get when you use htmx with django.
   With my library the htmx requests manage themselves meaning that you
   don't need to do if htmx request .... else render regular view ,
   see the documentation for more details on how that works.
2) To avoid all the extra urls! You don't need a new endpoint for every
   single request (edit, delete etc.) instead the htmx requests handle
   it themselves. (again see documentation for more details)

It also has some built in htmx requests for basic functionality.

If you want a delete button, there's a built in htmx request that all
you do is pass in the object (to a template tag) that you want to delete
and the built in htmx request will handle it for you!

Please take a look at the repo and documentation. https://github.com/yaakovLowenstein/simple-django-htmx

I am open to pull requests if there's anything you can think of adding/
or if any bugs are found!

Thanks!

.. figure:: images/introduction.png
   :align: center
