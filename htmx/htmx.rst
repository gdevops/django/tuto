.. index::
   pair: HTMX; Django

.. _packages_django_htmx:

============================================================================================
**Django with htmx**
============================================================================================

.. toctree::
   :maxdepth: 3


   django_htmx/django_htmx
   simple_django_htmx/simple_django_htmx
